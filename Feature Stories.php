<?php include "include/header.php" ?>
      <!-- menu end -->
    </div>
    <section class="wpb-content-wrapper">
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="simple-search" style="background: rgba(0, 0, 0, 0) url(wp-content/img/2022-MG-Astor-40-900x506.jpg) center center no-repeat; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
                <div class="container">
                 

 

                  <div class="search-holder">
                    <div id="custom-search-input">
                      <form method="get" action="#">
                        <div class="col-md-11 col-sm-11 col-xs-11 no-padding">
                          <input type="text" autocomplete="off" name="ad_title" id="autocomplete-dynamic" class="form-control " placeholder="What Are You Looking For..." />
                        </div>
                        <div class="col-md-1 col-sm-1 col-xs-1 no-padding">
                          <button class="btn btn-theme" type="submit">
                            <span class=" glyphicon glyphicon-search"></span>
                          </button>
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="custom-padding ">
                <!-- Main Container -->
                <div class="container">
                  <!-- Row -->
                  <div class="row">
                    <div class="heading-panel">
                      <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                        <!-- Main Title -->
                        <h2>Car feature  <span class="heading-color"> stories and </span>travelogues  </h2>
                        <!-- Short Description -->
                        <p class="heading-text">Can’t wait to get behind the wheel again? Here’s how BMW makes it easier and safer for you to do so..</p>
                      </div>
                    </div>
                    
                            <!-- Ad Box End -->
                          </div>
                          <div class="col-md-4 col-xs-12 col-sm-6">
                            <!-- Ad Box -->
                            <div class="category-grid-box">
                              <!-- Ad Img -->
                              <div class="category-grid-img">
                                <img src="wp-content/img/slavia-exterior-right-front-three-quarter-5.webp" alt="2017 Ford Mustang" class="img-responsive" />
                                <!-- Ad Status -->
                                <span class="ad-status">Featured</span>
                                <!-- User Review -->
                                <div class="user-preview">
                                  <a href="#">
                                    <img src="wp-content/img/slavia-exterior-right-front-three-quarter-5.webp" class="avatar avatar-small" alt="2017 Ford Mustang" />
                                  </a>
                                </div>
                                <!-- View Details -->
                                <a href="#" class="view-details"> View Details </a>
                                <!-- Additional Info -->
                                <div class="additional-information">
                                  <p>Transmission: Automatic</p>
                                  <p>Body Type: Coupe</p>
                                  <p>Condition: Used</p>
                                  <p>Posted on: January 24, 2019</p>
                                </div>
                                <!-- Additional Info End-->
                              </div>
                              <!-- Ad Img End -->
                              <div class="short-description">
                                <!-- Category Title -->
                                <div class="category-title">
                                  <span class="padding_cats">
                                    <a href="#">Ford</a>
                                  </span>
                                  <span class="padding_cats">
                                    <a href="#">Mustang</a>
                                  </span>
                                </div>
                                <h3>
                                  
                                </h3>
                                <div class="price">₹ 11<span class=""> .29 Lakh</span>
                                </div>
                              </div>
                              <!-- Addition Info -->
                              <div class="ad-info">
                                <ul class="list-unstyled">
                                  <li>
                                    <i class="flaticon-gas-station-1"></i>Petrol
                                  </li>
                                  <li>
                                    <i class="flaticon-dashboard"></i>12,454 km
                                  </li>
                                  <li>
                                    <i class="flaticon-engine-2"></i>2,000 cc
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <!-- Ad Box End -->
                          </div>
                          <div class="col-md-4 col-xs-12 col-sm-6">
                            <!-- Ad Box -->
                            <div class="category-grid-box">
                              <!-- Ad Img -->
                              <div class="category-grid-img">
                                <img src="wp-content/img/all-new-city-exterior-right-front-three-quarter.webp" alt="2010 Chevrolet Camaro" class="img-responsive" />
                                <!-- Ad Status -->
                                <span class="ad-status">Featured</span>
                                <!-- User Review -->
                                <div class="user-preview">
                                  <a href="../dealer/carspot/indexdb00.html?type=ads">
                                    <img src="wp-content/img/all-new-city-exterior-right-front-three-quarter.webp" class="avatar avatar-small" alt="2010 Chevrolet Camaro" />
                                  </a>
                                </div>
                                <!-- View Details -->
                                <a href="#" class="view-details"> View Details </a>
                                <!-- Additional Info -->
                                <div class="additional-information">
                                  <p>Transmission: Automatic</p>
                                  <p>Body Type: Coupe</p>
                                  <p>Condition: Used</p>
                                  <p>Posted on: January 24, 2019</p>
                                </div>
                                <!-- Additional Info End-->
                              </div>
                              <!-- Ad Img End -->
                              <div class="short-description">
                                <!-- Category Title -->
                                <div class="category-title">
                                  <span class="padding_cats">
                                    <a href="#">Corvette</a>
                                  </span>
                                </div>
                                <h3>
                                 
                                </h3>
                                <div class="price">₹ 11.<span class=""> 60 Lakh </span>
                                </div>
                              </div>
                              <!-- Addition Info -->
                              <div class="ad-info">
                                <ul class="list-unstyled">
                                  <li>
                                    <i class="flaticon-gas-station-1"></i>Petrol
                                  </li>
                                  <li>
                                    <i class="flaticon-dashboard"></i>23 km
                                  </li>
                                  <li>
                                    <i class="flaticon-engine-2"></i>1,500 cc
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <!-- Ad Box End -->
                          </div>
                          <div class="col-md-4 col-xs-12 col-sm-6">
                            <!-- Ad Box -->
                            <div class="category-grid-box">
                              <!-- Ad Img -->
                              <div class="category-grid-img">
                                <a href="#" class="play-video">
                                  <img src="wp-content/img/kushaq-exterior-right-front-three-quarter-52.webp" alt="Icon" />
                                </a>
                                <img src="wp-content/img/kushaq-exterior-right-front-three-quarter-52.webp" alt="Mustang Shelby GT350 Coupe" class="img-responsive" />
                                <!-- Ad Status -->
                                <span class="ad-status">Featured</span>
                                <!-- User Review -->
                                <div class="user-preview">
                                  <a href="#">
                                    <img src="wp-content/img/kushaq-exterior-right-front-three-quarter-52.webp" class="avatar avatar-small" alt="Mustang Shelby GT350 Coupe" />
                                  </a>
                                </div>
                                <!-- View Details -->
                                <a href="#" class="view-details"> View Details </a>
                                <!-- Additional Info -->
                                <div class="additional-information">
                                  <p>Transmission: Manual</p>
                                  <p>Body Type: Coupe</p>
                                  <p>Condition: Used</p>
                                  <p>Posted on: June 20, 2017</p>
                                </div>
                                <!-- Additional Info End-->
                              </div>
                              <!-- Ad Img End -->
                              <div class="short-description">
                                <!-- Category Title -->
                                <div class="category-title">
                                  <span class="padding_cats">
                                    <a href="#">Ford</a>
                                  </span>
                                  <span class="padding_cats">
                                    <a href="#">Mustang</a>
                                  </span>
                                </div>
                                <h3>
                                  <a href="#"></a>
                                </h3>
                                <div class="price">₹ 11.58 Lakh</div>
                              </div>
                              <!-- Addition Info -->
                              <div class="ad-info">
                                <ul class="list-unstyled">
                                  <li>
                                    <i class="flaticon-gas-station-1"></i>Diesel
                                  </li>
                                  <li>
                                    <i class="flaticon-dashboard"></i>5,400 km
                                  </li>
                                  <li>
                                    <i class="flaticon-engine-2"></i>3,500 cc
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <!-- Ad Box End -->
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="text-center">
                      <div class="load-more-btn">
                        <a href="#" target="%20_blank" class="btn btn-lg  btn-theme"> View All Featured Ads <i class="fa fa-refresh"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <div class="container">
                <div class="row">
                  <!-- <img alt="img" src="wp-content/img/c" class="block-content wow zoomIn" data-wow-delay="0ms" data-wow-duration="3500ms" /> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="news section-padding  gray ">
                <div class="container">
                  <div class="row">
                    <div class="heading-panel">
                      <div class="col-xs-12 col-md-12 col-sm-12 left-side">
                        <!-- Main Title -->
                        <h2>Choose   <span class="heading-color"> MG </span>Motor Uk HS Trim  </h2>
                        <!-- Short Description -->
                        <p class="heading-text">Buy the new MG HS today.</p>
                      </div>
                    </div>
                    <div class="col-md-7 col-sm-12 col-xs-12">
                      <div class="mainimage">
                        <a href="#">
                          <img class="img-responsive" alt="2017 Mercedes-Benz E-Class Coupe review" src="wp-content/img1/1119.webp" />
                          <div class="overlay">
                            <!-- <h2>2017 Mercedes-Benz E-Class Coupe review</h2> -->
                          </div>
                        </a>
                        <div class="clearfix"></div>
                      </div>
                    </div>
                    <div class="col-md-5 col-sm-12 col-xs-12">
                      <div class="newslist">
                        <ul>
                          <li>
                            <div class="imghold">
                              <a href="#">
                                <img class="img-responsive" alt="Audi RS 3 Sedan Review with pricing specs performance and safety" src="wp-content/img/a55mo4a_1528703.webp ">
                              </a>
                            </div>
                            <div class="texthold">
                              <h4>
                                <a href="#">The MG HS has the bold lines and proud features of a thoroughbred SUV designed for family living.  </a>
                              </h4>
                              <p></p>
                            </div>
                            <div class="clear"></div>
                          </li>
                          <li>
                            <div class="imghold">
                              <a href="#">
                                <img class="img-responsive" alt="The 2017 Honda NSX: A Responsible Supercar" src="wp-content/img/8tv6ota_1501799.webp" />
                              </a>
                            </div>
                            <div class="texthold">
                              <h4>
                                <a href="#"> purposeful, and with a spacious interior filled with comfort and intelligent features,</a>
                              </h4>
                              <p></p>
                            </div>
                            <div class="clear"></div>
                          </li>
                          <li>
                            <div class="imghold">
                              <a href="#">
                                <img class="img-responsive" alt="Driving The Epic 2017 Nissan GT-R Sports Edition" src="wp-content/img/mg-hs-gal-1.webp" />
                              </a>
                            </div>
                            <div class="texthold">
                              <h4>
                                <a href="#">s easy to just get in and get away for the perfect escape. With three trim levels to choose from; </a>
                              </h4>
                              <p></p>
                            </div>
                            <div class="clear"></div>
                          </li>
                          <li>
                            <div class="imghold">
                              <a href="#">
                                <img class="img-responsive" alt="The 2017 Honda Civic Si Is The Kind Of Fun That Won&#8217;t Ruin Your Life" src="wp-content/img/website-new-model-thumbnail-mg-hs-720x383.webp" />
                              </a>
                            </div>
                            <div class="texthold">
                              <h4>
                                <a href="#">MG HS offers buyers an exciting and high-quality alternative to the conventional choices with a sporty exterior and a family .</a>
                              </h4>
                              <p></p>
                            </div>
                            <div class="clear">
                              
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </section>
	<?php include 'include/footer.php';?>