<!doctype html>
<html lang="en-US">
  <!-- Mirrored from carspot.scriptsbundle.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Dec 2022 10:48:10 GMT -->
  <!-- Added by HTTrack -->
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <!-- /Added by HTTrack -->
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <title>CarSpot &#8211; </title>
    <meta name='robots' content='max-image-preview:large' />
    <link rel='dns-prefetch' href='http://static.addtoany.com/' />
    <link rel='dns-prefetch' href='http://www.google.com/' />
    <link rel='dns-prefetch' href='http://maps.googleapis.com/' />
    <link rel='dns-prefetch' href='http://code.iconify.design/' />
    <link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
    <link rel="alternate" type="application/rss+xml" title="CarSpot - WordPress Theme &raquo; Feed" href="feed/index.html" />
    <link rel="alternate" type="application/rss+xml" title="CarSpot - WordPress Theme &raquo; Comments Feed" href="comments/feed/index.html" />
 


    <link rel='stylesheet' id='contact-form-7-css' href='wp-content/plugins/contact-form-7/includes/css/styles77e1.css?ver=5.6.4' type='text/css' media='all' />
    <link rel='stylesheet' id='woocommerce-layout-css' href='wp-content/plugins/woocommerce/assets/css/woocommerce-layout9d27.css?ver=7.1.0' type='text/css' media='all' />
    <link rel='stylesheet' id='woocommerce-smallscreen-css' href='wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen9d27.css?ver=7.1.0' type='text/css' media='only screen and (max-width: 768px)' />
    <link rel='stylesheet' id='woocommerce-general-css' href='wp-content/plugins/woocommerce/assets/css/woocommerce9d27.css?ver=7.1.0' type='text/css' media='all' />
    <style id='woocommerce-inline-inline-css' type='text/css'>
      .woocommerce form .form-row .required {
        visibility: visible;
      }
    </style>
    <link rel='stylesheet' id='popup-video-iframe-style-css' href='wp-content/themes/carspot/css/video_player6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-style-css' href='wp-content/themes/carspot/style6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='bootstrap-css' href='wp-content/themes/carspot/css/bootstrap6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-theme-tooltip-css' href='wp-content/themes/carspot/css/user-dashboard/protip.min6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-theme-css' href='wp-content/themes/carspot/css/style6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-jquery-confirm-css' href='wp-content/themes/carspot/css/user-dashboard/jquery-confirm6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-datepicker-css' href='wp-content/themes/carspot/css/datepicker.min6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-theme-slug-fonts-css' href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400italic,600,600italic,700,700italic,900italic,900,300,300italic%7CPoppins:400,500,600&amp;subset=latin,latin-ext' type='text/css' media='all' />
    <link rel='stylesheet' id='et-line-fonts-css' href='wp-content/themes/carspot/css/et-line-fonts6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-css' href='wp-content/plugins/elementor/assets/lib/font-awesome/css/font-awesome.min1849.css?ver=4.7.0' type='text/css' media='all' />
    <link rel='stylesheet' id='line-awesome-css' href='wp-content/themes/carspot/css/line-awesome.min6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='animate-css' href='wp-content/themes/carspot/css/animate.min6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='flaticon-css' href='wp-content/themes/carspot/css/flaticon6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='flaticon2-css' href='wp-content/themes/carspot/css/flaticon26a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='custom-icons-css' href='wp-content/themes/carspot/css/custom_icons6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-select2-css' href='wp-content/themes/carspot/css/select2.min6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='nouislider-css' href='wp-content/themes/carspot/css/nouislider.min6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-range-slider-css' href='wp-content/themes/carspot/css/rangeslider.min6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='owl-carousel-css' href='wp-content/themes/carspot/css/owl.carousel6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='owl-theme-css' href='wp-content/themes/carspot/css/owl.theme6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-style-demo-css' href='wp-content/themes/carspot/css/style-demo6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-sb-menu-css' href='wp-content/themes/carspot/css/sb.menu6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-carspot-mystyle-css' href='wp-content/themes/carspot/css/mystyle6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-custom-css' href='wp-content/themes/carspot/css/custom6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='toastr-css' href='wp-content/themes/carspot/css/toastr.min6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-woo-css' href='wp-content/themes/carspot/css/woocommerce6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='minimal-css' href='wp-content/themes/carspot/skins/minimal/minimal6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='fancybox2-css' href='wp-content/themes/carspot/css/jquery.fancybox.min6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='slider-css' href='wp-content/themes/carspot/css/slider6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-menu-css' href='wp-content/themes/carspot/css/carspot-menu6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='responsive-media-css' href='wp-content/themes/carspot/css/responsive-media6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-responsive-css' href='wp-content/themes/carspot/css/responsive6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='defualt-color-css' href='wp-content/themes/carspot/css/colors/defualt.css' type='text/css' media='all' />
    <link rel='stylesheet' id='js_composer_front-css' href='wp-content/plugins/js_composer/assets/css/js_composer.minf7be.css?ver=6.10.0' type='text/css' media='all' />
    <link rel='stylesheet' id='addtoany-css' href='wp-content/plugins/add-to-any/addtoany.mind47c.css?ver=1.16' type='text/css' media='all' />
    <script type='text/javascript' src='wp-includes/js/jquery/jquery.mina7a0.js?ver=3.6.1' id='jquery-core-js'></script>
    <script type='text/javascript' src='wp-includes/js/jquery/jquery-migrate.mind617.js?ver=3.3.2' id='jquery-migrate-js'></script>
    <script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min53ec.js?ver=2.7.0-wc.7.1.0' id='jquery-blockui-js'></script>
    <script type='text/javascript' id='wc-add-to-cart-js-extra'>
      /* 
					<![CDATA[ */
      var wc_add_to_cart_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
        "i18n_view_cart": "View cart",
        "cart_url": "https:\/\/carspot.scriptsbundle.com\/cart\/",
        "is_cart": "",
        "cart_redirect_after_add": "no"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min9d27.js?ver=7.1.0' id='wc-add-to-cart-js'></script>
    <script type='text/javascript' id='addtoany-core-js-before'>
      window.a2a_config = window.a2a_config || {};
      a2a_config.callbacks = [];
      a2a_config.overlays = [];
      a2a_config.templates = {};
    </script>
 
    <script type='text/javascript' async src='../static.addtoany.com/menu/page.js' id='addtoany-core-js'></script>
    <script type='text/javascript' async src='wp-content/plugins/add-to-any/addtoany.min4963.js?ver=1.1' id='addtoany-jquery-js'></script>
    <script type='text/javascript' src='wp-content/plugins/js_composer/assets/js/vendors/woocommerce-add-to-cartf7be.js?ver=6.10.0' id='vc_woocommerce-add-to-cart-js-js'></script>
    <!--[if lt IE 9]>
				<script type='text/javascript' src='https://carspot.scriptsbundle.com/wp-content/themes/carspot/js/html5shiv.min.js?ver=3.7.2' id='carspot-maxcdn1-js'></script>
				<![endif]-->
    <!--[if lt IE 9]>
				<script type='text/javascript' src='https://carspot.scriptsbundle.com/wp-content/themes/carspot/js/respond.min.js?ver=1.4.2' id='carspot-maxcdn2-js'></script>
				<![endif]-->
    <link rel="https://api.w.org/" href="wp-json/index.html" />
    <link rel="alternate" type="application/json" href="wp-json/wp/v2/pages/2555.json" />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 6.1.1" />
    <meta name="generator" content="WooCommerce 7.1.0" />
    <link rel="canonical" href="index.html" />
    <link rel='shortlink' href='index.html' />
    <link rel="alternate" type="application/json+oembed" href="wp-json/oembed/1.0/embedbb5b.json?url=https%3A%2F%2Fcarspot.scriptsbundle.com%2F" />
    <link rel="alternate" type="text/xml+oembed" href="wp-json/oembed/1.0/embed7fb4?url=https%3A%2F%2Fcarspot.scriptsbundle.com%2F&amp;format=xml" />
    <meta name="framework" content="Redux 4.2.14" />
    <noscript>
      <style>
        .woocommerce-product-gallery {
          opacity: 1 !important;
        }
      </style>
    </noscript>
    <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress." />
    <style type="text/css" id="wp-custom-css">

   @media(min-width:320px) and (max-width:767px) {
        .section-container-left {
          padding-top: 200px;
        }
      }

      @media(min-width:320px) and (max-width:767px) {
        #wrapper .sidebar {
          position: absolute !important;
        }
      }

      .services-icons-section {
        left: auto !important;
      }
    </style>
    <noscript>
      <style>
        .wpb_animate_when_almost_visible {
          opacity: 1;
        }
      </style>
    </noscript>
  </head>
  <body >
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="loading" id="sb_loading">Loading&#8230;</div>
    <div class="sb-top-bar_notification">
      <a href="javascript:void(0)"> For a better experience please change your browser to CHROME, FIREFOX, OPERA or Internet Explorer. </a>
    </div>
    <div class="colored-header">
      <!-- Navigation Menu -->
      <div class="clearfix"></div>
      <!-- menu start -->
      <nav id="menu-1" class="mega-menu header-transparent ">
        <!-- menu list items container -->
        <section class="menu-list-items">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <!-- menu logo -->
                <ul class="menu-logo">
                  <li>
                    <a href="index.php">
                    <img src="wp-content/img/99.png" style="
    width: 113px;
    height: 73px;
    height: 100;
">
                    </a>
                  </li>
                </ul>
                <!-- menu links -->
                <!-- menu links -->
                <ul class="menu-links">
                  <li>
                    <a href="index.php">Home <i class="fa fa-angle-down fa-indicator"></i>
                    </a>
                    <ul class="drop-down-multilevel grid-col-12">
                      <li class="hoverTrigger">
                        <a href="cars.php">Search New cars </a>
                      <li class="hoverTrigger">
                        <a href="popular brand.php">Popular Brands </a>
                      <li class="hoverTrigger">
                        <a href="latest care.php">Latest cars</a>
                      <li class="hoverTrigger">
                        <a href="popular cars.php">Popular cars</a>
                      <li class="hoverTrigger">
                        <a href="upcoming cars.php">Upcoming cars</a>
                      <li class="hoverTrigger">
                        <a href="Electric cars.php">ELectric cars <span class="label ml-10" style='background-color: #00ff9d'>New</span>
                        </a>
                      <li class="hoverTrigger">
                        <a href="suggest me car.php">Suggest me car <span class="label ml-10" style='background-color: #00ff88'>New</span>
                        </a>
                      <li class="hoverTrigger">
                        <a href="service car.php">Service centers</a>
                    </ul>
                  </li>
                  <li>
                    <a href="#">Used  Car <i class="fa fa-angle-down fa-indicator"></i>
                    </a>
                    <ul class="drop-down-multilevel grid-col-12">
                      <li class="hoverTrigger">
                        <a href="#">Car ln your city<i class="fa fa-angle-right fa-indicator"></i>
                        </a>
                        <ul class="drop-down-multilevel grid-col-12 ">
                          <li>
                            <a href="upcoming.php">Ahmedebad </a>
                          </li>
                          <li>
                            <a href="ahmedabad.php">Bangalore </a>
                          </li>
                          <li>
                            <a href="chennal.php">Chennai </a>
                          </li>
                          <!-- <li>
                            <a href="#">Delhi ncr </a>
                          </li>
                          <li>
                            <a href="#">Hyderadad </a>
                          </li> -->
                        </ul>
                      <li class="hoverTrigger">
                        <a href="#">CarDekon Used cars<i class="fa fa-angle-right fa-indicator"></i>
                        </a>
                        <ul class="drop-down-multilevel grid-col-12 ">
                          <li>
                            <a href="carDekho Used car stores.php">carDekho Used car stores </a>
                          </li>
                          
                          <li>
                            <a href="Sell Used car.php">Sell Used car </a>
                          </li>
                        </ul>
                      <li class="hoverTrigger">
                        <a href="Used car Dealers.php">Uaed car valuation <i class="fa fa-angle-right fa-indicator"></i>
                        </a>
                        <ul class="drop-down-multilevel grid-col-12 ">
                          <li>
                            <a href="Used car Dealers.php">Used car Dealers</a>
                          </li>
                          <li>
                            <a href="Used car valuation.php">Used car valuation <span class="label ml-10" style='background-color: #ff8000'>New</span>
                            </a>
                          </li>
                        </ul>
                    </ul>
                  </li>
                  <li>
                    <a href="#">NEWS $REVLEWS <i class="fa fa-angle-down fa-indicator"></i>
                    </a>
                    <ul class="drop-down-multilevel grid-col-12">
                      <li class="hoverTrigger">
                        <a href="car News.php">car News </a>
                      <li class="hoverTrigger">
                        <a href="Feature Stories.php">Feature Stories </a>
                    </ul>
                  </li>
                  <li>
                    <a href="#">Compare cars<i class="fa fa-angle-down fa-indicator"></i>
                    </a>
                    <ul class="drop-down-multilevel grid-col-12">
                      <li class="hoverTrigger">
                        <a href="Compare car.php">Compare cars</a>
                      <li class="hoverTrigger">
                       
                    </ul>
                  </li>
                  <li>
                    <a href="car selling tips.php">Users car <i class="fa fa-angle-down fa-indicator"></i>
                    </a>
                    <ul class="drop-down-multilevel grid-col-12">
                      <li class="hoverTrigger">
                        <a href="car selling tips.php">car selling tips</a>
                      <li class="hoverTrigger">
                        <a href="Citroen c3 .php">Citroen c3 </a>
                    </ul>
                  </li>
                 
                  <li>
                  <a href="#">MORE <i class="fa fa-angle-down fa-indicator"></i>
                    </a>
                    <ul class="drop-down-multilevel grid-col-12">
                      <li class="hoverTrigger">
                        <a href="services 1.php">Services 1</a>
                      <li class="hoverTrigger">
                        <a href="about Us 2.php">About Us 2 </a>
                        <li class="hoverTrigger">
                        <a href="Contact Us.php">Contact Us </a>
                    </ul>
                    
                    
                    <div class="">
                      <div class="">
                        <div class="">
                        
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
                <ul class="menu-search-bar">
                  <li>
                    <a href="login.php"> Login </a>
                  </li>
                  <li>
                    <a href="Register.php"> Register </a>
                  </li>
                  <li>
                    <a href="sell-your-car.php" class="btn btn-theme"> Sell Your Car </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </section>
      </nav>



