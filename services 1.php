


<?php include "include/header.php" ?> 
    <section class="transparent-breadcrumb-listing" style="background: url(wp-content/img2/-gromov-4781951.jpg); background-repeat: no-repeat ; background-size: cover ;  background-position: center center ;  background-attachment: scroll; ">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="list-heading">
              <h2>Our Services</h2>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="wpb-content-wrapper">
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="padding-top-90 services-center  ">
                <div class="container">
                  <div class="row clearfix">
                    <div class="col-md-4 col-sm-6 col-xs-12 pull-left ">
                      <!--Service Block -->
                      <div class="services-grid">
                        <div class="icons icon-right">
                          <i class="flaticon-engine"></i>
                        </div>
                        <h4>Engine Upgrades</h4>
                        <p>We have the right caring, experience and dedicated professional for you.</p>
                      </div>
                      <!--Service Block -->
                      <div class="services-grid">
                        <div class="icons icon-right">
                          <i class="flaticon-settings"></i>
                        </div>
                        <h4>Car Inspection</h4>
                        <p>We have the right caring, experience and dedicated professional for you.</p>
                      </div>
                      <!--Service Block -->
                      <div class="services-grid">
                        <div class="icons icon-right">
                          <i class="flaticon-vehicle-2"></i>
                        </div>
                        <h4>Car Oil Change</h4>
                        <p>We have the right caring, experience and dedicated professional for you.</p>
                      </div>
                    </div>
                    <!--Right Column-->
                    <div class="col-md-4 col-sm-6 col-xs-12 pull-right ">
                      <!--Service Block -->
                      <div class="services-grid">
                        <div class="icons icon-right">
                          <i class="flaticon-power"></i>
                        </div>
                        <h4>Battery & Electrical</h4>
                        <p>We have the right caring, experience and dedicated professional for you.</p>
                      </div>
                      <!--Service Block -->
                      <div class="services-grid">
                        <div class="icons icon-right">
                          <i class="flaticon-vehicle"></i>
                        </div>
                        <h4>Brake Repair & Service</h4>
                        <p>We have the right caring, experience and dedicated professional for you.</p>
                      </div>
                      <!--Service Block -->
                      <div class="services-grid">
                        <div class="icons icon-right">
                          <i class="flaticon-gearstick"></i>
                        </div>
                        <h4>Transmission Check</h4>
                        <p>We have the right caring, experience and dedicated professional for you.</p>
                      </div>
                    </div>
                    <!--Image Column-->
                    <div class="col-md-4 col-sm-12 col-xs-12">
                      <figure class="wow   animated" data-wow-delay="0ms" data-wow-duration="3500ms">
                        <img class="center-block" src="wp-content/img2/car-mechanic-1.png" alt="Image Not Available" />
                      </figure>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="section-padding parallex  " style="background: rgba(0, 0, 0, 0) url(wp-content/img2/motor-group-11589868.jpg) center center no-repeat; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
                <!-- Main Container -->
                <div class="container">
                  <div class="row">
                    <div class="owl-testimonial-2  owl-carousel owl-theme">
                      <!--Testimonial Column-->
                      <div class="single_testimonial">
                        <div class="textimonial-content">
                          <h4>Awesome ! Loving It</h4>
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                        </div>
                        <div class="testimonial-meta-box">
                          <img src="wp-content/img2/1-14-90x90 (1).jpg" alt="Sonu Monu" />
                          <div class="testimonial-meta">
                            <h3>Sonu Monu</h3>
                            <p>CTO</p>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                          </div>
                        </div>
                      </div>
                      <!--Testimonial Column-->
                      <div class="single_testimonial">
                        <div class="textimonial-content">
                          <h4>Very quick and Fast Support</h4>
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                        </div>
                        <div class="testimonial-meta-box">
                          <img src="wp-content/img2/6-90x90.jpg" alt="Hania Sheikh " />
                          <div class="testimonial-meta">
                          
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                          </div>
                        </div>
                      </div>
                      <!--Testimonial Column-->
                      <div class="single_testimonial">
                        <div class="textimonial-content">
                          <h4>Done in 3 Months! Awesome</h4>
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                        </div>
                        <div class="testimonial-meta-box">
                          <img src="wp-content/img2/1-14-90x90.jpg" alt="Humayun Sarfraz " />
                          <div class="testimonial-meta">
                            
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                          </div>
                        </div>
                      </div>
                      <!--Testimonial Column-->
                      <div class="single_testimonial">
                        <div class="textimonial-content">
                          <h4>Just fabulous theme</h4>
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                        </div>
                        <div class="testimonial-meta-box">
                          <img src="wp-content/img2/1-14-90x90 (1).jpg" alt="Arslan Tariq" />
                          <div class="testimonial-meta">
                           
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="car-inspection section-padding    ">
                <div class="container">
                  <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 nopadding hidden-sm">
                      <img src="wp-content/img2/service-team.png" class="wow  img-responsive" data-wow-delay="0ms" data-wow-duration="3000ms" alt="Image Not Found" />
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12 nopadding">
                      <div class="call-to-action-detail-section">
                        <div class="heading-2">
                          <h3>Want To Sale Your Car ?</h3>
                          <h2>Car Inspection</h2>
                        </div>
                        <p>Our CarSure experts inspect the car on over 200 checkpoints so you get complete satisfaction and peace of mind before buying. </p>
                        <div class="row">
                          <ul>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Transmission
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Steering
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Engine
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Tires
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Lighting
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Interior
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Suspension
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Exterior
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Brakes
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Air Conditioning
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Engine Diagnostics
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Wheel Alignment
                            </li>
                          </ul>
                        </div>
                        <a href="# " target="%20_blank" class="btn-theme btn-lg btn"> Schedule Inspection <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="client-section  gray ">
                <div class="container">
                  <div class="row">
                    <div class="col-md-4 col-sm-12 col-xs-12">
                      <div class="margin-top-10">
                        <h3>Why Choose Us</h3>
                        <h2>Our premium Clients</h2>
                      </div>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-12">
                      <div class="brand-logo-area clients-bg">
                        <div class="clients-list owl-carousel owl-theme">
                          <div class="client-logo">
                            <a href="#" target="_blank">
                              <img src="wp-content/img2/client_4.png" class="img-responsive" alt="clients" />
                            </a>
                          </div>
                          <div class="client-logo">
                            <a href="#" target="_blank">
                              <img src="wp-content/img2/client_3.png" class="img-responsive" alt="clients" />
                            </a>
                          </div>
                          <div class="client-logo">
                            <a href="#" target="_blank">
                              <img src="wp-content/img2/client_1-1.png" class="img-responsive" alt="clients" />
                            </a>
                          </div>
                          <div class="client-logo">
                            <a href="#" target="_blank">
                              <img src="wp-content/img2/client_4 (1).png" class="img-responsive" alt="clients" />
                            </a>
                          </div>
                          <div class="client-logo">
                            <a href="#" target="_blank">
                              <img src="wp-content/img2/client_4 (1).png" class="img-responsive" alt="clients" />
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </section>
    
    <div class="clear">

    </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </section>
	<?php include 'include/footer.php';?>
   