<!doctype html>
<html lang="en-US">
  <!-- Mirrored from carspot.scriptsbundle.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Dec 2022 10:48:10 GMT -->
  <!-- Added by HTTrack -->
  <meta http-equiv="content-type" content="text/html;charset=UTF-8" />
  <!-- /Added by HTTrack -->
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <title>Car &#8211; </title>
    <meta name='robots' content='max-image-preview:large' />
    <link rel='dns-prefetch' href='http://static.addtoany.com/' />
    <link rel='dns-prefetch' href='http://www.google.com/' />
    <link rel='dns-prefetch' href='http://maps.googleapis.com/' />
    <link rel='dns-prefetch' href='http://code.iconify.design/' />
    <link rel='dns-prefetch' href='http://fonts.googleapis.com/' />
    <link rel="alternate" type="application/rss+xml" title="CarSpot - WordPress Theme &raquo; Feed" href="feed/index.html" />
    <link rel="alternate" type="application/rss+xml" title="CarSpot - WordPress Theme &raquo; Comments Feed" href="comments/feed/index.html" />
 


    <link rel='stylesheet' id='contact-form-7-css' href='wp-content/plugins/contact-form-7/includes/css/styles77e1.css?ver=5.6.4' type='text/css' media='all' />
    <link rel='stylesheet' id='woocommerce-layout-css' href='wp-content/plugins/woocommerce/assets/css/woocommerce-layout9d27.css?ver=7.1.0' type='text/css' media='all' />
    <link rel='stylesheet' id='woocommerce-smallscreen-css' href='wp-content/plugins/woocommerce/assets/css/woocommerce-smallscreen9d27.css?ver=7.1.0' type='text/css' media='only screen and (max-width: 768px)' />
    <link rel='stylesheet' id='woocommerce-general-css' href='wp-content/plugins/woocommerce/assets/css/woocommerce9d27.css?ver=7.1.0' type='text/css' media='all' />
    <style id='woocommerce-inline-inline-css' type='text/css'>
      .woocommerce form .form-row .required {
        visibility: visible;
      }
    </style>
    <link rel='stylesheet' id='popup-video-iframe-style-css' href='wp-content/themes/carspot/css/video_player6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-style-css' href='wp-content/themes/carspot/style6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='bootstrap-css' href='wp-content/themes/carspot/css/bootstrap6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-theme-tooltip-css' href='wp-content/themes/carspot/css/user-dashboard/protip.min6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-theme-css' href='wp-content/themes/carspot/css/style6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-jquery-confirm-css' href='wp-content/themes/carspot/css/user-dashboard/jquery-confirm6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-datepicker-css' href='wp-content/themes/carspot/css/datepicker.min6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-theme-slug-fonts-css' href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,400italic,600,600italic,700,700italic,900italic,900,300,300italic%7CPoppins:400,500,600&amp;subset=latin,latin-ext' type='text/css' media='all' />
    <link rel='stylesheet' id='et-line-fonts-css' href='wp-content/themes/carspot/css/et-line-fonts6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='font-awesome-css' href='wp-content/plugins/elementor/assets/lib/font-awesome/css/font-awesome.min1849.css?ver=4.7.0' type='text/css' media='all' />
    <link rel='stylesheet' id='line-awesome-css' href='wp-content/themes/carspot/css/line-awesome.min6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='animate-css' href='wp-content/themes/carspot/css/animate.min6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='flaticon-css' href='wp-content/themes/carspot/css/flaticon6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='flaticon2-css' href='wp-content/themes/carspot/css/flaticon26a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='custom-icons-css' href='wp-content/themes/carspot/css/custom_icons6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-select2-css' href='wp-content/themes/carspot/css/select2.min6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='nouislider-css' href='wp-content/themes/carspot/css/nouislider.min6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-range-slider-css' href='wp-content/themes/carspot/css/rangeslider.min6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='owl-carousel-css' href='wp-content/themes/carspot/css/owl.carousel6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='owl-theme-css' href='wp-content/themes/carspot/css/owl.theme6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-style-demo-css' href='wp-content/themes/carspot/css/style-demo6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-sb-menu-css' href='wp-content/themes/carspot/css/sb.menu6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-carspot-mystyle-css' href='wp-content/themes/carspot/css/mystyle6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-custom-css' href='wp-content/themes/carspot/css/custom6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='toastr-css' href='wp-content/themes/carspot/css/toastr.min6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-woo-css' href='wp-content/themes/carspot/css/woocommerce6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='minimal-css' href='wp-content/themes/carspot/skins/minimal/minimal6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='fancybox2-css' href='wp-content/themes/carspot/css/jquery.fancybox.min6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='slider-css' href='wp-content/themes/carspot/css/slider6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-menu-css' href='wp-content/themes/carspot/css/carspot-menu6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='responsive-media-css' href='wp-content/themes/carspot/css/responsive-media6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='carspot-responsive-css' href='wp-content/themes/carspot/css/responsive6a4d.css?ver=6.1.1' type='text/css' media='all' />
    <link rel='stylesheet' id='defualt-color-css' href='wp-content/themes/carspot/css/colors/defualt.css' type='text/css' media='all' />
    <link rel='stylesheet' id='js_composer_front-css' href='wp-content/plugins/js_composer/assets/css/js_composer.minf7be.css?ver=6.10.0' type='text/css' media='all' />
    <link rel='stylesheet' id='addtoany-css' href='wp-content/plugins/add-to-any/addtoany.mind47c.css?ver=1.16' type='text/css' media='all' />
    <script type='text/javascript' src='wp-includes/js/jquery/jquery.mina7a0.js?ver=3.6.1' id='jquery-core-js'></script>
    <script type='text/javascript' src='wp-includes/js/jquery/jquery-migrate.mind617.js?ver=3.3.2' id='jquery-migrate-js'></script>
    <script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/jquery-blockui/jquery.blockUI.min53ec.js?ver=2.7.0-wc.7.1.0' id='jquery-blockui-js'></script>
    <script type='text/javascript' id='wc-add-to-cart-js-extra'>
      /* 
					<![CDATA[ */
      var wc_add_to_cart_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
        "i18n_view_cart": "View cart",
        "cart_url": "https:\/\/carspot.scriptsbundle.com\/cart\/",
        "is_cart": "",
        "cart_redirect_after_add": "no"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/add-to-cart.min9d27.js?ver=7.1.0' id='wc-add-to-cart-js'></script>
    <script type='text/javascript' id='addtoany-core-js-before'>
      window.a2a_config = window.a2a_config || {};
      a2a_config.callbacks = [];
      a2a_config.overlays = [];
      a2a_config.templates = {};
    </script>
 
    <script type='text/javascript' async src='../static.addtoany.com/menu/page.js' id='addtoany-core-js'></script>
    <script type='text/javascript' async src='wp-content/plugins/add-to-any/addtoany.min4963.js?ver=1.1' id='addtoany-jquery-js'></script>
    <script type='text/javascript' src='wp-content/plugins/js_composer/assets/js/vendors/woocommerce-add-to-cartf7be.js?ver=6.10.0' id='vc_woocommerce-add-to-cart-js-js'></script>
    <!--[if lt IE 9]>
				<script type='text/javascript' src='https://carspot.scriptsbundle.com/wp-content/themes/carspot/js/html5shiv.min.js?ver=3.7.2' id='carspot-maxcdn1-js'></script>
				<![endif]-->
    <!--[if lt IE 9]>
				<script type='text/javascript' src='https://carspot.scriptsbundle.com/wp-content/themes/carspot/js/respond.min.js?ver=1.4.2' id='carspot-maxcdn2-js'></script>
				<![endif]-->
    <link rel="https://api.w.org/" href="wp-json/index.html" />
    <link rel="alternate" type="application/json" href="wp-json/wp/v2/pages/2555.json" />
    <link rel="EditURI" type="application/rsd+xml" title="RSD" href="xmlrpc0db0.php?rsd" />
    <link rel="wlwmanifest" type="application/wlwmanifest+xml" href="wp-includes/wlwmanifest.xml" />
    <meta name="generator" content="WordPress 6.1.1" />
    <meta name="generator" content="WooCommerce 7.1.0" />
    <link rel="canonical" href="index.html" />
    <link rel='shortlink' href='index.html' />
    <link rel="alternate" type="application/json+oembed" href="wp-json/oembed/1.0/embedbb5b.json?url=https%3A%2F%2Fcarspot.scriptsbundle.com%2F" />
    <link rel="alternate" type="text/xml+oembed" href="wp-json/oembed/1.0/embed7fb4?url=https%3A%2F%2Fcarspot.scriptsbundle.com%2F&amp;format=xml" />
    <meta name="framework" content="Redux 4.2.14" />
    <noscript>
      <style>
        .woocommerce-product-gallery {
          opacity: 1 !important;
        }
      </style>
    </noscript>
    <meta name="generator" content="Powered by WPBakery Page Builder - drag and drop page builder for WordPress." />
    <style type="text/css" id="wp-custom-css">

   @media(min-width:320px) and (max-width:767px) {
        .section-container-left {
          padding-top: 200px;
        }
      }

      @media(min-width:320px) and (max-width:767px) {
        #wrapper .sidebar {
          position: absolute !important;
        }
      }

      .services-icons-section {
        left: auto !important;
      }
    </style>
    <noscript>
      <style>
        .wpb_animate_when_almost_visible {
          opacity: 1;
        }
      </style>
    </noscript>
  </head>
  <body >
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="loading" id="sb_loading">Loading&#8230;</div>
    <div class="sb-top-bar_notification">
      <a href="javascript:void(0)"> For a better experience please change your browser to CHROME, FIREFOX, OPERA or Internet Explorer. </a>
    </div>
    <div class="colored-header">
      <!-- Navigation Menu -->
      <div class="clearfix"></div>
      <!-- menu start -->
      <nav id="menu-1" class="mega-menu header-transparent ">
        <!-- menu list items container -->
        <section class="menu-list-items">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 col-md-12">
                <!-- menu logo -->
                <ul class="menu-logo">
                  <li>
                    <a href="index.php">
                    <img src="wp-content/img/99.png" style="
    width: 113px;
    height: 73px;
    height: 100;
">
                    </a>
                  </li>
                </ul>
                <!-- menu links -->
                <!-- menu links -->
                <ul class="menu-links">
                  <li>
                    <a href="index.php">Home <i class="fa fa-angle-down fa-indicator"></i>
                    </a>
                    <ul class="drop-down-multilevel grid-col-12">
                      <li class="hoverTrigger">
                        <a href="cars.php">Search New cars </a>
                      <li class="hoverTrigger">
                        <a href="popular brand.php">Popular Brands </a>
                      <li class="hoverTrigger">
                        <a href="latest care.php">Latest cars</a>
                      <li class="hoverTrigger">
                        <a href="popular cars.php">Popular cars</a>
                      <li class="hoverTrigger">
                        <a href="upcoming cars.php">Upcoming cars</a>
                      <li class="hoverTrigger">
                        <a href="Electric cars.php">ELectric cars <span class="label ml-10" style='background-color: #00ff9d'>New</span>
                        </a>
                      <li class="hoverTrigger">
                        <a href="suggest me car.php">Suggest me a car <span class="label ml-10" style='background-color: #00ff88'>New</span>
                        </a>
                      <li class="hoverTrigger">
                        <a href="service car.php">Service centers </a>
                    </ul>
                  </li>
                  <li>
                    <a href="#">USed  CAr <i class="fa fa-angle-down fa-indicator"></i>
                    </a>
                    <ul class="drop-down-multilevel grid-col-12">
                      <li class="hoverTrigger">
                        <a href="#">Car ln your city<i class="fa fa-angle-right fa-indicator"></i>
                        </a>
                        <ul class="drop-down-multilevel grid-col-12 ">
                          <li>
                            <a href="upcoming.php">Ahmedebad </a>
                          </li>
                          <li>
                            <a href="ahmedabad.php">Bangalore </a>
                          </li>
                          <li>
                            <a href="chennal.php">Chennai </a>
                          </li>
                          <li>
                            <!-- <a href="#">Delhi ncr </a>
                          </li>
                          <li>
                            <a href="#">Hyderadad </a>
                          </li> -->
                        </ul>
                      <li class="hoverTrigger">
                        <a href="#">CarDekon Used cars<i class="fa fa-angle-right fa-indicator"></i>
                        </a>
                        <ul class="drop-down-multilevel grid-col-12 ">
                          <li>
                            <a href="carDekho Used car stores.php">carDekho Used car stores </a>
                          </li>
                          
                          <li>
                            <a href="Sell Used car.php">Sell Used car </a>
                          </li>
                        </ul>
                      <li class="hoverTrigger">
                        <a href="Used car Dealers.php">Uaed car valuation <i class="fa fa-angle-right fa-indicator"></i>
                        </a>
                        <ul class="drop-down-multilevel grid-col-12 ">
                          <li>
                            <a href="Used car Dealers.php">Used car Dealers</a>
                          </li>
                          <li>
                            <a href="Used car valuation.php">Used car valuation<span class="label ml-10" style='background-color: #ff8000'>New</span>
                            </a>
                          </li>
                        </ul>
                    </ul>
                  </li>
                  <li>
                    <a href="#">NEWS $REVLEWS <i class="fa fa-angle-down fa-indicator"></i>
                    </a>
                    <ul class="drop-down-multilevel grid-col-12">
                      <li class="hoverTrigger">
                        <a href="car News.php">car News </a>
                      <li class="hoverTrigger">
                        <a href="Feature Stories.php">Feature Stories </a>
                    </ul>
                  </li>
                  <li>
                    <a href="#">Compare cars <i class="fa fa-angle-down fa-indicator"></i>
                    </a>
                    <ul class="drop-down-multilevel grid-col-12">
                      <li class="hoverTrigger">
                        <a href="Compare car.php">Compare cars</a>
                      <li class="hoverTrigger">
                        
                    </ul>
                  </li>
                  <li>
                    <a href="car selling tips.php">Users car<i class="fa fa-angle-down fa-indicator"></i>
                    </a>
                    <ul class="drop-down-multilevel grid-col-12">
                      <li class="hoverTrigger">
                        <a href="car selling tips.php">car selling tips </a>
                      <li class="hoverTrigger">
                        <a href="Citroen c3 .php">Citroen c3 </a>
                    </ul>
                  </li>
                  <li>
                  <a href="#">MORE <i class="fa fa-angle-down fa-indicator"></i>
                    </a>
                    <ul class="drop-down-multilevel grid-col-12">
                      <li class="hoverTrigger">
                        <a href="services 1.php">Services 1</a>
                      <li class="hoverTrigger">
                        <a href="about Us 2.php">About Us 2 </a>
                        <li class="hoverTrigger">
                        <a href="Contact Us.php">Contact Us </a>
                    </ul>
                    
                    
                    <div class="">
                      <div class="">
                        <div class="">
                        
                        </div>
                      </div>
                    </div>
                  </li>
                </ul>
                <ul class="menu-search-bar">
                  <li>
                    <a href="login.php"> Login </a>
                  </li>
                  <li>
                    <a href="Register.php"> Register </a>
                  </li>
                  <li>
                    <a href="sell-your-car.php" class="btn btn-theme"> Sell Your Car </a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </section>
      </nav>
      <!-- menu end -->
    </div>
    <section class="wpb-content-wrapper">
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="hero-section section-style" style="background: rgba(0, 0, 0, 0) url(wp-content/uploads/2019/01/2012_porsche_panamera_turbo-1600x900.jpg) no-repeat scroll center center / cover;-webkit-background-size: cover;-moz-background-size: cover;-o-background-size: cover;background-size: cover;">
                <div class="container">
                  <div class="row">
                    <div class="col-lg-7 col-md-7 col-sm-6 col-xs-12">
                      <div class="hero-text">
                        <h1> Find Your right Car</h1>
                        <p> Buy Used cars at Best price</p>
                        <ul>
                          <li>
                            <i class="fa fa-hand-o-right"></i> Post your car's ad for free in 30 seconds.
                          </li>
                          <li>
                            <i class="fa fa-hand-o-right"></i> Get authentic offers from verified buyers.
                          </li>
                          <li>
                            <i class="fa fa-hand-o-right"></i> Sell your car faster than others at a better price.
                          </li>
                        </ul>
                        <a href="#" class="btn btn-theme"> Post your car </a>
                      </div>
                    </div>
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xs-12">
                      <img src="wp-content/themes/carspot/images/hero-form-shedow.png" class="hero-form-top-style" alt="image not found" />
                      <div class="hero-form">
                        <div class="hero-form-heading">
                          <h2> Find your right car</h2>
                          <!-- <p>Used Cars for Sale in Your Town Search Now!</p> -->
                        </div>
                        <form action="#">
                          <div class="form-group">
                            <!-- <label>Keyword</label>
                            <input type="text" class="form-control" autocomplete="off" id="autocomplete-dynamic" name="ad_title" placeholder="What are you looking for..." /> -->
                          </div>
                          <div class="form-group">
                            <label>Select Budget</label>
                            <select class=" form-control make" name="cat_id">
                              <option label="Select Budget" value="">Select Budget</option>
                              <option value="50">Alfa Romeo (0)</option>
                              <option value="1-5">&nbsp;&nbsp; - &nbsp;Lakh </option>
                              <option value="5-10">&nbsp;&nbsp; - &nbsp;lakh </option>
                              <option value="10-15">&nbsp;&nbsp; - &nbsp;lakh </option>
                              <option value="15-20">&nbsp;&nbsp; - &nbsp;lahk </option>
                              <option value="20-35">&nbsp;&nbsp; - &nbsp;lakh </option>
                              <option value="35.50">&nbsp;&nbsp; - &nbsp;lakh </option>
                              <option value="">&nbsp;&nbsp; - &nbsp;Above 1 crore </option>
                             
                            </select>
                          </div>
                          <div class="form-group">
                            <label>All vehicle Types</label>
                            <select class=" form-control make" name="year_from">
                              <option label="Select Year : Any Year" value="">All vehicle types</option>
                              <option value="2010">Hatchbck</option>
                              <option value="2011">Seden</option>
                              <option value="2012">suv</option>
                              <option value="2013">muv</option>
                              <option value="2014">luxury</option>
                              <option value="2015">super luxury</option>
                              <option value="2016">convertible</option>
                              <option value="2017">Hybrid</option>
                              <option value="2017">coupe</option>
                            </select>
                          </div>
                          <div class="form-group">
                            <button type="submit" class="btn btn-lg btn-theme btn-block">Search Now</button>
                          </div>
                        </form>
                      </div>
                    </div>
                    <img src="wp-content/img/hector-exterior-right-front-three-quarter-166303-removebg-preview.png" class="hero-car wow slideInLeft img-responsive" data-wow-delay="0ms" data-wow-duration="3000ms" alt="Image not found" style="
    left: revert;
">
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      

      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="custom-padding ">
                <!-- Main Container -->
                <div class="container">
                  <!-- Row -->
                  <div class="row">
                    <div class="heading-panel">
                      <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                        <!-- Main Title -->
                        <h2>The most <span class="heading-color">searched cars</span> </h2>
                        <!-- Short Description -->
                        <p class="heading-text"></p>
                      </div>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                      <div class="row">
                        <div class="grid-style-2">
                          <div class="posts-masonry ads-for-home">
                            <div class="col-md-4  col-lg-4 col-sm-6 col-xs-12" id=".holder-1375">
                              <div class="category-grid-box-1">
                                <div class="featured-ribbon">
                                  <span>Featured</span>
                                </div>
                                <div class="image">
                                  <a href="#">
                                    <img src="wp-content/img2/o-12984628 (1).jpeg" alt="2017 Maserati Ghibli SQ4 Blue 1,695 Miles" class="img-responsive" />
                                  </a>
                                  <div class="price-tag">
                                    <div class="price">
                                      <span>Rs.9.92 - <span class=""> (8.85 Lakh)</span>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="short-description-1 clearfix">
                                  <div class="category-title">
                                    <span class="padding_cats">
                                      <a href="ad_category/audi/index.html">Audi</a>
                                    </span>
                                    <span class="padding_cats">
                                      <a href="ad_category/a3/index.html">A3</a>
                                    </span>
                                  </div>
                                  <h3>
                                    <a href="#">Maruti Swift</a>
                                  </h3>
                                  <p class="location">
                                    <i class="fa fa-map-marker"></i>
                                    <a href="#">California</a>, <a href="#">United States</a>
                                  </p>
                                  <ul class="list-unstyled">
                                    <li>
                                      <i class="flaticon-gas-station-1"></i>Petrol
                                    </li>
                                    <li>
                                      <i class="flaticon-dashboard"></i>1,694 km
                                    </li>
                                    <li>
                                      <i class="flaticon-engine-2"></i>3000 cc
                                    </li>
                                    <li>
                                      <i class="flaticon-car-2"></i>Sedan
                                    </li>
                                    <li>
                                      <i class="flaticon-cogwheel-outline"></i>Blue
                                    </li>
                                  </ul>
                                </div>
                                <div class="ad-info-1">
                                  <p>
                                    <!-- <i class="flaticon-calendar"></i> &nbsp; <span>January 24, 2019</span> -->
                                  </p>
                                  <ul class="pull-right ">
                                    <li>
                                      <a data-toggle="tooltip" data-placement="top" data-original-title="Save Ad" href="javascript:void(0);" class="save-ad" data-adid="1375">
                                        <i class="flaticon-like-1"></i>
                                      </a>
                                      <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                                    </li>
                                    <li>
                                      <a href="#">
                                        <i class="flaticon-message"></i>
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <!-- Listing Ad Grid -->
                            </div>
                            <div class="col-md-4  col-lg-4 col-sm-6 col-xs-12" id=".holder-1366">
                              <div class="category-grid-box-1">
                                <div class="featured-ribbon">
                                  <span>Featured</span>
                                </div>
                                <div class="image">
                                  <a href="#">
                                    <img src="wp-content/img2/pexels-mike-b-215528.jpg" alt="Maserati Ghibli SQ4 Blue 1,695 Miles 2017" class="img-responsive" />
                                  </a>
                                  <div class="price-tag">
                                    <div class="price">
                                      <span>Rs.60.49 -  <span class=""> (9.71 Lakh*)</span>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="short-description-1 clearfix">
                                  <div class="category-title">
                                    <span class="padding_cats">
                                      <a href="#">Audi</a>
                                    </span>
                                    <span class="padding_cats">
                                      <a href="#">A4</a>
                                    </span>
                                  </div>
                                  <h3>
                                    <a href="#">Maruti Baleno..</a>
                                  </h3>
                                  <p class="location">
                                    <i class="fa fa-map-marker"></i>
                                    <a href="#">California</a>, <a href="#">United States</a>
                                  </p>
                                  <ul class="list-unstyled">
                                    <li>
                                      <i class="flaticon-gas-station-1"></i>Petrol
                                    </li>
                                    <li>
                                      <i class="flaticon-dashboard"></i>30.61 km/kg
                                    </li>
                                    <li>
                                      <i class="flaticon-engine-2"></i>1197 cc
                                    </li>
                                    <li>
                                      <i class="flaticon-car-2"></i>yes
                                    </li>
                                    <li>
                                      <i class="flaticon-cogwheel-outline"></i>318
                                    </li>
                                  </ul>
                                </div>
                                <div class="ad-info-1">
                                  <p>
                                    <!-- <i class="flaticon-calendar"></i> &nbsp; <span>January 24, 2019</span> -->
                                  </p>
                                  <ul class="pull-right ">
                                    <li>
                                      <a data-toggle="tooltip" data-placement="top" data-original-title="Save Ad" href="javascript:void(0);" class="save-ad" data-adid="1366">
                                        <i class="flaticon-like-1"></i>
                                      </a>
                                      <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                                    </li>
                                    <li>
                                      <a href="#">
                                        <i class="flaticon-message"></i>
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <!-- Listing Ad Grid -->
                            </div>
                            <div class="col-md-4  col-lg-4 col-sm-6 col-xs-12" id=".holder-1362">
                              <div class="category-grid-box-1">
                                <div class="featured-ribbon">
                                  <span>Featured</span>
                                </div>
                                <div class="image">
                                  <a href="#">
                                    <img src="wp-content/img2/pex11158760 (1).jpeg" alt="2017 Maserati Ghibli SQ4 Blue 1,694 Miles" class="img-responsive" />
                                  </a>
                                  <div class="price-tag">
                                    <div class="price">
                                      <span>Rs.12.92 -  <span class=""> 8.51 Lakh*</span>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="short-description-1 clearfix">
                                  <div class="category-title">
                                    <span class="padding_cats">
                                      <a href="#">Austin</a>
                                    </span>
                                    <span class="padding_cats">
                                      <a href="#">Mini</a>
                                    </span>
                                  </div>
                                  <h3>
                                    <a href="#">Renault Triber.</a>
                                  </h3>
                                  <p class="location">
                                    <i class="fa fa-map-marker"></i>
                                    <a href="#">California</a>, <a href="#">United States</a>
                                  </p>
                                  <ul class="list-unstyled">
                                    <li>
                                      <i class="flaticon-gas-station-1"></i>Petrol
                                    </li>
                                    <li>
                                      <i class="flaticon-dashboard"></i>20.0 kmpl
                                    </li>
                                    <li>
                                      <i class="flaticon-engine-2"></i>3500 cc
                                    </li>
                                    <li>
                                      <i class="flaticon-car-2"></i>Sedan
                                    </li>
                                    <li>
                                      <i class="flaticon-cogwheel-outline"></i>Blue
                                    </li>
                                  </ul>
                                </div>
                                <div class="ad-info-1">
                                  <p>
                                    <!-- <i class="flaticon-calendar"></i> &nbsp; <span>January 24, 2019</span> -->
                                  </p>
                                  <ul class="pull-right ">
                                    <li>
                                      <a data-toggle="tooltip" data-placement="top" data-original-title="Save Ad" href="javascript:void(0);" class="save-ad" data-adid="1362">
                                        <i class="flaticon-like-1"></i>
                                      </a>
                                      <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                                    </li>
                                    <li>
                                      <a href="#">
                                        <i class="flaticon-message"></i>
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <!-- Listing Ad Grid -->
                            </div>
                            <div class="col-md-4  col-lg-4 col-sm-6 col-xs-12" id=".holder-1554">
                              <div class="category-grid-box-1">
                                <div class="featured-ribbon">
                                  <span>Featured</span>
                                </div>
                                <div class="image">
                                  <a href="#">
                                    <img src="wp-content/img2/pexels-photo-13061032.jpeg" alt="2017 Ford Mustang" class="img-responsive" />
                                  </a>
                                  <div class="price-tag">
                                    <div class="price">
                                      <span>Rs.33.50 <span class=""> (Lakh*)</span>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="short-description-1 clearfix">
                                  <div class="category-title">
                                    <span class="padding_cats">
                                      <a href="#">Ford</a>
                                    </span>
                                    <span class="padding_cats">
                                      <a href="#">Mustang</a>
                                    </span>
                                  </div>
                                  <h3>
                                    <a href="#">Volkswagen Tiguan</a>
                                  </h3>
                                  <p class="location">
                                    <i class="fa fa-map-marker"></i>
                                    <a href="#">Fort Lauderdale</a>, <a href="#">Florida</a>, <a href="#">United States</a>
                                  </p>
                                  <ul class="list-unstyled">
                                    <li>
                                      <i class="flaticon-gas-station-1"></i>Petrol
                                    </li>
                                    <li>
                                      <i class="flaticon-dashboard"></i>12,454 km
                                    </li>
                                    <li>
                                      <i class="flaticon-engine-2"></i>2000 cc
                                    </li>
                                    <li>
                                      <i class="flaticon-car-2"></i>Coupe
                                    </li>
                                    <li>
                                      <i class="flaticon-cogwheel-outline"></i>White
                                    </li>
                                  </ul>
                                </div>
                                <div class="ad-info-1">
                                  <p>
                                    <!-- <i class="flaticon-calendar"></i> &nbsp; <span>January 24, 2019</span> -->
                                  </p>
                                  <ul class="pull-right ">
                                    <li>
                                      <a data-toggle="tooltip" data-placement="top" data-original-title="Save Ad" href="javascript:void(0);" class="save-ad" data-adid="1554">
                                        <i class="flaticon-like-1"></i>
                                      </a>
                                      <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                                    </li>
                                    <li>
                                      <a href="#">
                                        <i class="flaticon-message"></i>
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <!-- Listing Ad Grid -->
                            </div>
                            <div class="col-md-4  col-lg-4 col-sm-6 col-xs-12" id=".holder-1552">
                              <div class="category-grid-box-1">
                                <div class="featured-ribbon">
                                  <span>Featured</span>
                                </div>
                                <div class="image">
                                  <a href="#">
                                    <img src="wp-content/img2/pexels-hyundai-motor-group-11320632 (1).jpg" alt="2010 Chevrolet Camaro" class="img-responsive" />
                                  </a>
                                  <div class="price-tag">
                                    <div class="price">
                                      <span>Rs.8.41  <span class=""> (- 14.07 Lakh*)</span>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="short-description-1 clearfix">
                                  <div class="category-title">
                                    <span class="padding_cats">
                                      <a href="#">Corvette</a>
                                    </span>
                                  </div>
                                  <h3>
                                    <a href="#">Mahindra XUV300</a>
                                  </h3>
                                  <p class="location">
                                    <i class="fa fa-map-marker"></i>
                                    <a href="#">Fort Lauderdale</a>, <a href="#">Florida</a>, <a href="#">United States</a>
                                  </p>
                                  <ul class="list-unstyled">
                                    <li>
                                      <i class="flaticon-gas-station-1"></i>Petrol
                                    </li>
                                    <li>
                                      <i class="flaticon-dashboard"></i>23 km
                                    </li>
                                    <li>
                                      <i class="flaticon-engine-2"></i>1500 cc
                                    </li>
                                    <li>
                                      <i class="flaticon-car-2"></i>Coupe
                                    </li>
                                    <li>
                                      <i class="flaticon-cogwheel-outline"></i>White
                                    </li>
                                  </ul>
                                </div>
                                <div class="ad-info-1">
                                  <p>
                                    <!-- <i class="flaticon-calendar"></i> &nbsp; <span>January 24, 2019</span> -->
                                  </p>
                                  <ul class="pull-right ">
                                    <li>
                                      <a data-toggle="tooltip" data-placement="top" data-original-title="Save Ad" href="javascript:void(0);" class="save-ad" data-adid="1552">
                                        <i class="flaticon-like-1"></i>
                                      </a>
                                      <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                                    </li>
                                    <li>
                                      <a href="#">
                                        <i class="flaticon-message"></i>
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <!-- Listing Ad Grid -->
                            </div>
                            <div class="col-md-4  col-lg-4 col-sm-6 col-xs-12" id=".holder-81">
                              <div class="category-grid-box-1">
                                <div class="featured-ribbon">
                                  <span>Featured</span>
                                </div>
                                <div class="image">
                                  <a href="#" class="play-video">
                                    <img src="wp-content/img2/pexels-hyundai-motor-group-13163937.jpg" alt="Icon" />
                                  </a>
                                  <a href="#">
                                    <img src="wp-content/img2/pexels-hyundai-motor-group-13163937.jpg" alt="Mustang Shelby GT350 Coupe" class="img-responsive" />
                                  </a>
                                  <div class="price-tag">
                                    <div class="price">
                                      <span>$ 17,000</span>
                                    </div>
                                  </div>
                                </div>
                                <div class="short-description-1 clearfix">
                                  <div class="category-title">
                                    <span class="padding_cats">
                                      <a href="#">Ford</a>
                                    </span>
                                    <span class="padding_cats">
                                      <a href="#">Mustang</a>
                                    </span>
                                  </div>
                                  <h3>
                                    <a href="#">Mustang Shelby GT350...</a>
                                  </h3>
                                  <p class="location">
                                    <i class="fa fa-map-marker"></i>
                                    <a href="#">District of Columbia</a>, <a href="#">United States</a>
                                  </p>
                                  <ul class="list-unstyled">
                                    <li>
                                      <i class="flaticon-gas-station-1"></i>Diesel
                                    </li>
                                    <li>
                                      <i class="flaticon-dashboard"></i>5,400 km
                                    </li>
                                    <li>
                                      <i class="flaticon-engine-2"></i>3500 cc
                                    </li>
                                    <li>
                                      <i class="flaticon-car-2"></i>Coupe
                                    </li>
                                    <li>
                                      <i class="flaticon-cogwheel-outline"></i>Black
                                    </li>
                                  </ul>
                                </div>
                                <div class="ad-info-1">
                                  <p>
                                    <!-- <i class="flaticon-calendar"></i> &nbsp; <span>June 20, 2017</span> -->
                                  </p>
                                  <ul class="pull-right ">
                                    <li>
                                      <a data-toggle="tooltip" data-placement="top" data-original-title="Save Ad" href="javascript:void(0);" class="save-ad" data-adid="81">
                                        <i class="flaticon-like-1"></i>
                                      </a>
                                      <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                                    </li>
                                    <li>
                                      <a href="#">
                                        <i class="flaticon-message"></i>
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <!-- Listing Ad Grid -->
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="text-center">
                      <div class="load-more-btn"></div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
<!-- <---html css---->


      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="custom-padding ">
                <!-- Main Container -->
                <div class="container">
                  <!-- Row -->
                  <div class="row">
                    <div class="heading-panel">
                      <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                        <!-- Main Title -->
                        <h2>The most <span class="heading-color">searched cars</span> </h2>
                        <!-- Short Description -->
                        <p class="heading-text">Eu delicata rationibus usu. Vix te putant utroque, ludus fabellas duo eu, his dico ut debet consectetuer.</p>
                      </div>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                      <div class="row">
                        <div class="posts-masonry">
                          <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                            <div class="row">
                              <div class="trending-ads">
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                  <div class="trending-single-ad">
                                    <div class="img-container">
                                      <a href="#">
                                        <img src="wp-content/img/hyundai0.webp" alt="2017 Maserati Ghibli SQ4 Blue 1,695 Miles" class="img-responsive" />
                                      </a>
                                      <span class="ad-status">Featured</span>
                                      <div class="total-images">
                                        <strong></strong> 
                                      </div>
                                      <div class="quick-view">
                                        <a href="#" class="view-button">
                                          <i class="fa fa-search"></i>
                                        </a>
                                      </div>
                                    </div>
                                    <div class="trending-ad-detail">
                                      <span class="price">Rs.10.45 -   <span class=""> (19.65 Lakh*)</span>
                                      </span>
                                      <h3>
                                        <a href="#"></a>
                                      </h3>
                                      <p>
                                        <a href="#">
                                          <i class="fa fa-map-marker"></i>
                                        </a>
                                        <a href="#">All models mentioned on</a>, <a href="#"></a>
                                      </p>
                                      <p>
                                        <i class="fa fa-clock-o">Petrol
21.11 kmpl</i> 
                                      </p>
                                      <ul>
                                        <li>
                                          <a href="javascript:void(0)">
                                            <i class="flaticon-gas-station-1"></i>Petrol </a>
                                        </li>
                                        <li>
                                          <a href="javascript:void(0)">
                                            <i class="flaticon-dashboard"></i>1694 km </a>
                                        </li>
                                        <li>
                                          <a href="javascript:void(0)">
                                            <i class="flaticon-engine-2"></i>3,000 cc </a>
                                        </li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                  <div class="trending-single-ad">
                                    <div class="img-container">
                                      <a href="#">
                                        <img src="wp-content/img/front-left-side-47 (6).jpg" alt="Maserati Ghibli SQ4 Blue 1,695 Miles 2017" class="img-responsive" />
                                      </a>
                                      <span class="ad-status">Featured</span>
                                      <div class="total-images">
                                        <strong></strong> 
                                      </div>
                                      <div class="quick-view">
                                        <a href="#" class="view-button">
                                          <i class="fa fa-search"></i>
                                        </a>
                                      </div>
                                    </div>
                                    <div class="trending-ad-detail">
                                      <span class="price">Rs.14.43 <span class=""> (- 20.36 Lakh* )</span>
                                      </span>
                                      <h3>
                                        <a href="#">MG Hector.</a>
                                      </h3>
                                      <p>
                                        <a href="#">
                                          <i class="fa fa-map-marker"></i>
                                        </a>
                                        <a href="#">California</a>, <a href="#">United States</a>
                                      </p>
                                      <p>
                                        <i class="fa fa-clock-o"></i> 
                                      </p>
                                      <ul>
                                        <li>
                                          <a href="javascript:void(0)">
                                            <i class="flaticon-gas-station-1"></i>Petrol </a>
                                        </li>
                                        <li>
                                          <a href="javascript:void(0)">
                                            <i class="flaticon-dashboard"></i>1695 km </a>
                                        </li>
                                        <li>
                                          <a href="javascript:void(0)">
                                            <i class="flaticon-engine-2"></i>2,000 cc </a>
                                        </li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                  <div class="trending-single-ad">
                                    <div class="img-container">
                                      <a href="#">
                                        <img src="wp-content/img/90fb7abb94138c9d45a60fd7280a1e90.webp" alt="2017 Maserati Ghibli SQ4 Blue 1,694 Miles" class="img-responsive" />
                                      </a>
                                      <span class="ad-status">Featured</span>
                                      <div class="total-images">
                                        <strong></strong> 
                                      </div>
                                      <div class="quick-view">
                                        <a href="#" class="view-button">
                                          <i class="fa fa-search"></i>
                                        </a>
                                      </div>
                                    </div>
                                    <div class="trending-ad-detail">
                                      <span class="price">$₹ 10.03 
<span class=""> (Lakh)</span>
                                      </span>
                                      <h3>
                                        <a href="#">2017 Maserati Ghibli...</a>
                                      </h3>
                                      <p>
                                        <a href="#">
                                          <i class="fa fa-map-marker"></i>
                                        </a>
                                        <a href="#">86,226 kms • </a>,Diesel • Manual  <a href="#"></a>
                                      </p>
                                      <p>
                                        <i class="fa fa-clock-o"></i> 2017 Jeep Compass 2.0
                                      </p>
                                      <ul>
                                        <li>
                                          <a href="javascript:void(0)">
                                            <i class="flaticon-gas-station-1"></i>Petrol </a>
                                        </li>
                                        <li>
                                          <a href="javascript:void(0)">
                                            <i class="flaticon-dashboard"></i>1694 km </a>
                                        </li>
                                        <li>
                                          <a href="javascript:void(0)">
                                            <i class="flaticon-engine-2"></i>3,500 cc </a>
                                        </li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                  <div class="trending-single-ad">
                                    <div class="img-container">
                                      <a href="#">nav  <img src="wp-content/img/6387522e3dc81.webp" alt="2017 Ford Mustang" class="img-responsive" /></a><span class="ad-status"></span><div class="total-images">
                                        <strong></strong> 
                                      </div>
                                      <div class="quick-view">
                                        <a href="#" class="view-button">
                                          <i class="fa fa-search"></i>
                                        </a>
                                      </div>
                                    </div>
                                    <div class="trending-ad-detail">
                                      <span class="price">Rs.33.50 -<span class=""> ( 33.50 Lakh*)</span>
                                      </span>
                                      <h3>
                                        <a href="#">2017 Ford Mustang</a>
                                      </h3>
                                      <p>
                                        <a href="#">
                                          <i class="fa fa-map-marker"></i>
                                        </a>
                                        <a href="#">Fort Lauderdale</a>, <a href="#">Florida</a>, <a href="#">United States</a>
                                      </p>
                                      <p>
                                        <i class="fa fa-clock-o"></i> 
                                      </p>
                                      <ul>
                                        <li>
                                          <a href="javascript:void(0)">
                                            <i class="flaticon-gas-station-1"></i>Petrol </a>
                                        </li>
                                        <li>
                                          <a href="javascript:void(0)">
                                            <i class="flaticon-dashboard"></i>12454 km </a>
                                        </li>
                                        <li>
                                          <!-- <a href="javascript:void(0)">
                                            <i class="flaticon-engine-2"></i>2,000 cc </a> -->
                                        </li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                  <div class="trending-single-ad">
                                    <div class="img-container">
                                      <a href="#">
                                        <img src="wp-content/IMG/front-left-side-47 (14).jpg" alt="2010 Chevrolet Camaro" class="img-responsive" />
                                      </a>
                                      <span class="ad-status">Featured</span>
                                      <div class="total-images">
                                      
                                      </div>
                                      <div class="quick-view">
                                        <a href="#" class="view-button">
                                          <i class="fa fa-search"></i>
                                        </a>
                                      </div>
                                    </div>
                                    <div class="trending-ad-detail">
                                      <span class="price">Rs.4.64 <span class=""> (- 5.99 Lakh* )</span>
                                      </span>
                                      <h3>
                                        <a href="">Renault KWID</a>
                                      </h3>
                                      <p>
                                        <a href="#">
                                          <i class="fa fa-map-marker"></i>
                                        </a>
                                        <a href="#">Fort Lauderdale</a>, <a href="#">Florida</a>, <a href="#">United States</a>
                                      </p>
                                      <p>
                                        <i class="fa fa-clock-o"></i> January 24, 2019
                                      </p>
                                      <ul>
                                        <li>
                                          <a href="javascript:void(0)">
                                            <i class="flaticon-gas-station-1"></i>Petrol </a>
                                        </li>
                                        <li>
                                          <a href="javascript:void(0)">
                                            <i class="flaticon-dashboard"></i>23,000 km </a>
                                        </li>
                                        <li>
                                          <a href="javascript:void(0)">
                                            <i class="flaticon-engine-2"></i>22.25 kmpl </a>
                                        </li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                                <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
                                  <div class="trending-single-ad">
                                    <div class="img-container">
                                      <a href="#">
                                        <img src="wp-content/IMG/front-left-side-47 (15).jpg" alt="Mustang Shelby GT350 Coupe" class="img-responsive" />
                                      </a>
                                      <span class="ad-status">Featured</span>
                                      <div class="total-images">
                                        <strong></strong>
                                      </div>
                                      <div class="quick-view">
                                        <a href="#" class="view-button">
                                          <i class="fa fa-search"></i>
                                        </a>
                                      </div>
                                    </div>
                                    <div class="trending-ad-detail">
                                      <span class="price">Rs.2.00 Cr*</span>
                                      <h3>
                                        <a href="#">BMW XMChange car.</a>
                                      </h3>
                                      <p>
                                        <a href="#">
                                          <i class="fa fa-map-marker"></i>
                                        </a>
                                        <a href="#">District of Columbia</a>, <a href="#">United States</a>
                                      </p>
                                      <p>
                                        <i class="fa fa-clock-o"></i>Expected Launch - Dec 10, 2022
                                      </p>
                                      <ul>
                                        <li>
                                          <a href="javascript:void(0)">
                                            <i class="flaticon-gas-station-1"></i>Diesel </a>
                                        </li>
                                        <li>
                                          <a href="javascript:void(0)">
                                            <i class="flaticon-dashboard"></i>5400 km </a>
                                        </li>
                                        <li>
                                          <a href="javascript:void(0)">
                                            <i class="flaticon-engine-2"></i>3,500 cc </a>
                                        </li>
                                      </ul>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                        </div>
                      </div>
                      <div class="text-center">
                        <div class="load-more-btn"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
 
    

      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="car-inspection section-padding    ">
                <div class="container">
                  <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 nopadding hidden-sm">
                      <img src="wp-content/uploads/2017/06/car-in-red.png" class="wow slideInLeft img-responsive" data-wow-delay="0ms" data-wow-duration="3000ms" alt="Image Not Found" />
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12 nopadding">
                      <div class="call-to-action-detail-section">
                        <div class="heading-2">
                          <h3>Want To Sale Your Car ?</h3>
                          <h2>Car Inspection</h2>
                        </div>
                        <p>Our CarSure experts inspect the car on over 200 checkpoints so you get complete satisfaction and peace of mind before buying. </p>
                        <div class="row">
                          <ul>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Transmission
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Steering
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Engine
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Tires
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Lighting
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Interior
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Suspension
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Exterior
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Brakes
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Air Conditioning
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Engine Diagnostics
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Wheel Alignment
                            </li>
                          </ul>
                        </div>
                        <a href="# " target="%20_blank" class="btn-theme btn-lg btn"> Schedule Inspection <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="footer-transparent" style=" background: url(wp-content/uploads/2018/04/1920x400.html); background-repeat: no-repeat; background-attachment: scroll; background-size: cover; background-position: bottom center;">
      <div class="container">
        <div class="row">
          <div class="col-lg-5 col-sm-6 col-md-5 col-xs-12">
            <div class="footer-block">
            <img src="wp-content/img/99.png" style="
    width: 113px;
    height: 73px;
    height: 100;
">
              <p>CarSpot Automotive Car Classified WordPress Theme is flexible and powerful Car Classified wordpress Theme for your all automotive &amp; car listing business needs.</p>
              <div class="social-bar">
                <ul>
                  <li>
                    <a class="fa fa-facebook" href="#/"></a>
                  </li>
                  <li>
                    <a class="fa fa-twitter " href="#"></a>
                  </li>
                  <li>
                    <a class="fa fa-linkedin " href="#"></a>
                  </li>
                  <li>
                    <a class="fa fa-google-plus" href="#"></a>
                  </li>
                  <li>
                    <a class="fa fa-youtube-play" href="#"></a>
                  </li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-sm-6 col-md-4 col-lg-4 col-xs-12">
            <div class="footer-block">
              <h4>Quick Links</h4>
              <ul class="page-links multiple">
                <li>
                  <a href="index.php">Home</a>
                </li>
                <li>
                  <a href="about Us 2.php">About Us</a>
                </li>
                <li>
                  <a href="#">Comparison</a>
                </li>
                <li>
                  <a href="#">Expert Reviews</a>
                </li>
                <li>
                  <a href="#">Elements</a>
                </li>
                <li>
                  <a href="#">Checkout</a>
                </li>
                <li>
                  <a href="#">Packages</a>
                </li>
                <li>
                  <a href="#">Contact Us</a>
                </li>
              </ul>
            </div>
          </div>
          <div class="col-sm-12 col-md-3 col-lg-3 col-xs-12">
            <div class="footer-block">
             
                <a href="#">
                  <div class="icon">
                    
                  </div>
                  <div class="icon-text">
                  <img src="wp-content/img/1).svg">
                  <img src="wp-content/img/download-on-the-app-store-apple-logo-svgrepo-com.svg" style="max-width: 184px;margin-top: -43px;">
                </a>
              </div>
             
                <a href="#">
                
                  </div>
                
                  </div>
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="footer-bottom">
        <div class="container">
          <div class="row">
            <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12">
              <p>
              <p>Copyright 2019 ©  Created By  <a href="#">greenusys</a> All Rights Reserved. </p>
              </p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <input type="hidden" id="is_rtl" value="0" />
    <input type='hidden' id='is_video_active' value='1' />
    <input type='hidden' id='is_category_based' value='0' />
    <input type="hidden" id="profile_page" value="https://carspot.scriptsbundle.com/user-dashboard/?page-type=dashboard" />
    <input type="hidden" id="login_page" value="https://carspot.scriptsbundle.com/login/" />
    <input type="hidden" id="theme_path" value="https://carspot.scriptsbundle.com/wp-content/themes/carspot/" />
    <input type="hidden" id="carspot_ajax_url" value="https://carspot.scriptsbundle.com/wp-admin/admin-ajax.php" />
    <input type="hidden" id="carspot_forgot_msg" value="Password sent on your email." />
    <input type="hidden" id="carspot_profile_msg" value="Profile saved successfully." />
    <input type="hidden" id="carspot_max_upload_reach" value="Maximum upload limit reached" />
    <input type="hidden" id="not_logged_in" value="You have been logged out." />
    <input type="hidden" id="sb_upload_limit" value="5" />
    <!--gallery upload size-->
    <input type="hidden" id="sb_upload_size" value="15728640-15MB" />
    <input type="hidden" id="sb-bid-timezone" value="Etc/UTC" />
    <input type="hidden" id="facebook_key" value="1727676677526152" />
    <input type="hidden" id="google_key" value="171460363966-gcnnlfeusakhbldka9nj39acrebdf083.apps.googleusercontent.com" />
    <input type="hidden" id="confirm_delete" value="Are you sure to delete this?" />
    <input type="hidden" id="confirm_update" value="Are you sure to update this?" />
    <input type="hidden" id="ad_updated" value="Ad updated successfully." />
    <input type="hidden" id="ad_posted" value="Ad posted successfully." />
    <input type="hidden" id="inpection_posted" value="Inspection posted successfully." />
    <input type="hidden" id="redirect_uri" value="https://carspot.scriptsbundle.com/" />
    <input type="hidden" id="select_place_holder" value="Select Budget" />
    <input type="hidden" id="is_sticky_header" value="1" />
    <input type="hidden" id="current_currency" value="$ " />
    <input type="hidden" id="is_sub_active" value="1" />
    <input type="hidden" id="account_deleted" value="Your account have been deleted permanently.." />
    <input type="hidden" id="nonce_error" value="This is a secure website" />
    <!--Sticky header logic-->
    <input type="hidden" id="header_style" value="transparent" />
    <input type="hidden" id="is_sticky_header" value="1" />
    <input type="hidden" id="sticky_sb_logo_url" value="#" />
    <input type="hidden" id="static_sb_logo_url" value="#" />
    <input type="hidden" id="msg_notification_on" value="1" />
    <input type="hidden" id="msg_notification_time" value="1000000000" />
    <input type="hidden" id="is_unread_msgs" value="0" />
    <a href="#0" class="cd-top">Top</a>
    <!-- Email verification and reset password -->
    <style>
      .compare-floating-btn {
        position: fixed;
        right: 0;
        top: 30%;
        background-color: #242424;
        color: #fff;
        box-shadow: 1px 0px 20px rgba(0, 0, 0, 0.07);
        z-index: 999;
        border-radius: 4px 0 0 4px;
      }

      .compare-floating-btn a {
        padding: 15px 20px;
        color: #fff;
        display: inline-block;
      }

      .compare-floating-btn span.badge {
        position: absolute;
        border-radius: 50%;
        top: -12px;
        left: -12px;
        background-color: #E52D27;
        padding: 5px 10px;
        font-family: "Poppins", sans-serif;
        border: 2px solid #FFF;
        font-size: 12px;
        box-shadow: 0px 0px 22px rgba(0, 0, 0, 0.07);
      }
      .array_map{
        max-width:100%;
      padding: auto;
      text-outline: 0 0 #000;
      height: auto;
width: 100%;      }
    </style>
    <span class="compare-floating-btn " style='display:none;'>
      <a href="compare-ads/index.html" class="protip" data-pt-title=" Compare Ads" data-pt-position="left" data-pt-scheme="dark-transparent" data-pt-size="small">
        <i class="fa fa-clone"></i>
      </a>
    </span>
    <script type="text/html" id="wpb-modifications"></script>
    <script type="text/javascript">
      (function() {
        var c = document.body.className;
        c = c.replace(/woocommerce-no-js/, 'woocommerce-js');
        document.body.className = c;
      })();
    </script>
    <script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/swv/js/index77e1.js?ver=5.6.4' id='swv-js'></script>
    <script type='text/javascript' id='contact-form-7-js-extra'>
      /* 
                                                <![CDATA[ */
      var wpcf7 = {
        "api": {
          "root": "https:\/\/carspot.scriptsbundle.com\/wp-json\/",
          "namespace": "contact-form-7\/v1"
        }
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/plugins/contact-form-7/includes/js/index77e1.js?ver=5.6.4' id='contact-form-7-js'></script>
    <script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/js-cookie/js.cookie.min8b1a.js?ver=2.1.4-wc.7.1.0' id='js-cookie-js'></script>
    <script type='text/javascript' id='woocommerce-js-extra'>
      /* 
                                                <![CDATA[ */
      var woocommerce_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/?wc-ajax=%%endpoint%%"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/woocommerce.min9d27.js?ver=7.1.0' id='woocommerce-js'></script>
    <script type='text/javascript' id='wc-cart-fragments-js-extra'>
      /* 
                                                <![CDATA[ */
      var wc_cart_fragments_params = {
        "ajax_url": "\/wp-admin\/admin-ajax.php",
        "wc_ajax_url": "\/?wc-ajax=%%endpoint%%",
        "cart_hash_key": "wc_cart_hash_c31bc78334fe3259ffd51b96eaf94a9a",
        "fragment_name": "wc_fragments_c31bc78334fe3259ffd51b96eaf94a9a",
        "request_timeout": "5000"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/plugins/woocommerce/assets/js/frontend/cart-fragments.min9d27.js?ver=7.1.0' id='wc-cart-fragments-js'></script>
    <script type='text/javascript' src='wp-content/plugins/carspot_framework/js/theme6a4d.js?ver=6.1.1' id='carspot-theme-js-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/bootstrap.min6a4d.js?ver=6.1.1' id='bootstrap-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/toastr.min6a4d.js?ver=6.1.1' id='toastr-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/datepicker.min6a4d.js?ver=6.1.1' id='carspot-dt-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/animateNumber.min6a4d.js?ver=6.1.1' id='animate-number-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/easing6a4d.js?ver=6.1.1' id='easing-js'></script>
    <script type='text/javascript' src='wp-content/plugins/js_composer/assets/lib/bower/isotope/dist/isotope.pkgd.minf7be.js?ver=6.10.0' id='isotope-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/carousel.min6a4d.js?ver=6.1.1' id='carousel-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/dropzone6a4d.js?ver=6.1.1' id='dropzone-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/carspot-menu6a4d.js?ver=6.1.1' id='carspot-megamenu-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/form-dropzone6a4d.js?ver=6.1.1' id='form-dropzone-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/icheck.min6a4d.js?ver=6.1.1' id='icheck-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/modernizr6a4d.js?ver=6.1.1' id='modernizr-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/jquery.appear.min6a4d.js?ver=6.1.1' id='jquery-appear-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/jquery.countTo6a4d.js?ver=6.1.1' id='jquery-countTo-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/jquery.inview.min6a4d.js?ver=6.1.1' id='jquery-inview-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/nouislider.all.min6a4d.js?ver=6.1.1' id='nouislider-all-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/perfect-scrollbar.min6a4d.js?ver=6.1.1' id='perfect-scrollbar-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/select2.min6a4d.js?ver=6.1.1' id='select-2-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/slide6a4d.js?ver=6.1.1' id='slide-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/color-switcher6a4d.js?ver=6.1.1' id='color-switcher-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/parsley.min6a4d.js?ver=6.1.1' id='parsley-js'></script>
    <script type='text/javascript' src='../www.google.com/recaptcha/api6a4d.js?ver=6.1.1' id='recaptcha-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/hello6a4d.js?ver=6.1.1' id='hello-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/jquery-te.min6a4d.js?ver=6.1.1' id='jquery-te-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/jquery.tagsinput.min6a4d.js?ver=6.1.1' id='tagsinput-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/bootstrap-confirmation.min6a4d.js?ver=6.1.1' id='bootstrap-confirmation-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/jquery.fancybox.min6a4d.js?ver=6.1.1' id='fancybox2-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/jquery.autocomplete.min6a4d.js?ver=6.1.1' id='auto-complete-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/protip.min6a4d.js?ver=6.1.1' id='tooltop-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/timer6a4d.js?ver=6.1.1' id='timer-js'></script>
    <script type='text/javascript' src='wp-includes/js/dist/vendor/moment.min1bc8.js?ver=2.29.4' id='moment-js'></script>
    <script type='text/javascript' id='moment-js-after'>
      moment.updateLocale('en_US', {
        "months": ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"],
        "monthsShort": ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"],
        "weekdays": ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"],
        "weekdaysShort": ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"],
        "week": {
          "dow": 1
        },
        "longDateFormat": {
          "LT": "g:i a",
          "LTS": null,
          "L": null,
          "LL": "F j, Y",
          "LLL": "F j, Y g:i a",
          "LLLL": null
        }
      });
    </script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/moment-timezone-with-data6a4d.js?ver=6.1.1' id='moment-time-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/jquery-confirm.min6a4d.js?ver=6.1.1' id='jquery-confirm-js'></script>
    <script type='text/javascript' src='http://maps.googleapis.com/maps/api/js?key=AIzaSyBlD_32FsDWBArqaWybw9mSZUzX3JwzPTo&amp;language=en&amp;ver=6.1.1' id='google-map-js'></script>
    <script type='text/javascript' src='wp-includes/js/comment-reply.min6a4d.js?ver=6.1.1' id='comment-reply-js'></script>
    <script type='text/javascript' src='wp-includes/js/jquery/ui/core.min3f14.js?ver=1.13.2' id='jquery-ui-core-js'></script>
    <script type='text/javascript' src='wp-includes/js/jquery/ui/mouse.min3f14.js?ver=1.13.2' id='jquery-ui-mouse-js'></script>
    <script type='text/javascript' src='wp-includes/js/jquery/ui/sortable.min3f14.js?ver=1.13.2' id='jquery-ui-sortable-js'></script>
    <script type='text/javascript' src='wp-includes/js/jquery/jquery.ui.touch-punchc682.js?ver=0.2.2' id='jquery-touch-punch-js'></script>
    <script type='text/javascript' src='wp-includes/js/imagesloaded.mineda1.js?ver=4.1.4' id='imagesloaded-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/wow6a4d.js?ver=6.1.1' id='wow-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/video_player6a4d.js?ver=6.1.1' id='popup-video-iframe-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/rangeslider.min6a4d.js?ver=6.1.1' id='carspot-range-slider-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/sb.menu6a4d.js?ver=6.1.1' id='carspot-sb-menu-js'></script>
    <script type='text/javascript' src='../code.iconify.design/2/2.0.3/iconify.min6a4d.js?ver=6.1.1' id='carspot-iconify-js'></script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/custom-new6a4d.js?ver=6.1.1' id='carspot-custom-new-js'></script>
    <script type='text/javascript' id='carspot-custom-js-extra'>
      /* 
                                                <![CDATA[ */
      var get_strings = {
        "carspot_map_type": "google_map",
        "one": "One Star",
        "two": "Two Stars",
        "three": "Three Stars",
        "four": "Four Stars",
        "five": "Five Stars",
        "noStars": "Not Rated",
        "Sunday": "Sunday",
        "Monday": "Monday",
        "Tuesday": "Tuesday",
        "Wednesday": "Wednesday",
        "Thursday": "Thursday",
        "Friday": "Friday",
        "Saturday": "Saturday",
        "Sun": "Sun",
        "Mon": "Mon",
        "Tue": "Tue",
        "Wed": "Wed",
        "Thu": "Thu",
        "Fri": "Fri",
        "Sat": "Sat",
        "Su": "Su",
        "Mo": "Mo",
        "Tu": "Tu",
        "We": "We",
        "Th": "Th",
        "Fr": "Fr",
        "Sa": "Sa",
        "January": "January",
        "February": "February",
        "March": "March",
        "April": "April",
        "May": "May",
        "June": "June",
        "July": "July",
        "August": "August",
        "September": "September",
        "October": "October",
        "November": "November",
        "December": "December",
        "Jan": "Jan",
        "Feb": "Feb",
        "Mar": "Mar",
        "Apr": "Apr",
        "Jun": "Jun",
        "Jul": "July",
        "Aug": "Aug",
        "Sep": "Sep",
        "Oct": "Oct",
        "Nov": "Nov",
        "Dec": "Dec",
        "Today": "Today",
        "Clear": "Clear",
        "dateFormat": "dateFormat",
        "timeFormat": "timeFormat",
        "alt": "Confirm",
        "altMsg": "Are you sure you want to do this?",
        "acDelMsg": "Caution: your account will be deleted permanently?",
        "showNumber": "Show number",
        "addToCompare": "Added to compare list",
        "alreadyToCompare": "Already added two cars",
        "removeToCompare": "Removed from compare list",
        "alertConfirm": "confirm",
        "alertCancle": "cancel"
      };
      /* ]]> */
    </script>
    <script type='text/javascript' src='wp-content/themes/carspot/js/custom6a4d.js?ver=6.1.1' id='carspot-custom-js'></script>
    <script type='text/javascript' src='wp-content/plugins/js_composer/assets/js/dist/js_composer_front.minf7be.js?ver=6.10.0' id='wpb_composer_front_js-js'></script>
    <script type="text/javascript">
      (function($) {
        "use strict";
        // Adding email in mailchimp
        $('#processing_req').hide();
        $('#save_email').on('click', function() {
          var sb_email = $('#sb_email').val();
          var sb_action = $('#sb_action').val();
          if (carspot_validateEmail(sb_email)) {
            $('#save_email').hide();
            $('#processing_req').show();
            $.post('wp-admin/admin-ajax.html', {
              action: 'sb_mailchimp_subcribe',
              sb_email: sb_email,
              sb_action: sb_action
            }).done(function(response) {
              $('#processing_req').hide();
              $('#save_email').show();
              if (response == 1) {
                toastr.success('Thank you, we will get back to you.', 'Success!', {
                  timeOut: 2500,
                  "closeButton": true,
                  "positionClass": "toast-bottom-right"
                });
                $('#sb_email').val('');
              } else {
                toastr.error('There is some error, please check your API-KEY and LIST-ID.', 'Error!', {
                  timeOut: 2500,
                  "closeButton": true,
                  "positionClass": "toast-bottom-right"
                });
              }
            });
          } else {
            toastr.error('Please add valid email.', 'Error!', {
              timeOut: 2500,
              "closeButton": true,
              "positionClass": "toast-bottom-right"
            });
          }
        });
      })(jQuery);

      function checkVals() {
        return false;
      }
    </script>
     
  </body>
  <!-- Mirrored from carspot.scriptsbundle.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 06 Dec 2022 10:50:15 GMT -->
</html>