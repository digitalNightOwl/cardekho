<?php include "include/header.php" ?>
      <!-- menu end -->
    </div>
    <section class="wpb-content-wrapper">
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="simple-search" style="background: rgba(0, 0, 0, 0) url(wp-content/img1/2.jpg) center center no-repeat; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
                <div class="container">
                  <h1>MG Hector Colours</h1>
                  <p>Upcoming Cars in India 2022

 

&amp;Portsmouth, Bognor Regis, Southampton & Guildford!</p>
                 
              </section>
            </div>
          </div>
        </div>
      </div>
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="custom-padding ">
                <!-- Main Container -->
                <div class="container">
                  <!-- Row -->
                  <div class="row">
                    <div class="heading-panel">
                      <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                        <!-- Main Title -->
                        <h2>latest <span class="heading-color"> Used Cars </span> For Sale </h2>
                        <!-- Short Description -->
                        <p class="heading-text">Among these 102 upcoming cars, there are 6 MUVs, 6 Coupes, 66 SUVs, 1 Convertible, 11 Sedans, 14 Hatchbacks, 1 Wagon, 1 Luxury and 1 Pickup Truck. Of the above, 36 cars are expected to launch in the next three months. Also find out.</p>
                      </div>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                      <div class="row">
                        <div class="posts-masonry ads-for-home">
                          <div class="col-md-4 col-xs-12 col-sm-6">
                            <!-- Ad Box -->
                            <div class="category-grid-box">
                              <!-- Ad Img -->
                              <div class="category-grid-img">
                                <img src="wp-content/img1/3.jpg" alt="2017 Maserati Ghibli SQ4 Blue 1,695 Miles" class="img-responsive" />
                                <!-- Ad Status -->
                                <span class="ad-status">Featured</span>
                                <!-- User Review -->
                                <div class="user-preview">
                                  <a href="#">
                                    <img src="wp-content/img1/3.jpg" class="avatar avatar-small" alt="2017 Maserati Ghibli SQ4 Blue 1,695 Miles" />
                                  </a>
                                </div>
                                <!-- View Details -->
                                <a href="#" class="view-details"> View Details </a>
                                <!-- Additional Info -->
                                <div class="additional-information">
                                  <p>Transmission: Automatic</p>
                                  <p>Body Type: Sedan</p>
                                  <p>Condition: Used</p>
                                  <p>Posted on: January 24, 2019</p>
                                </div>
                                <!-- Additional Info End-->
                              </div>
                              <!-- Ad Img End -->
                              <div class="short-description">
                                <!-- Category Title -->
                                <div class="category-title">
                                  <span class="padding_cats">
                                    <a href="#">Audi</a>
                                  </span>
                                  <span class="padding_cats">
                                    <a href="#">A3</a>
                                  </span>
                                </div>
                                <h3>
                                 
                                </h3>
                                <div class="price">Rs26.<span class="">00 Lakh</span>
                                </div>
                              </div>
                              <!-- Addition Info -->
                              <div class="ad-info">
                                <ul class="list-unstyled">
                                  <li>
                                    <i class="flaticon-gas-station-1"></i>Petrol
                                  </li>
                                  <li>
                                    <i class="flaticon-dashboard"></i>1,694 km
                                  </li>
                                  <li>
                                    <i class="flaticon-engine-2"></i>3,000 cc
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <!-- Ad Box End -->
                          </div>
                          <div class="col-md-4 col-xs-12 col-sm-6">
                            <!-- Ad Box -->
                            <div class="category-grid-box">
                              <!-- Ad Img -->
                              <div class="category-grid-img">
                                <img src="wp-content/img1/front-left-side-47.webp" alt="Maserati Ghibli SQ4 Blue 1,695 Miles 2017" class="img-responsive" />
                                <!-- Ad Status -->
                                <span class="ad-status">Featured</span>
                                <!-- User Review -->
                                <div class="user-preview">
                                  <a href="#">
                                    <img src="wp-content/img1/front-left-side-47.webp" class="avatar avatar-small" alt="Maserati Ghibli SQ4 Blue 1,695 Miles 2017" />
                                  </a>
                                </div>
                                <!-- View Details -->
                                <a href="#" class="view-details"> View Details </a>
                                <!-- Additional Info -->
                                <div class="additional-information">
                                  <p>Transmission: Automatic</p>
                                  <p>Body Type: Sedan</p>
                                  <p>Condition: Used</p>
                                  <p>Posted on: January 24, 2019</p>
                                </div>
                                <!-- Additional Info End-->
                              </div>
                              <!-- Ad Img End -->
                              <div class="short-description">
                                <!-- Category Title -->
                                <div class="category-title">
                                  <span class="padding_cats">
                                    <a href="#">Audi</a>
                                  </span>
                                  <span class="padding_cats">
                                    <a href="#">A4</a>
                                  </span>
                                </div>
                                <h3>
                               
                                </h3>
                                <div class="price"> Rs.3.<span class="">50 Cr* </span>
                                </div>
                              </div>
                              <!-- Addition Info -->
                              <div class="ad-info">
                                <ul class="list-unstyled">
                                  <li>
                                    <i class="flaticon-gas-station-1"></i>Petrol
                                  </li>
                                  <li>
                                    <i class="flaticon-dashboard"></i>1,695 km
                                  </li>
                                  <li>
                                    <i class="flaticon-engine-2"></i>2,000 cc
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <!-- Ad Box End -->
                          </div>
                          <div class="col-md-4 col-xs-12 col-sm-6">
                            <!-- Ad Box -->
                            <div class="category-grid-box">
                              <!-- Ad Img -->
                              <div class="category-grid-img">
                                <img src="wp-content/img1/front-left-side-47 (1).webp" alt="2017 Maserati Ghibli SQ4 Blue 1,694 Miles" class="img-responsive" />
                                <!-- Ad Status -->
                                <span class="ad-status">Featured</span>
                                <!-- User Review -->
                                <div class="user-preview">
                                  <a href="#">
                                    <img src="wp-content/img1/front-left-side-47 (1).webp" class="avatar avatar-small" alt="2017 Maserati Ghibli SQ4 Blue 1,694 Miles" />
                                  </a>
                                </div>
                                <!-- View Details -->
                                <a href="#" class="view-details"> View Details </a>
                                <!-- Additional Info -->
                                <div class="additional-information">
                                  <p>Transmission: Automatic</p>
                                  <p>Body Type: Sedan</p>
                                  <p>Condition: Used</p>
                                  <p>Posted on: January 24, 2019</p>
                                </div>
                                <!-- Additional Info End-->
                              </div>
                              <!-- Ad Img End -->
                              <div class="short-description">
                                <!-- Category Title -->
                                <div class="category-title">
                                  <span class="padding_cats">
                                    <a href="#">Austin</a>
                                  </span>
                                  <span class="padding_cats">
                                    <a href="#">Mini</a>
                                  </span>
                                </div>
                                <h3>
                                  
                                </h3>
                                <div class="price">Rs.18.<span class=""> 00 Lakh* </span>
                                </div>
                              </div>
                              <!-- Addition Info -->
                              <div class="ad-info">
                                <ul class="list-unstyled">
                                  <li>
                                    <i class="flaticon-gas-station-1"></i>Petrol
                                  </li>
                                  <li>
                                    <i class="flaticon-dashboard"></i>1,694 km
                                  </li>
                                  <li>
                                    <i class="flaticon-engine-2"></i>3,500 cc
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <!-- Ad Box End -->



                            
                          </div>




                          <div class="col-md-4 col-xs-12 col-sm-6">
                            <!-- Ad Box -->
                            <div class="category-grid-box">
                              <!-- Ad Img -->
                              <div class="category-grid-img">
                                <img src="wp-content/img/slavia-exterior-right-front-three-quarter-5.webp" alt="2017 Ford Mustang" class="img-responsive" />
                                <!-- Ad Status -->
                                <span class="ad-status">Featured</span>
                                <!-- User Review -->
                                <div class="user-preview">
                                  <a href="#">
                                    <img src="wp-content/img/slavia-exterior-right-front-three-quarter-5.webp" class="avatar avatar-small" alt="2017 Ford Mustang" />
                                  </a>
                                </div>
                                <!-- View Details -->
                                <a href="#" class="view-details"> View Details </a>
                                <!-- Additional Info -->
                                <div class="additional-information">
                                  <p>Transmission: Automatic</p>
                                  <p>Body Type: Coupe</p>
                                  <p>Condition: Used</p>
                                  <p>Posted on: January 24, 2019</p>
                                </div>
                                <!-- Additional Info End-->
                              </div>
                              <!-- Ad Img End -->
                              <div class="short-description">
                                <!-- Category Title -->
                                <div class="category-title">
                                  <span class="padding_cats">
                                    <a href="#">Ford</a>
                                  </span>
                                  <span class="padding_cats">
                                    <a href="#">Mustang</a>
                                  </span>
                                </div>
                                <h3>
                                  
                                </h3>
                                <div class="price">₹ 11<span class=""> .29 Lakh</span>
                                </div>
                              </div>
                              <!-- Addition Info -->
                              <div class="ad-info">
                                <ul class="list-unstyled">
                                  <li>
                                    <i class="flaticon-gas-station-1"></i>Petrol
                                  </li>
                                  <li>
                                    <i class="flaticon-dashboard"></i>12,454 km
                                  </li>
                                  <li>
                                    <i class="flaticon-engine-2"></i>2,000 cc
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <!-- Ad Box End -->
                          </div>
                          <div class="col-md-4 col-xs-12 col-sm-6">
                            <!-- Ad Box -->
                            <div class="category-grid-box">
                              <!-- Ad Img -->
                              <div class="category-grid-img">
                                <img src="wp-content/img/all-new-city-exterior-right-front-three-quarter.webp" alt="2010 Chevrolet Camaro" class="img-responsive" />
                                <!-- Ad Status -->
                                <span class="ad-status">Featured</span>
                                <!-- User Review -->
                                <div class="user-preview">
                                  <a href="../dealer/carspot/indexdb00.html?type=ads">
                                    <img src="wp-content/img/all-new-city-exterior-right-front-three-quarter.webp" class="avatar avatar-small" alt="2010 Chevrolet Camaro" />
                                  </a>
                                </div>
                                <!-- View Details -->
                                <a href="#" class="view-details"> View Details </a>
                                <!-- Additional Info -->
                                <div class="additional-information">
                                  <p>Transmission: Automatic</p>
                                  <p>Body Type: Coupe</p>
                                  <p>Condition: Used</p>
                                  <p>Posted on: January 24, 2019</p>
                                </div>
                                <!-- Additional Info End-->
                              </div>
                              <!-- Ad Img End -->
                              <div class="short-description">
                                <!-- Category Title -->
                                <div class="category-title">
                                  <span class="padding_cats">
                                    <a href="#">Corvette</a>
                                  </span>
                                </div>
                                <h3>
                                 
                                </h3>
                                <div class="price">₹ 11.<span class=""> 60 Lakh </span>
                                </div>
                              </div>
                              <!-- Addition Info -->
                              <div class="ad-info">
                                <ul class="list-unstyled">
                                  <li>
                                    <i class="flaticon-gas-station-1"></i>Petrol
                                  </li>
                                  <li>
                                    <i class="flaticon-dashboard"></i>23 km
                                  </li>
                                  <li>
                                    <i class="flaticon-engine-2"></i>1,500 cc
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <!-- Ad Box End -->
                          </div>
                          <div class="col-md-4 col-xs-12 col-sm-6">
                            <!-- Ad Box -->
                            <div class="category-grid-box">
                              <!-- Ad Img -->
                              <div class="category-grid-img">
                                <a href="#" class="play-video">
                                  <img src="wp-content/img/kushaq-exterior-right-front-three-quarter-52.webp" alt="Icon" />
                                </a>
                                <img src="wp-content/img/kushaq-exterior-right-front-three-quarter-52.webp" alt="Mustang Shelby GT350 Coupe" class="img-responsive" />
                                <!-- Ad Status -->
                                <span class="ad-status">Featured</span>
                                <!-- User Review -->
                                <div class="user-preview">
                                  <a href="#">
                                    <img src="wp-content/img/kushaq-exterior-right-front-three-quarter-52.webp" class="avatar avatar-small" alt="Mustang Shelby GT350 Coupe" />
                                  </a>
                                </div>
                                <!-- View Details -->
                                <a href="#" class="view-details"> View Details </a>
                                <!-- Additional Info -->
                                <div class="additional-information">
                                  <p>Transmission: Manual</p>
                                  <p>Body Type: Coupe</p>
                                  <p>Condition: Used</p>
                                  <p>Posted on: June 20, 2017</p>
                                </div>
                                <!-- Additional Info End-->
                              </div>
                              <!-- Ad Img End -->
                              <div class="short-description">
                                <!-- Category Title -->
                                <div class="category-title">
                                  <span class="padding_cats">
                                    <a href="#">Ford</a>
                                  </span>
                                  <span class="padding_cats">
                                    <a href="#">Mustang</a>
                                  </span>
                                </div>
                                <h3>
                                  <a href="#"></a>
                                </h3>
                                <div class="price">₹ 11.58 Lakh</div>
                              </div>
                              <!-- Addition Info -->
                              <div class="ad-info">
                                <ul class="list-unstyled">
                                  <li>
                                    <i class="flaticon-gas-station-1"></i>Diesel
                                  </li>
                                  <li>
                                    <i class="flaticon-dashboard"></i>5,400 km
                                  </li>
                                  <li>
                                    <i class="flaticon-engine-2"></i>3,500 cc
                                  </li>
                                </ul>
                              </div>
                            </div>
                            <!-- Ad Box End -->
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="text-center">
                      <div class="load-more-btn">
                        <a href="#" target="%20_blank" class="btn btn-lg  btn-theme"> View All Featured Ads <i class="fa fa-refresh"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <div class="container">
                <div class="row">
                  <!-- <img alt="img" src="wp-content/img/c" class="block-content wow zoomIn" data-wow-delay="0ms" data-wow-duration="3500ms" /> -->
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      
                          <div class="clear"></div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </section>
	<?php include 'include/footer.php';?>