<?php include "include/header.php" ?> 
<section class="wpb-content-wrapper">
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
               <section class="transparent-breadcrumb-listing" style="background: url(wp-content/img/hero-cars-2-1.png); background-repeat: no-repeat ; background-size: cover ;  background-position: center center ;  background-attachment: scroll; ">
                <div class="container">
                  <h1>MG Hector Colours</h1>
                  <p>Stock Available at our MG Dealerships in 

 

&amp;Portsmouth, Bognor Regis, Southampton & Guildford!</p>
                  <div class="search-holder">
                  
                        </div>
                      </form>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>

                            <!-- Input Field -->
                           
  <div class="vc_row wpb_row vc_row-fluid">
    <!-- Ads Archive End -->
    <div class="wpb_column vc_column_container vc_col-sm-12">
      <div class="vc_column-inner">
        <div class="wpb_wrapper">
          <section class="custom-padding    over-hidden">
            <div class="container">
              <div class="row grid-style-2">
                <div class="heading-panel">
                  <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                    <!-- Main Title -->
                    <h2>Latest <span class="heading-color"> Featured </span> Cars </h2>
                    <!-- Short Description -->
                    <p class="heading-text">Eu delicata rationibus usu. Vix te putant utroque, ludus fabellas duo eu, his dico ut debet consectetuer.</p>
                  </div>
                </div>
                <!-- Middle Content Box -->
                <div class="col-md-12 col-xs-12 col-sm-12 ads-for-home">
                  <div class="row">
                    <div class="featured-slider container owl-carousel owl-theme">
                      <div class="item">
                        <div class="col-md-12  col-lg-12 col-sm-12 col-xs-12" id="slider-.holder-1375">
                          <div class="category-grid-box-1">
                            <div class="featured-ribbon">
                              <span>Featured</span>
                            </div>
                            <div class="image">
                              <a href="#">
                                <img src="wp-content/img/hector-plus-exterior-right-front-three-quarter-7.webp" alt="2017 Maserati Ghibli SQ4 Blue 1,695 Miles" class="img-responsive" />
                              </a>
                              <div class="price-tag">
                                <div class="price">
                                  <span>₹14.93 - <span class=""> ( 21.19 Lakh)</span>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div class="short-description-1 clearfix">
                              <div class="category-title">
                                <span class="padding_cats">
                                  <a href="#">Audi</a>
                                </span>
                                <span class="padding_cats">
                                  <a href="#">A3</a>
                                </span>
                              </div>
                              <h3>
                                <a href="#">Avg. Ex-Showroom price
						.</a>
                              </h3>
                              <p class="location">
                                <i class="fa fa-map-marker"></i>
                                <a href="#">California</a>, <a href="#">United States</a>
                              </p>
                              <ul class="list-unstyled">
                                <li>
                                  <i class="flaticon-gas-station-1"></i>Petrol
                                </li>
                                <li>
                                  <i class="flaticon-dashboard"></i>1,694 km
                                </li>
                                <li>
                                  <i class="flaticon-engine-2"></i>3000 cc
                                </li>
                                <li>
                                  <i class="flaticon-car-2"></i>Sedan
                                </li>
                                <li>
                                  <i class="flaticon-cogwheel-outline"></i>Blue
                                </li>
                              </ul>
                            </div>
                            <div class="ad-info-1">
                              <p>
                                <!-- <i class="flaticon-calendar"></i> &nbsp; <span>January 24, 2019</span> -->
                              </p>
                              <ul class="pull-right ">
                                <li>
                                  <a data-toggle="tooltip" data-placement="top" data-original-title="Save Ad" href="javascript:void(0);" class="save-ad" data-adid="1375">
                                    <i class="flaticon-like-1"></i>
                                  </a>
                                  <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                                </li>
                                <li>
                                  <a href="#">
                                    <i class="flaticon-message"></i>
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </div>
                          <!-- Listing Ad Grid -->
                        </div>
                      </div>
                      <div class="item">
                        <div class="col-md-12  col-lg-12 col-sm-12 col-xs-12" id="slider-.holder-1366">
                          <div class="category-grid-box-1">
                            <div class="featured-ribbon">
                              <span>Featured</span>
                            </div>
                            <div class="image">
                              <a href="#">
                                <img src="wp-content/img/tata-harrier-right-front-three-quarter58.webp" alt="Maserati Ghibli SQ4 Blue 1,695 Miles 2017" class="img-responsive" />
                              </a>
                              <div class="price-tag">
                                <div class="price">
                                  <span>₹14.79 - 
						<span class=""> (22.34 Lakh)</span>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div class="short-description-1 clearfix">
                              <div class="category-title">
                                <span class="padding_cats">
                                  <a href="#">Audi</a>
                                </span>
                                <span class="padding_cats">
                                  <a href="#">A4</a>
                                </span>
                              </div>
                              <h3>
                                <a href="#">Avg. Ex-Showroom price</a>
                              </h3>
                              <p class="location">
                                <i class="fa fa-map-marker"></i>
                                <a href="#">California</a>, <a href="#">United States</a>
                              </p>
                              <ul class="list-unstyled">
                                <li>
                                  <i class="flaticon-gas-station-1"></i>Petrol
                                </li>
                                <li>
                                  <i class="flaticon-dashboard"></i>1,695 km
                                </li>
                                <li>
                                  <i class="flaticon-engine-2"></i>2000 cc
                                </li>
                                <li>
                                  <i class="flaticon-car-2"></i>Sedan
                                </li>
                                <li>
                                  <i class="flaticon-cogwheel-outline"></i>Black
                                </li>
                              </ul>
                            </div>
                            <div class="ad-info-1">
                              <p>
                                <!-- <i class="flaticon-calendar"></i> &nbsp; <span>January 24, 2019</span> -->
                              </p>
                              <ul class="pull-right ">
                                <li>
                                  <a data-toggle="tooltip" data-placement="top" data-original-title="Save Ad" href="javascript:void(0);" class="save-ad" data-adid="1366">
                                    <i class="flaticon-like-1"></i>
                                  </a>
                                  <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                                </li>
                                <li>
                                  <a href="#">
                                    <i class="flaticon-message"></i>
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </div>
                          <!-- Listing Ad Grid -->
                        </div>
                      </div>
                      <div class="item">
                        <div class="col-md-12  col-lg-12 col-sm-12 col-xs-12" id="slider-.holder-54">
                          <div class="category-grid-box-1">
                            <div class="featured-ribbon">
                              <span>Featured</span>
                            </div>
                            <div class="image">
                              <a href="#" class="play-video">
                                <img src="wp-content/img/right-front-three-quarter.webp" alt="Icon" />
                              </a>
                              <a href="#">
                                <img src="wp-content/img/right-front-three-quarter.webp" alt="Honda BR-V i-VTEC S 2017" class="img-responsive" />
                              </a>
                              <div class="price-tag">
                                <div class="price">
                                  <span>₹10.32 
 <span class=""> (- 18.23 Lakh)</span>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div class="short-description-1 clearfix">
                              <div class="category-title">
                                <span class="padding_cats">
                                  <a href="#">Honda</a>
                                </span>
                                <span class="padding_cats">
                                  <a href="#">BR-V</a>
                                </span>
                              </div>
                              <h3>
                                <a href="#">Avg. Ex-Showroom price
						</a>
                              </h3>
                              <p class="location">
                                <i class="fa fa-map-marker"></i>
                                <a href="#">Bellevue</a>, <a href="#">Washington</a>, <a href="#">United States</a>
                              </p>
                              <ul class="list-unstyled">
                                <li>
                                  <i class="flaticon-gas-station-1"></i>Hybrid
                                </li>
                                <li>
                                  <i class="flaticon-dashboard"></i>25,000 km
                                </li>
                                <li>
                                  <i class="flaticon-engine-2"></i>3500 cc
                                </li>
                                <li>
                                  <i class="flaticon-car-2"></i>SUV
                                </li>
                                <li>
                                  <i class="flaticon-cogwheel-outline"></i>White
                                </li>
                              </ul>
                            </div>
                            <div class="ad-info-1">
                              <p>
                                <!-- <i class="flaticon-calendar"></i> &nbsp; <span>June 20, 2017</span> -->
                              </p>
                              <ul class="pull-right ">
                                <li>
                                  <a data-toggle="tooltip" data-placement="top" data-original-title="Save Ad" href="javascript:void(0);" class="save-ad" data-adid="54">
                                    <i class="flaticon-like-1"></i>
                                  </a>
                                  <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                                </li>
                                <li>
                                  <a href="#">
                                    <i class="flaticon-message"></i>
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </div>
                          <!-- Listing Ad Grid -->
                        </div>
                      </div>
                      <div class="item">
                        <div class="col-md-12  col-lg-12 col-sm-12 col-xs-12" id="slider-.holder-44">
                          <div class="category-grid-box-1">
                            <div class="featured-ribbon">
                              <span>Featured</span>
                            </div>
                            <div class="image">
                              <a href="#" class="play-video">
                                <img src="wp-content/img/seltos-right-front-three-quarter.webp" alt="Icon" />
                              </a>
                              <a href="#">
                                <img src="wp-content/img/seltos-right-front-three-quarter.webp" alt="A4 2.0 Quattro Premium Plus" class="img-responsive" />
                              </a>
                              <div class="price-tag">
                                <div class="price">
                                  <span>₹10.49
						<span class=""> ( - 18.65 Lakh)</span>
                                  </span>
                                </div>
                              </div>
                            </div>
                            <div class="short-description-1 clearfix">
                              <div class="category-title">
                                <span class="padding_cats">
                                  <a href="#">Audi</a>
                                </span>
                                <span class="padding_cats">
                                  <a href=".#">A4</a>
                                </span>
                              </div>
                              <h3>
                                <a href="#">Avg. Ex-Showroom price.</a>
                              </h3>
                              <p class="location">
                                <i class="fa fa-map-marker"></i>
                                <a href="#">Carmel</a>, <a href="#">California</a>, <a href="#">United States</a>
                              </p>
                              <ul class="list-unstyled">
                                <li>
                                  <i class="flaticon-gas-station-1"></i>Petrol
                                </li>
                                <li>
                                  <i class="flaticon-dashboard"></i>125,000 km
                                </li>
                                <li>
                                  <i class="flaticon-engine-2"></i>3500 cc
                                </li>
                                <li>
                                  <i class="flaticon-car-2"></i>Coupe
                                </li>
                                <li>
                                  <i class="flaticon-cogwheel-outline"></i>Blue
                                </li>
                              </ul>
                            </div>
                            <div class="ad-info-1">
                              <p>
                                <!-- <i class="flaticon-calendar"></i> &nbsp; <span>June 20, 2017</span> -->
                              </p>
                              <ul class="pull-right ">
                                <li>
                                  <a data-toggle="tooltip" data-placement="top" data-original-title="Save Ad" href="javascript:void(0);" class="save-ad" data-adid="44">
                                    <i class="flaticon-like-1"></i>
                                  </a>
                                  <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                                </li>
                                <li>
                                  <a href="#">
                                    <i class="flaticon-message"></i>
                                  </a>
                                </li>
                              </ul>
                            </div>
                          </div>
                          <!-- Listing Ad Grid -->
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
  <div class="vc_row wpb_row vc_row-fluid">
    <div class="wpb_column vc_column_container vc_col-sm-12">
      <div class="vc_column-inner">
        <div class="wpb_wrapper">
          <section class="sell-box padding-top-70">
            <div class="container">
              <div class="row">
                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                  <div class="sell-box-grid">
                    <div class="short-info">
                      <h3> Want To Sale Your Car ?</h3>
                      <h2>
                        <a href="# " target="%20_blank"> Are you looking for a car? </a>
                      </h2>
                      <p>Search your car in our Inventory and request a quote on the vehicle of your choosing sonu.</p>
                    </div>
                    <div class="text-center">
                      <img class="img-responsive wow slideInLeft center-block" data-wow-delay="0ms" data-wow-duration="2000ms" src="wp-content/img/br__1_-removebg-preview.png" alt="Image Not Found" />
                    </div>
                  </div>
                </div>
                <div class="col-lg-6 col-md-6 col-xs-12 col-sm-12">
                  <div class="sell-box-grid">
                    <div class="short-info">
                      <h3>Want To Sale Your Car ?</h3>
                      <h2>
                        <a href="# " target="%20_blank"> Do you want to sell your car? </a>
                      </h2>
                      <p>Request search your car in our Inventory and a quote on the vehicle of your choosing.</p>
                    </div>
                    <div class="text-center">
                      <img class="img-responsive wow slideInRight center-block" data-wow-delay="0ms" data-wow-duration="2000ms" src="wp-content/img/s8uvkua_1551597-removebg-preview.png" alt="Image Not Found" />
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
  <div class="vc_row wpb_row vc_row-fluid">
    <div class="wpb_column vc_column_container vc_col-sm-12">
      <div class="vc_column-inner">
        <div class="wpb_wrapper">
          <section class="custom-padding ">
            <!-- Main Container -->
            <div class="container">
              <!-- Row -->
              <div class="row">
                <div class="heading-panel">
                  <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                    <!-- Main Title -->
                    <h2>Latest <span class="heading-color"> Trending </span> Ads </h2>
                    <!-- Short Description -->
                    <p class="heading-text">Eu delicata rationibus usu. Vix te putant utroque, ludus fabellas duo eu, his dico ut debet consectetuer.</p>
                  </div>
                </div>
                <div class="text-center">
                  <div class="load-more-btn">
                    <a href="#" target="%20_blank" class="btn btn-lg  btn-theme"> View All Cars <i class="fa fa-refresh"></i>
                    </a>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
  
  <div class="vc_row wpb_row vc_row-fluid">
    <div class="wpb_column vc_column_container vc_col-sm-12">
      <div class="vc_column-inner">
        <div class="wpb_wrapper">
          <section class="custom-padding  ">
            <!-- Main Container -->
            <div class="container">
              <div class="row">
                <div class="heading-panel">
                  <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                    <!-- Main Title -->
                    <h2>Latest <span class="heading-color"> Blog </span> Post </h2>
                    <!-- Short Description -->
                    <p class="heading-text">Eu delicata rationibus usu. Vix te putant utroque, ludus fabellas duo eu, his dico ut debet consectetuer.</p>
                  </div>
                </div>
                <div class="col-md-12 col-xs-12 col-sm-12">
                  <div class="row">
                    <div class="col-md-4 col-sm-6 col-xs-12">
                      <div class="blog-post">
                        <div class="post-img">
                          <a href="#">
                            <img class="img-responsive" alt="Ford Focus Production for North America Will Cease for a Year" src="wp-content/img/front-left-side-47 (1).webp" />
                          </a>
                        </div>
                        <div class="blog-content">
                          <div class="user-preview">
                            <a href="../dealer/carspot/index.html">
                              <img class="img-circle resize" alt="Avatar" src="wp-content/img/front-left-side-47 (1).webp" />
                            </a>
                          </div>
                          <div class="post-info">
                            <a href="javascript:void(0);">June 30, 2017</a>
                            <a href="javascript:void(0);">0 comments</a>
                          </div>
                          <h3 class="post-title">
                            <a href="#">With purposeful, sporty looks and a spacious interior filled with comfort and intelligent features, it’s easy to just get in and get away. </a>
                          </h3>
                          <p class="post-excerpt"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quis erat sed lorem dictum ullamcorper. Sed vel elit sed nunc ornare auctor... <a href="#">
                              <strong> Read More </strong>
                            </a>
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                      <div class="blog-post">
                        <div class="post-img">
                          <a href="#">
                            <img class="img-responsive" alt="We Hear: Audi RS Models Could Be Offered in Rear-Wheel Drive" src="wp-content/img/2022-MG-Astor-40-900x506.jpg" />
                          </a>
                        </div>
                        <div class="blog-content">
                          <div class="user-preview">
                            <a href="#">
                              <img class="img-circle resize" alt="Avatar" src="wp-content/img/6.png" />
                            </a>
                          </div>
                          <div class="post-info">
                            <a href="javascript:void(0);">June 30, 2017</a>
                            <a href="javascript:void(0);">0 comments</a>
                          </div>
                          <h3 class="post-title">
                            <a href="#">MG HS offers buyers an exciting and high-quality alternative to the conventional choices with a sporty exterior and a family friendly interior with soft-. </a>
                          </h3>
                          <p class="post-excerpt"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quis erat sed lorem dictum ullamcorper. Sed vel elit sed nunc ornare auctor... <a href="#">
                              <strong> Read More </strong>
                            </a>
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4 col-sm-6 col-xs-12">
                      <div class="blog-post">
                        <div class="post-img">
                          <a href="#">
                            <img class="img-responsive" alt="Land Rover Freelander 2 Se First Impression" src="wp-content/img/hector-exterior-left-front-three-quarter-2.webp" />
                          </a>
                        </div>
                        <div class="blog-content">
                          <div class="user-preview">
                            <a href="#">
                              <img class="img-circle resize" alt="Avatar" src="wp-content/img/6.png" />
                            </a>
                          </div>
                          <div class="post-info">
                            <a href="javascript:void(0);">June 30, 2017</a>
                            <a href="javascript:void(0);">0 comments</a>
                          </div>
                          <h3 class="post-title">
                            <a href="#">The MG HS has the bold lines and proud features of a thoroughbred SUV designed for family living. Sporty looks, </a>
                          </h3>
                          <p class="post-excerpt"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quis erat sed lorem dictum ullamcorper. Sed vel elit sed nunc ornare auctor... <a href="#">
                              <strong> Read More </strong>
                            </a>
                          </p>
                        </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                    <div class="text-center">
                      <div class="load-more-btn"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div> <?php include 'include/footer.php';?>