<?php include "include/header.php" ?> 
<div data-elementor-type="wp-page" data-elementor-id="2580" class="elementor elementor-2580">
  <div class="elementor-inner">
    <div class="elementor-section-wrap">
      <div class="elementor-section elementor-top-section elementor-element elementor-element-3f5c7e5 elementor-section-full_width elementor-section-stretched elementor-section-height-default elementor-section-height-default" data-id="3f5c7e5" data-element_type="section" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;}">
        <div class="elementor-container elementor-column-gap-no">
          <div class="elementor-row">
            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-6f78cdf" data-id="6f78cdf" data-element_type="column">
              <div class="elementor-column-wrap elementor-element-populated">
                <div class="elementor-widget-wrap">
                  <div class="elementor-element elementor-element-b5ede5c elementor-widget elementor-widget-search__home_modern_2" data-id="b5ede5c" data-element_type="widget" data-widget_type="search__home_modern_2.default">
                    <div class="elementor-widget-container">
                      <section class="hero-section-2 section-style-3 opacity-color">
                        <div class="container">
                          <div class="row">
                            <div class="col-lg-10 col-md-10 col-sm-12 col-xs-12 col-lg-offset-1 col-md-offset-1">
                              <div class="hero-section-2-text">
                                <p>Electric Car Zone</p>
                                <h1> Electric Car Zone</h1>
                              </div>
                            </div>
                            <img decoding="async" src="wp-content/img/hero-cars-2-1.png" class="hero-car wow zoomIn img-responsive" data-wow-delay="0ms" data-wow-duration="3000ms" alt="image not found" />
                          </div>
                        </div>
                      </section>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="elementor-section elementor-top-section elementor-element elementor-element-e120e97 elementor-section-full_width elementor-section-stretched elementor-section-height-default elementor-section-height-default" data-id="e120e97" data-element_type="section" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;}">
        <div class="elementor-container elementor-column-gap-no">
          <div class="elementor-row">
            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-f699446" data-id="f699446" data-element_type="column">
              <div class="elementor-column-wrap elementor-element-populated">
                <div class="elementor-widget-wrap">
                  <div class="elementor-element elementor-element-abe3c17 elementor-widget elementor-widget-cats_fancy_short_base" data-id="abe3c17" data-element_type="widget" data-widget_type="cats_fancy_short_base.default">
                    <div class="elementor-widget-container">
                      <section class="custom-padding categories hover-animation">
                        <div class="container">
                          <div class="row">
                            <div class="heading-panel">
                              <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                                <!-- Main Title -->
                                <h2>Popular cars<span class="heading-color">  by brand </span>
                                </h2>
                                <!-- Short Description -->
                                <p class="heading-text"></p>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-2 col-sm-4">
                                <div class="box">
                                  <a href="#">
                                    <img decoding="async" alt="Audi" src="wp-content/img/kia.webp" />
                                    <h4>kia </h4>
                                  </a>
                                </div>
                              </div>
                              <div class="col-md-2 col-sm-4">
                                <div class="box">
                                  <a href="#">
                                    <img decoding="async" alt="Alfa Romeo" src="wp-content/img1/1.webp" />
                                    <h4> Mg </h4>
                                  </a>
                                </div>
                              </div>
                              <div class="col-md-2 col-sm-4">
                                <div class="box">
                                  <a href="#">
                                    <img decoding="async" alt="BMW" src="wp-content/img1/maruti.webp" />
                                    <h4> maruti </h4>
                                  </a>
                                </div>
                              </div>
                              <div class="col-md-2 col-sm-4">
                                <div class="box">
                                  <a href="#">
                                    <img decoding="async" alt="Ferrari" src="wp-content/img1/hyundai.webp" />
                                    <h4> Hyundai </h4>
                                  </a>
                                </div>
                              </div>
                              <div class="col-md-2 col-sm-4">
                                <div class="box">
                                  <a href="#">
                                    <img decoding="async" alt="Ford" src="wp-content/img1/nissan.webp" />
                                    <h4> Ford </h4>
                                  </a>
                                </div>
                              </div>
                              <div class="col-md-2 col-sm-4">
                                <div class="box">
                                  <a href="#">
                                    <img decoding="async" alt="Honda" src="wp-content/img1/datsun.webp" />
                                    <h4> Dataun </h4>
                                  </a>
                                </div>
                              </div>
                              <div class="clearfix"></div>
                            </div>
                          </div>
                        </div>
                      </section>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="elementor-section elementor-top-section elementor-element elementor-element-bf12090 elementor-section-full_width elementor-section-stretched elementor-section-height-default elementor-section-height-default" data-id="bf12090" data-element_type="section" data-settings="{&quot;stretch_section&quot;:&quot;section-stretched&quot;}">
        <div class="elementor-container elementor-column-gap-no">
          <div class="elementor-row">
            <div class="elementor-column elementor-col-100 elementor-top-column elementor-element elementor-element-0cf84ee" data-id="0cf84ee" data-element_type="column">
              <div class="elementor-column-wrap elementor-element-populated">
                <div class="elementor-widget-wrap">
                  <div class="elementor-element elementor-element-3d84431 elementor-widget elementor-widget-ads_short_base" data-id="3d84431" data-element_type="widget" data-widget_type="ads_short_base.default">
                    <div class="elementor-widget-container">
                      <section class="custom-padding">
                        <!-- Main Container -->
                        <div class="container">
                          <!-- Row -->
                          <div class="row">
                            <div class="heading-panel">
                              <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                                <!-- Main Title -->
                                <h2>Latest <span class="heading-color">Featured Cars</span>
                                </h2>
                                <!-- Short Description -->
                              </div>
                            </div>
                            <div class="col-md-12 col-xs-12 col-sm-12">
                              <div class="row">
                                <div class="grids-style-4">
                                  <div class="posts-masonry ads-for-home">
                                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3 col-xxl-3 toggle-list-items">
                                      <div class="ftd-card">
                                        <div class="card-img">
                                          <img decoding="async" src="wp-content/img1/front-left-side-47.jpg "alt="2017 Maserati Ghibli SQ4 Blue 1,694 Miles" class="img-responsive" />
                                          <span></span>
                                          <span class="iconify" data-icon="ant-design:star-filled"></span>
                                        </div>
                                        <div class="card-meta">
                                          <h6>Rs.1.<span class=""> (50 Cr*)</span>
                                          </h6>
                                          <a href="#">
                                            <h4>Key Specs of Toyota Land Cruiser.</h4>
                                          </a>
                                          <div class="location">
                                            <p>
                                              <span class="iconify" data-icon="entypo:location-pin"></span>
                                              <a href="#">California</a>, <a href="#">United States</a>
                                            </p>
                                          </div>
                                          <a href="javascript:void(0)">
                                            <img decoding="async" class="prf-back" src="wp-content/img1/front-left-side-47 (2).webp" alt="" />
                                            <img decoding="async" class="prf" src="wp-content/img1/front-left-side-47 (2).webp" alt="  Profile" />
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3 col-xxl-3 toggle-list-items">
                                      <div class="ftd-card">
                                        <div class="card-img">
                                          <img decoding="async" src="wp-content/img1/front-left-side-47 (1).jpg" alt="Honda BR-V i-VTEC S 2017" class="img-responsive" />
                                          <span></span>
                                          <span class="iconify" data-icon="ant-design:star-filled"></span>
                                        </div>
                                        <div class="card-meta">
                                          <h6>Rs.25<span class=""> (.00 Lakh* )</span>
                                          </h6>
                                          <a href="">
                                            <h4> Kia Sportage</h4>
                                          </a>
                                          <div class="location">
                                            <p>
                                              <span class="iconify" data-icon="entypo:location-pin"></span>
                                              <a href="#">Bellevue</a>, <a href="#">Washington</a>, <a href="#">United States</a>
                                            </p>
                                          </div>
                                          <a href="javascript:void(0)">
                                            <img decoding="async" class="prf-back" src="wp-content/img1/front-left-side-47 (2).jpg" alt="" />
                                            <img decoding="async" class="prf" src="wp-content/img1/front-left-side-47 (2).jpg" alt="  Profile" />
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3 col-xxl-3 toggle-list-items">
                                      <div class="ftd-card">
                                        <div class="card-img">
                                          <img decoding="async" src="wp-content/img1/front-left-side-47 (3).jpg" alt="Maserati Ghibli SQ4 Blue 1,695 Miles 2017" class="img-responsive" />
                                          <span></span>
                                          <span class="iconify" data-icon="ant-design:star-filled"></span>
                                        </div>
                                        <div class="card-meta">
                                          <h6>Rs.17.<span class=""> (00 Lakh*)</span>
                                          </h6>
                                          <a href="#">
                                            <h4>Mahindra XUV400 EV</h4>
                                          </a>
                                          <div class="location">
                                            <p>
                                              <span class="iconify" data-icon="entypo:location-pin"></span>
                                              <a href="#">California</a>, <a href="#">United States</a>
                                            </p>
                                          </div>
                                          <a href="javascript:void(0)">
                                            <img decoding="async" class="prf-back" src="wp-content/img1/front-left-side-47 (4).jpg" alt="" />
                                            <img decoding="async" class="prf" src="wp-content/img1/front-left-side-47 (4).jpg" alt="  Profile" />
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                                   

                                    <div class="col-12 col-sm-6 col-md-4 col-lg-3 col-xl-3 col-xxl-3 toggle-list-items">
                                      <div class="ftd-card">
                                        <div class="card-img">
                                          <img decoding="async" src="wp-content/img1/mg0.webp" alt="Land Rover Freelander 2 Se" class="img-responsive" />
                                          <span></span>
                                          <span class="iconify" data-icon="ant-design:star-filled"></span>
                                        </div>
                                        <div class="card-meta">
                                          <h6>Price On Call</h6>
                                          <a href="#">
                                            <h4> Land Rover Freelande...</h4>
                                          </a>
                                          <div class="location">
                                            <p>
                                              <span class="iconify" data-icon="entypo:location-pin"></span>
                                              <a href="#">District of Columbia</a>, <a href="#">United States</a>
                                            </p>
                                          </div>
                                          <a href="javascript:void(0)">
                                            <img decoding="async" class="prf-back" src="wp-content/img1/mg0 (1).webp" alt="" />
                                            <img decoding="async" class="prf" src="wp-content/img1/mg0 (1).webp" alt="  Profile" />
                                          </a>
                                        </div>
                                      </div>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="text-center">
                              <div class="load-more-btn"></div>
                            </div>
                          </div>
                        </div>
                      </section>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
     
                           
      
      <div class="clear"></div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </section>
	<?php include 'include/footer.php';?>