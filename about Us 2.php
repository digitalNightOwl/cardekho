

<?php include "include/header.php" ?> 
<style>
.right-side img {
    bottom: -250px;
    position: absolute;
    right: -13px;
}</style>
      <!-- menu end -->
    </div>
    <section class="transparent-breadcrumb-listing" style="background: url(wp-content/img2/ary-vessels-6794821.jpg); background-repeat: no-repeat ; background-size: cover ;  background-position: center center ;  background-attachment: scroll; ">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="list-heading">
              <h2>About Us</h2>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="wpb-content-wrapper">
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="custom-padding   " id="about-section-3">
                <div class="container">
                  <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-12 col-xs-12">
                      <div class="about-title">
                        <h2> About <span class="heading-color"> CarSpot </span> Dealership </h2>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent congue justo scelerisque mattis iaculis. Maecenas vestibulum faucibus enim scelerisque egestas. Praesent aliquet tincidunt id mi. </p>
                        <ul class="custom-links">
                          <li>
                            <a href="javascript:void(0)">Extend the life of your car</a>
                          </li>
                          <li>
                            <a href="javascript:void(0)">Extend the life of your car</a>
                          </li>
                          <li>
                            <a href="javascript:void(0)">Keep your engine cool</a>
                          </li>
                          <li>
                            <a href="javascript:void(0)">Extend the life of your car</a>
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-12 col-xs-12">
                      <div class="right-side">
                        <h5>Lorem ipsum dolor sit amet consectetur adipisicing elit Perfer repudiandae nostrum alias quibusdam!</h5>
                        <p>Lorem ipsum dolor sit amet consectetur adipisic.Min commodi enim nemo illum repellendus accusantium maiores itu delectus doloribus alias ea quisquam cum nullavolupta delectu.</p>
                        <p>nobis eius, deleniti dicta molestiae atque. Exercitationem odit dolor cumque facilis natus recusandae id, dolorum modi ducimus minus.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent congue justo scelerisque mattis iaculis. Maecenas vestibulum faucibus enim scelerisque egestas. Praesent aliquet id hendrerit id, hendrerit ac odio. In dui mauris, auctor vel vestibulum vitae, tincidunt id mi. <img src="wp-content/img2/mechanic.png" class="center-block img-responsive" alt="Image Not Found" />
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="row about-stats">
                    <div class="">
                      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <div class="icons">
                          <i class="flaticon-vehicle"></i>
                        </div>
                        <div class="number">
                          <span class="timer" data-from="0" data-to="1780" data-speed="1500" data-refresh-interval="5">0</span>
                        </div>
                        <h4>Total <span>Cars</span>
                        </h4>
                      </div>
                      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <div class="icons">
                          <i class="flaticon-security"></i>
                        </div>
                        <div class="number">
                          <span class="timer" data-from="0" data-to="820" data-speed="1500" data-refresh-interval="5">0</span>
                        </div>
                        <h4>Verified <span>Dealers</span>
                        </h4>
                      </div>
                      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <div class="icons">
                          <i class="flaticon-like-1"></i>
                        </div>
                        <div class="number">
                          <span class="timer" data-from="0" data-to="750" data-speed="1500" data-refresh-interval="5">0</span>
                        </div>
                        <h4>Active <span>Users</span>
                        </h4>
                      </div>
                      <div class="col-lg-3 col-md-3 col-sm-6 col-xs-6">
                        <div class="icons">
                          <i class="flaticon-light-bulb-1"></i>
                        </div>
                        <div class="number">
                          <span class="timer" data-from="0" data-to="55" data-speed="1500" data-refresh-interval="5">0</span>
                        </div>
                        <h4>Featured <span>Ads</span>
                        </h4>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="about-us  ">
                <div class="container-fluid">
                  <div class="row">
                    <div class="col-md-12 no-padding">
                      <div class="col-sm-12 col-md-4 col-xs-12 no-padding">
                        <div class="why-us border-box text-center">
                          <h5>Why choose us</h5>
                          <p>Mauris eros tortor, tristique cursus porttitor et, luctus sed urna. Quisque id libero risus. Aliquam accumsan erat id sem placerat tempus. </p>
                        </div>
                      </div>
                      <div class="col-sm-12 col-md-4 col-xs-12 no-padding">
                        <div class="why-us border-box text-center">
                          <h5>Our mission</h5>
                          <p>Mauris eros tortor, tristique cursus porttitor et, luctus sed urna. Quisque id libero risus. Aliquam accumsan erat id sem placerat tempus. </p>
                        </div>
                      </div>
                      <div class="col-sm-12 col-md-4 col-xs-12 no-padding">
                        <div class="why-us border-box text-center">
                          <h5>Only creative solutions</h5>
                          <p>Mauris eros tortor, tristique cursus porttitor et, luctus sed urna. Quisque id libero risus. Aliquam accumsan erat id sem placerat tempus. </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="section-padding-120 our-services">
                <div class="background-1" style="background-image: url( wp-content/img2/mechanic.jpg);background-position: center top;background-repeat: no-repeat;background-size: cover;height: 100%;left: 0;margin-left: -10%;position: absolute;top: 0;width: 50%;z-index: 1;"></div>
                <div class="background-2" style=" background-image: url(wp-content/img2/service.jpg);background-position: center center;background-repeat: no-repeat;background-size: cover;height: 100%;position: absolute;right: 0;top: 0;width: 80%;"></div>
                <img alt="Image Not Found" src="wp-content/img2/sell-1.png" class="img-responsive wow slideInRight custom-img" data-wow-delay="0ms" data-wow-duration="2000ms" />
                <div class="container">
                  <div class="row clearfix">
                    <div class="left-column col-lg-4 col-md-4 col-md-12">
                      <div class="inner-box">
                        <h2>Our Services</h2>
                        <div class="text">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent congue justo scelerisque mattis iaculis. Maecenas vestibulum faucibus enim scelerisque egestas. Praesent aliquet id hendrerit id, hendrerit ac odio. In dui mauris, auctor vel vestibulum vitae, tincidunt id mi.</div>
                      </div>
                    </div>
                    <div class="service-column col-lg-8 col-md-12">
                      <div class="inner-box wow zoomIn animated" data-wow-delay="0ms" data-wow-duration="1500ms">
                        <div class="row clearfix">
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="services-grid-3">
                              <div class="content-area">
                                <h1>01</h1>
                                <a href="javascript:void(0)">
                                  <h4>Engine Diagnostic</h4>
                                </a>
                                <p>We have the right caring, experience and dedicated professional for you.</p>
                                <div class="service-icon">
                                  <i class="flaticon-car-wash"></i>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="services-grid-3">
                              <div class="content-area">
                                <h1>02</h1>
                                <a href="javascript:void(0)">
                                  <h4>Wheel Alignment</h4>
                                </a>
                                <p>We have the right caring, experience and dedicated professional for you.</p>
                                <div class="service-icon">
                                  <i class="flaticon-vehicle"></i>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="services-grid-3">
                              <div class="content-area">
                                <h1>03</h1>
                                <a href="javascript:void(0)">
                                  <h4>Oil Changing</h4>
                                </a>
                                <p>We have the right caring, experience and dedicated professional for you.</p>
                                <div class="service-icon">
                                  <i class="flaticon-vehicle-2"></i>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6 col-sm-6 col-xs-12">
                            <div class="services-grid-3">
                              <div class="content-area">
                                <h1>04</h1>
                                <a href="javascript:void(0)">
                                  <h4>Steering & Suspension</h4>
                                </a>
                                <p>We have the right caring, experience and dedicated professional for you.</p>
                                <div class="service-icon">
                                  <i class="flaticon-car-steering-wheel"></i>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="section-padding   ">
                <!-- Main Container -->
                <div class="container">
                  <div class="heading-panel">
                    <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                      <!-- Main Title -->
                      <h2>Our Clients <span class="heading-color"> Feedback </span>
                      </h2>
                      <!-- Short Description -->
                      <p class="heading-text">Eu delicata rationibus usu. Vix te putant utroque, ludus fabellas duo eu, his dico ut debet consectetuer.</p>
                    </div>
                  </div>
                  <div class="col-md-12 col-xs-12 col-sm-12">
                    <div class="row">
                      <div class="owl-testimonial-1 owl-carousel owl-theme">
                        <div class="single_testimonial">
                          <div class="textimonial-content">
                            <h4>Awesome ! Loving It</h4>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                          </div>
                          <div class="testimonial-meta-box">
                            <img src="wp-content/img2//6-90x90 (1).jpg" alt="Sonu Monu" />
                            <div class="testimonial-meta">
                             
                              <i class="fa fa-star-ofa fa-star"></i>
                              <i class="fa fa-star-ofa fa-star"></i>
                              <i class="fa fa-star-ofa fa-star"></i>
                              <i class="fa fa-star-ofa fa-star"></i>
                              <i class="fa fa-star-o"></i>
                            </div>
                          </div>
                        </div>
                        <div class="single_testimonial">
                          <div class="textimonial-content">
                            <h4>Awesome ! Loving It</h4>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                          </div>
                          <div class="testimonial-meta-box">
                            <img src="wp-content/img2/6-90x90 (1).jpg" alt="Emily Copper" />
                            <div class="testimonial-meta">
                         
                              <i class="fa fa-star-ofa fa-star"></i>
                              <i class="fa fa-star-ofa fa-star"></i>
                              <i class="fa fa-star-o"></i>
                              <i class="fa fa-star-o"></i>
                              <i class="fa fa-star-o"></i>
                            </div>
                          </div>
                        </div>
                        <div class="single_testimonial">
                          <div class="textimonial-content">
                            <h4>Awesome ! Loving It</h4>
                            <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                          </div>
                          <div class="testimonial-meta-box">
                            <img src="wp-content/img2/6-90x90 (1).jpg" alt="Alex John" />
                            <div class="testimonial-meta">
                              
                              <i class="fa fa-star-ofa fa-star"></i>
                              <i class="fa fa-star-ofa fa-star"></i>
                              <i class="fa fa-star-ofa fa-star"></i>
                              <i class="fa fa-star-ofa fa-star"></i>
                              <i class="fa fa-star-ofa fa-star"></i>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="client-section  gray ">
                <div class="container">
                  <div class="row">
                    <div class="col-md-4 col-sm-12 col-xs-12">
                      <div class="margin-top-10">
                        <h3>Why Choose Us</h3>
                        <h2>Our premium Clients</h2>
                      </div>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-12">
                      <div class="brand-logo-area clients-bg">
                        <div class="clients-list owl-carousel owl-theme"></div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="car-inspection section-padding    ">
                <div class="container">
                  <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 nopadding hidden-sm">
                      <img src="wp-content/img2/car-in-red.png" class="wow bounceIn img-responsive" data-wow-delay="0ms" data-wow-duration="3000ms" alt="Image Not Found" />
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12 nopadding">
                      <div class="call-to-action-detail-section">
                        <div class="heading-2">
                          <h3> Want To Sale Your Car ?</h3>
                          <h2>Car Inspection</h2>
                        </div>
                        <p>Our CarSure experts inspect the car on over 200 checkpoints so you get complete satisfaction and peace of mind before buying. </p>
                        <div class="row">
                          <ul>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Transmission
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Steering
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Engine
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Tires
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Lighting
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Interior
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Suspension
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Exterior
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Brakes
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Air Conditioning
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Engine Diagnostics
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Wheel Alignment
                            </li>
                          </ul>
                        </div>
                        <a href="# " target="%20_blank" class="btn-theme btn-lg btn"> Schedule Inspection <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div class="clear">

</div>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div>
</section>
<?php include 'include/footer.php';?>
