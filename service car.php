
 
<?php include "include/header.php" ?> 
    <section class="transparent-breadcrumb-listing" style="background: url(wp-content/img/pexels-sebastian-pichard-9503685.jpg); background-repeat: no-repeat ; background-size: cover ;  background-position: center center ;  background-attachment: scroll; ">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="list-heading">
             
            </div>
          </div>
        </div>
      </div>
    </section>
    <div class="main-content-area clearfix">
      <section class="section-padding pattern_dots">
        <!-- Main Container -->
        <div class="container">
          <!-- Row -->
          <div class="row">
            <!-- Middle Content Area -->
            <div class="col-md-12 col-lg-12 col-sx-12">
              <!-- Row -->
              <div class="row">
                <div class="cat-description"></div>
                <!-- Ads Archive -->
                <div class="posts-masonry">
                  <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
                    <ul class="list-unstyled">
                      <li>
                        <div class="well ad-listing clearfix ">
                          <div class="col-md-3 col-sm-5 col-xs-12 grid-style no-padding">
                            <div class="img-box 222">
                              <img src="wp-content/img1/front-left-side-47 (6).jpg" class="img-responsive" alt="2017 Ford Mustang">
                              <div class="total-images">
                               
                              </div>
                              <div class="quick-view">
                                <a href="#" class="view-button">
                                  <i class="fa fa-search"></i>
                                </a>
                              </div>
                            </div>
                            <span class="ad-status">Featured</span>
                            <div class="user-preview">
                              <a href="javascript:void(0);">
                                <img src="wp-content/img1/front-left-side-47 (6).jpg" class="avatar avatar-small" alt="2017 Ford Mustang">
                              </a>
                            </div>
                          </div>
                          <div class="col-md-9 col-sm-7 col-xs-12">
                            <!-- Ad Content-->
                            <div class="row">
                              <div class="content-area">
                                <div class="col-md-9 col-sm-12 col-xs-12">
                                  <h3>
                                    <a href="#">Mahindra Thar</a>
                                  </h3>
                                  <ul class="ad-meta-info">
                                    <li>
                                      <i class="fa fa-map-marker"></i>
                                      <a href="#">United States</a>, <a href="#">Fort Lauderdale</a>, <a href="#">Florida</a>
                                    </li>
                                    <li>
                                      <i class="fa fa-clock-o"></i> January 24, 2019
                                    </li>
                                  </ul>
                                  <div class="ad-details">
                                    <p>Mahindra Thar is a 4 seater SUV available in a price range of Rs. 13.59 - 16.29 Lakh*. It is available in 10 variants, 2 engine options that are BS6 compliant and 2 transmission options: Automatic & Manual. </p>
                                    <ul class="list-unstyled">
                                      <li>
                                        <i class="flaticon-gas-station-1"></i>Petrol
                                      </li>
                                      <li>
                                        <i class="flaticon-dashboard"></i>12,454 km
                                      </li>
                                      <li>
                                        <i class="flaticon-engine-2"></i>2000 cc
                                      </li>
                                      <li>
                                        <i class="flaticon-car-2"></i>Coupe
                                      </li>
                                      <li>
                                        <i class="flaticon-cogwheel-outline"></i>White
                                      </li>
                                    </ul>
                                  </div>
                                </div>
                                <div class="col-md-3 col-xs-12 col-sm-12">
                                  <!-- Ad Stats -->
                                  <div class="short-info">
                                    <div class="ad-stats hidden-xs">
                                      <span>Mahindra: </span> Thar
                                    </div>
                                    <div class="ad-stats hidden-xs">
                                      <span>Condition : </span> Used
                                    </div>
                                    <div class="ad-stats hidden-xs">
                                      <span>Visits : </span> 1390
                                    </div>
                                  </div>
                                  <!-- Price -->
                                  <div class="price">
                                    <span> Rs.13.59 - <span class=""> (16.29 Lakh* )</span>
                                    </span>
                                  </div>
                                  <!-- Ad View Button -->
                                  <a href="#" class="btn btn-block btn-theme">
                                    <i class="fa fa-eye" aria-hidden="true"></i> View Ad </a>
                                </div>
                              </div>
                            </div>
                            <!-- Ad Content End -->
                          </div>
                        </div>
                      </li>
                      <li>
                        <div class="well ad-listing clearfix ">
                          <div class="col-md-3 col-sm-5 col-xs-12 grid-style no-padding">
                            <div class="img-box 222">
                              <img src="wp-content/img1/front-left-side-47 (7).jpg" class="img-responsive" alt="2010 Chevrolet Camaro">
                              <div class="total-images">
                               
                              </div>
                              <div class="quick-view">
                                <a href="#" class="view-button">
                                  <i class="fa fa-search"></i>
                                </a>
                              </div>
                            </div>
                            <span class="ad-status">Featured</span>
                            <div class="user-preview">
                              <a href="javascript:void(0);">
                                <img src="wp-content/img1/front-left-side-47 (7).jpg" class="avatar avatar-small" alt="2010 Chevrolet Camaro">
                              </a>
                            </div>
                          </div>
                          <div class="col-md-9 col-sm-7 col-xs-12">
                            <!-- Ad Content-->
                            <div class="row">
                              <div class="content-area">
                                <div class="col-md-9 col-sm-12 col-xs-12">
                                  <h3>
                                    <a href="#"> 2010 Chevrolet Camaro </a>
                                  </h3>
                                  <ul class="ad-meta-info">
                                    <li>
                                      <i class="fa fa-map-marker"></i>
                                      <a href="#">United States</a>, <a href="#">Fort Lauderdale</a>, <a href=".#">Florida</a>
                                    </li>
                                    <li>
                                      <i class="fa fa-clock-o"></i> January 24, 2019
                                    </li>
                                  </ul>
                                  <div class="ad-details">
                                    <p>**** Only 5,200 Miles *** &#8211; Ford Executive Vehicle &#8211; Convertible &#8211; Navigation &#8211; Shaker Pro ...</p>
                                    <ul class="list-unstyled">
                                      <li>
                                        <i class="flaticon-gas-station-1"></i>Petrol
                                      </li>
                                      <li>
                                        <i class="flaticon-dashboard"></i>23 km
                                      </li>
                                      <li>
                                        <i class="flaticon-engine-2"></i>1500 cc
                                      </li>
                                      <li>
                                        <i class="flaticon-car-2"></i>Coupe
                                      </li>
                                      <li>
                                        <i class="flaticon-cogwheel-outline"></i>White
                                      </li>
                                    </ul>
                                  </div>
                                </div>
                                <div class="col-md-3 col-xs-12 col-sm-12">
                                  <!-- Ad Stats -->
                                  <div class="short-info">
                                    <div class="ad-stats hidden-xs">
                                      <span>Ad Type : </span> Buy
                                    </div>
                                    <div class="ad-stats hidden-xs">
                                      <span>Condition : </span> Used
                                    </div>
                                    <div class="ad-stats hidden-xs">
                                      <span>Visits : </span> 1427
                                    </div>
                                  </div>
                                  <!-- Price -->
                                  <div class="price">
                                    <span> $ 37,345 <span class=""> (Negotiable)</span>
                                    </span>
                                  </div>
                                  <!-- Ad View Button -->
                                  <a href="#" class="btn btn-block btn-theme">
                                    <i class="fa fa-eye" aria-hidden="true"></i> View Ad </a>
                                </div>
                              </div>
                            </div>

	      <div class="clear">
          
        </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </section>
	<?php include 'include/footer.php';?>