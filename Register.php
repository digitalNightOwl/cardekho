<?php include "include/header.php" ?> 

    <section class="transparent-breadcrumb-listing" style="background: url(wp-content/img2/i-motor-group-11245770.jpg); background-repeat: no-repeat ; background-size: cover ;  background-position: center center ;  background-attachment: scroll; ">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="list-heading">
              <h2>Register</h2>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="wpb-content-wrapper">
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <div class="main-content-area clearfix">
                <section class="section-padding  gray">
                  <div class="container">
                    <div class="row">
                      <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2">
                        <div class="form-grid">
                          <div class="form-group resend_email" style="display:none;">
                            <div role="alert" class="alert alert-success alert-outline alert-dismissible ">
                              <button aria-label="Close" data-dismiss="alert" class="close" type="button">
                                <span aria-hidden="true">&#10005;</span>
                              </button> You did not get the e-mail? RESEND NOW <a href="javascript:void(0)" id="resend_email">
                                <strong>Resend now. </strong>
                              </a>
                            </div>
                          </div>
                          <div class="form-group  contact_admin" style="display:none;">
                            <div role="alert" class="alert alert-success alert-outline alert-dismissible ">
                              <button aria-label="Close" data-dismiss="alert" class="close" type="button">
                                <span aria-hidden="true">&#10005;</span>
                              </button> You still haven’t received the e-mail? Contact the Administrator <a href="../contact-us/index.html" id="resend_email">
                                <strong>You still haven’t received the e-mail? Contact the Administrator.</strong>
                              </a>
                            </div>
                          </div>
                          <div class="row">
                            <div class="social-btns-grid">
                              <div class="col-md-6 col-sm-12 col-xs-12">
                                <a class="btn btn-lg btn-block btn-social btn-facebook" onclick="hello('facebook').login({scope : 'email',})">
                                  <span class="fa fa-facebook"></span>Sign up with Facebook </a>
                              </div>
                              <div class="col-md-6 col-sm-12 col-xs-12">
                                <a class="btn btn-block btn-social btn-google" onclick="hello('google').login({scope : 'email'})">
                                <img src="wp-content/img/google-logo-png-webinar-optimizing-for-success-google-business-webinar-13.png" class="img-resposive" alt="Google logo" style="
    width: auto;
    margin-top: -3px;
    height: 33px;
">Sign up with Google </a>
                              </div>
                            </div>
                          </div>
                          <h2 class="no-span">
                            <b>OR</b>
                          </h2>
                          <form id="sb-sign-form">
                            <div class="row">
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                  <label>Name</label>
                                  <input placeholder="Your Name" class="form-control" type="text" data-parsley-required="true" data-parsley-error-message="Please enter your name." name="sb_reg_name" id="sb_reg_name" />
                                </div>
                              </div>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                  <label>Contact Number</label>
                                  <input class="form-control" name="sb_reg_contact" data-parsley-required="true" data-parsley-error-message="This field is required." placeholder="Your Contact Number" type="text" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                  <label>Email</label>
                                  <input placeholder="Your Email" class="form-control" type="email" data-parsley-type="email" data-parsley-required="true" data-parsley-error-message="Please enter your valid email." data-parsley-trigger="change" name="sb_reg_email" id="sb_reg_email" />
                                </div>
                              </div>
                              <div class="col-md-6 col-sm-6 col-xs-12">
                                <div class="form-group">
                                  <label>Password</label>
                                  <input placeholder="Your Password" class="form-control" type="password" data-parsley-required="true" data-parsley-error-message="Please enter your password." name="sb_reg_password" />
                                </div>
                              </div>
                            </div>
                            <div class="row">
                              <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group">
                                  <label>User Type</label>
                                  <select class="form-control" data-placeholder="Select user type." name="sb_user_type" data-parsley-required="true" data-parsley-error-message="Please select user type.">
                                    <option value="">Select user type.</option>
                                    <option value="individual">Individual</option>
                                    <option value="dealer">Dealer</option>
                                  </select>
                                </div>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="row">
                                <div class="col-xs-12 col-md-12 col-sm-12">
                                  <div class="skin-minimal">
                                    <ul class="list">
                                      <li>
                                        <input type="checkbox" data-parsley-required="true" data-parsley-error-message="Please accept terms and conditions." id="minimal-checkbox-1" name="minimal-checkbox-1" />
                                        <label class="term-class" for="minimal-checkbox">
                                          <a href="index.php" title="Terms and Conditions" target="">Terms &amp; Condition</a>
                                        </label>
                                      </li>
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </div>
                            <div class="form-group">
                              <div class="g-recaptcha" data-sitekey="6Ldt5pIUAAAAABDsF2h0Wv3jLS3m94xtBFOOqhZj"></div>
                            </div>
                            <input type="hidden" value="yes" name="name_captcha" />
                            <button class="btn btn-theme btn-lg btn-block" type="submit" id="sb_register_submit"><a href="login.php" style="
    color: white;
">Register</a>
                            <button class="btn btn-theme btn-lg btn-block no-display" type="button" id="sb_register_msg">Processing...</button>
                            <button class="btn btn-theme btn-lg btn-block no-display" type="button" id="sb_register_redirect">Redirecting...</button>
                            <br />
                            <p class="text-center">
                              <a href="#">Already registered? Log in here</a>
                            </p>
                            <input type="hidden" id="get_action" value="register" />
                            <input type="hidden" id="nonce" value="1670323764" />
                            <input type="hidden" id="verify_account_msg" value="Verificaton email has been sent to your email." />
                            <input type="hidden" id="register_nonce" value="2051c26f2f" />
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div class="clearfix"></div>
                    <div class="text-center">
                      <div class="load-more-btn"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
    <div>
   

    
  </div> <?php include 'include/footer.php';?>