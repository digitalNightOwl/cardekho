 
<?php include "include/header.php" ?> 
 <section class="transparent-breadcrumb-listing" style="background: url(wp-content/img/pexels-abhinav-6986233.jpg); background-repeat: no-repeat ; background-size: cover ;  background-position: center center ;  background-attachment: scroll; ">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="list-heading">
          <h2></h2>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="main-content-area clearfix">
  <section class="section-padding  gray page-search">
    <div class="container">
      <!-- Row -->
      <div class="row">
        <div class="col-md-3 col-sm-12 col-xs-12">
          <div class="sidebar">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
              <div class="panel panel-default" id="red-title">
                <!-- Heading -->
                <div class="panel-heading" role="tab" id="headingFive">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="true" aria-controls="collapseFive">
                      <i class="more-less glyphicon glyphicon-plus"></i> Search By Keyword </a>
                  </h4>
                </div>
                <form method="get" action="#">
                  <!-- Content -->
                  <div id="collapseFive" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingFive">
                    <div class="panel-body">
                      <div class="search-widget">
                        <input id="autocomplete-dynamic" autocomplete="off" class="form-control" placeholder="search" type="text" name="ad_title" value="">
                        <button type="submit">
                          <i class="fa fa-search"></i>
                        </button>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="carspot_layout_type" value="2" />
                </form>
              </div>
              <div class="panel panel-default" id="red-category">
                <!-- Heading -->
                <div class="panel-heading" role="tab" id="headingOne">
                  <!-- Title -->
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                      <i class="more-less glyphicon glyphicon-plus"></i> Select Make </a>
                  </h4>
                  <!-- Title End -->
                </div>
                <!-- Content -->
                <form method="get" id="make_search" action="#">
                  <div id="collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                      <label class="control-label"> Select Make </label>
                      <select class="search-select form-control" id="make_id">
                        <option value=""> Select Any Category </option>
                        <option value="50">Alfa Romeo </option>
                        <option value="60">Aston Martin </option>
                        <option value="62">Audi </option>
                        <option value="65">Austin </option>
                        <option value="66">Austin Healey </option>
                        <option value="297">Aveo </option>
                        <option value="68">Bentley </option>
                        <option value="71">BMW </option>
                        <option value="302">Bolt </option>
                        <option value="307">Brothers </option>
                        <option value="314">Camaro </option>
                        <option value="325">Charger </option>
                        <option value="84">Corvette </option>
                        <option value="339">Dart </option>
                        <option value="103">Ferrari </option>
                        <option value="106">Ford </option>
                        <option value="119">Honda </option>
                        <option value="123">Hummer </option>
                        <option value="132">Jaguar </option>
                        <option value="134">Joy </option>
                        <option value="135">Lamborghini </option>
                        <option value="137">Land Rover </option>
                        <option value="156">McLaren </option>
                        <option value="157">Mercedes </option>
                        <option value="161">Mitsubishi </option>
                        <option value="167">Nissan </option>
                        <option value="169">Nitro </option>
                        <option value="172">Optra </option>
                        <option value="177">Porsche </option>
                        <option value="194">Renault </option>
                        <option value="198">Rolls-Royce </option>
                        <option value="210">Sonic </option>
                        <option value="211">Spark </option>
                        <option value="218">Stealth </option>
                        <option value="222">Suzuki </option>
                        <option value="239">Viper </option>
                        <option value="242">Volt </option>
                      </select>
                      <div id="select_modal" class="margin-top-10"></div>
                      <div id="select_modals" class="margin-top-10"></div>
                      <div id="select_forth_div" class="margin-top-10"></div>
                      <input type="submit" class="btn btn-theme btn-sm margin-top-10 margin-bottom-10" id="search_make" value="Search" />
                    </div>
                  </div>
                  <input type="hidden" name="cat_id" id="cat_id" value="" />
                  <input type="hidden" name="carspot_layout_type" value="2" />
                </form>
              </div>
              <div class="panel panel-default" id="red-country">
                <!-- Heading -->
                <div class="panel-heading" role="tab" id="location_heading">
                  <!-- Title -->
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#ad-location" aria-expanded="true" aria-controls="ad-location">
                      <i class="more-less glyphicon glyphicon-plus"></i> Ads By Location </a>
                  </h4>
                  <!-- Title End -->
                </div>
                <!-- Content -->
                <form method="get" id="search_countries" action="#">
                  <div id="ad-location" class="panel-collapse collapse " role="tabpanel" aria-labelledby="location_heading">
                    <div class="panel-body countries">
                      <ul>
                        <li>
                          <a href="javascript:void(0);" data-country-id="230"> United States <span>(8)</span>
                          </a>
                        </li>
                      </ul>
                    </div>
                  </div>
                  <input type="hidden" name="country_id" id="country_id" value="" />
                  <input type="hidden" name="carspot_layout_type" value="2" />
                </form>
                <div class="search-modal modal fade states_model" id="states_model" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                          <span aria-hidden="true">&#10005;</span>
                          <span class="sr-only">Close</span>
                        </button>
                        <h3 class="modal-title text-center">
                          <i class="icon-gears"></i> Select Your Location
                        </h3>
                      </div>
                      <div class="modal-body">
                        <!-- content goes here -->
                        <div class="search-block">
                          <div class="row">
                            <div class="col-md-12 col-xs-12 col-sm-12 popular-search" id="countries_response"></div>
                          </div>
                        </div>
                      </div>
                      <div class="modal-footer">
                        <button type="submit" id="country-btn" class="btn btn-lg btn-block"> Submit </button>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div class="panel panel-default" id="red-price">
                <!-- Heading -->
                <div class="panel-heading" role="tab" id="headingfour">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapsefour" aria-expanded="false" aria-controls="collapsefour">
                      <i class="more-less glyphicon glyphicon-plus"></i> Price </a>
                  </h4>
                </div>
                <!-- Content -->
                <form method="get" action="#" class="carspot-price-form">
                  <div id="collapsefour" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingfour">
                    <div class="panel-body">
                      <span class="price-slider-value">Price ($ ) <span id="price-min"></span> - <span id="price-max"></span>
                      </span>
                      <div id="price-slider"></div>
                      <div class="input-group margin-top-10">
                        <input type="text" class="form-control" name="min_price" id="min_selected" value="500" autocomplete="off" />
                        <span class="input-group-addon">-</span>
                        <input type="text" class="form-control" name="max_price" id="max_selected" value="100000" autocomplete="off" />
                      </div>
                      <input type="hidden" id="min_price" value="500" />
                      <input type="hidden" id="max_price" value="100000" />
                      <input type="submit" class="btn btn-theme btn-sm margin-top-10" value="Search" />
                    </div>
                  </div>
                  <input type="hidden" name="carspot_layout_type" value="2" />
                </form>
              </div>
              <div class="panel panel-default" id="red-milage">
                <!-- Heading -->
                <div class="panel-heading" role="tab" id="ad-mileage">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#mileage" aria-expanded="true" aria-controls="ad-mileage">
                      <i class="more-less glyphicon glyphicon-plus"></i> Ad Mileage (Km) </a>
                  </h4>
                </div>
                <!-- Content -->
                <form method="get" id="get_mileage" action="#">
                  <div id="mileage" class="panel-collapse collapse " role="tabpanel" aria-labelledby="ad-mileage">
                    <div class="panel-body">
                      <div class="input-group margin-top-10">
                        <input type="text" class="form-control" name="mileage_from" data-parsley-type="digits" data-parsley-required="true" data-parsley-error-message="Value should be numeric" placeholder="From" id="mileage_from" value="" />
                        <span class="input-group-addon">-</span>
                        <input type="text" class="form-control" data-parsley-required="true" data-parsley-type="digits" data-parsley-error-message="Value should be numeric" placeholder="To" name="mileage_to" id="mileage_to" value="" />
                      </div>
                      <input type="submit" class="btn btn-theme btn-sm margin-top-10" value="Search" />
                    </div>
                  </div>
                  <input type="hidden" name="carspot_layout_type" value="2" />
                </form>
              </div>
              <div class="panel panel-default" id="red-years">
                <!-- Heading -->
                <div class="panel-heading" role="tab" id="headingYear">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#Yearcollapse" aria-expanded="true" aria-controls="Yearcollapse">
                      <i class="more-less glyphicon glyphicon-plus"></i> Year </a>
                  </h4>
                </div>
                <!-- Content -->
                <form method="get" action="#">
                  <div id="Yearcollapse" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingYear">
                    <div class="panel-body">
                      <div class="input-group  margin-top-10">
                        <span class="input-group-addon">From</span>
                        <select id="year_from" name="year_from" class="form-control">
                          <option value="2010">2010 </option>
                          <option value="2011">2011 </option>
                          <option value="2012">2012 </option>
                          <option value="2013">2013 </option>
                          <option value="2014">2014 </option>
                          <option value="2015">2015 </option>
                          <option value="2016">2016 </option>
                          <option value="2017">2017 </option>
                        </select>
                      </div>
                      <div class="input-group">
                        <span class="input-group-addon">To</span>
                        <select id="year_to" name="year_to" class="form-control">
                          <option value="2010">2010 </option>
                          <option value="2011">2011 </option>
                          <option value="2012">2012 </option>
                          <option value="2013">2013 </option>
                          <option value="2014">2014 </option>
                          <option value="2015">2015 </option>
                          <option value="2016">2016 </option>
                          <option value="2017">2017 </option>
                        </select>
                      </div>
                      <input type="submit" id="ad_year" class="btn btn-theme btn-sm margin-top-10" value="Search" />
                    </div>
                  </div>
                  <input type="hidden" name="carspot_layout_type" value="2" />
                </form>
              </div>
              <div class="panel panel-default" id="red-transmission">
                <!-- Heading -->
                <div class="panel-heading" role="tab" id="body_transmission">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#transmission" aria-expanded="true" aria-controls="body_transmission">
                      <i class="more-less glyphicon glyphicon-plus"></i> Transmission </a>
                  </h4>
                </div>
                <!-- Content -->
                <form method="get" action="#">
                  <div id="transmission" class="panel-collapse collapse " role="tabpanel" aria-labelledby="body_transmission">
                    <div class="panel-body">
                      <div class="skin-minimal">
                        <ul class="list">
                          <li>
                            <input type="checkbox" id="transmission-type-67" value="Automatic" name="transmission[]">
                            <label for="transmission-type-67">Automatic </label>
                            <span> (6) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="transmission-type-151" value="Manual" name="transmission[]">
                            <label for="transmission-type-151">Manual </label>
                            <span> (2) </span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="carspot_layout_type" value="2" />
                </form>
              </div>
              <div class="panel panel-default" id="red-engine-type">
                <!-- Heading -->
                <div class="panel-heading" role="tab" id="ad_body_type">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#body_type" aria-expanded="true" aria-controls="ad_body_type">
                      <i class="more-less glyphicon glyphicon-plus"></i> Engine Type </a>
                  </h4>
                </div>
                <!-- Content -->
                <form method="get" action="#">
                  <div id="body_type" class="panel-collapse collapse " role="tabpanel" aria-labelledby="ad_body_type">
                    <div class="panel-body">
                      <div class="skin-minimal">
                        <ul class="list">
                          <li>
                            <input type="checkbox" id="engine-type-81" value="CNG" name="engine_type[]">
                            <label for="engine-type-81">CNG </label>
                            <span> (0) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="engine-type-96" value="Diesel" name="engine_type[]">
                            <label for="engine-type-96">Diesel </label>
                            <span> (1) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="engine-type-126" value="Hybrid" name="engine_type[]">
                            <label for="engine-type-126">Hybrid </label>
                            <span> (1) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="engine-type-176" value="Petrol" name="engine_type[]">
                            <label for="engine-type-176">Petrol </label>
                            <span> (6) </span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="carspot_layout_type" value="2" />
                </form>
              </div>
              <div class="panel panel-default" id="red-engine-capacity">
                <!-- Heading -->
                <div class="panel-heading" role="tab" id="engince-capacity">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#engine_capacity" aria-expanded="true" aria-controls="engince-capacity">
                      <i class="more-less glyphicon glyphicon-plus"></i> Engine Capacity (CC) </a>
                  </h4>
                </div>
                <!-- Content -->
                <form method="get" action="#">
                  <div id="engine_capacity" class="panel-collapse collapse " role="tabpanel" aria-labelledby="engince-capacity">
                    <div class="panel-body">
                      <div class="skin-minimal">
                        <ul class="list">
                          <li>
                            <input type="checkbox" id="engine-capacity-29" value="1000" name="engine_capacity[]">
                            <label for="engine-capacity-29">1000cc</label>
                            <span> (0) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="engine-capacity-30" value="1200" name="engine_capacity[]">
                            <label for="engine-capacity-30">1200cc</label>
                            <span> (0) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="engine-capacity-31" value="1500" name="engine_capacity[]">
                            <label for="engine-capacity-31">1500cc</label>
                            <span> (1) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="engine-capacity-32" value="2000" name="engine_capacity[]">
                            <label for="engine-capacity-32">2000cc</label>
                            <span> (2) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="engine-capacity-42" value="2500" name="engine_capacity[]">
                            <label for="engine-capacity-42">2500cc</label>
                            <span> (0) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="engine-capacity-43" value="3000" name="engine_capacity[]">
                            <label for="engine-capacity-43">3000cc</label>
                            <span> (1) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="engine-capacity-44" value="3500" name="engine_capacity[]">
                            <label for="engine-capacity-44">3500cc</label>
                            <span> (4) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="engine-capacity-45" value="800" name="engine_capacity[]">
                            <label for="engine-capacity-45">800cc</label>
                            <span> (0) </span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="carspot_layout_type" value="2" />
                </form>
              </div>
              <div class="panel panel-default" id="red-body-type">
                <!-- Heading -->
                <div class="panel-heading" role="tab" id="bodytype">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ad_bodytype" aria-expanded="true" aria-controls="body_type">
                      <i class="more-less glyphicon glyphicon-plus"></i> Body Type </a>
                  </h4>
                </div>
                <!-- Content -->
                <form method="get" action="#">
                  <div id="ad_bodytype" class="panel-collapse collapse " role="tabpanel" aria-labelledby="bodytype">
                    <div class="panel-body">
                      <div class="skin-minimal">
                        <ul class="list">
                          <li>
                            <input type="checkbox" id="body-type-82" value="Convertible" name="body_type[]">
                            <label for="body-type-82">Convertible </label>
                            <span> (0) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="body-type-85" value="Coupe" name="body_type[]">
                            <label for="body-type-85">Coupe </label>
                            <span> (4) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="body-type-87" value="Crossover" name="body_type[]">
                            <label for="body-type-87">Crossover </label>
                            <span> (0) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="body-type-100" value="Electric" name="body_type[]">
                            <label for="body-type-100">Electric </label>
                            <span> (0) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="body-type-118" value="Hatchback" name="body_type[]">
                            <label for="body-type-118">Hatchback </label>
                            <span> (0) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="body-type-127" value="Hybrid" name="body_type[]">
                            <label for="body-type-127">Hybrid </label>
                            <span> (0) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="body-type-140" value="Luxury" name="body_type[]">
                            <label for="body-type-140">Luxury </label>
                            <span> (0) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="body-type-203" value="Sedan" name="body_type[]">
                            <label for="body-type-203">Sedan </label>
                            <span> (3) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="body-type-220" value="SUV" name="body_type[]">
                            <label for="body-type-220">SUV </label>
                            <span> (1) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="body-type-226" value="Truck" name="body_type[]">
                            <label for="body-type-226">Truck </label>
                            <span> (0) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="body-type-234" value="Van/Minivan" name="body_type[]">
                            <label for="body-type-234">Van/Minivan </label>
                            <span> (0) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="body-type-243" value="Wagon" name="body_type[]">
                            <label for="body-type-243">Wagon </label>
                            <span> (0) </span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="carspot_layout_type" value="2" />
                </form>
              </div>
              <div class="panel panel-default" id="red-color-family">
                <!-- Heading -->
                <div class="panel-heading" role="tab" id="ad-color-family">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ad-color" aria-expanded="true" aria-controls="ad-color-family">
                      <i class="more-less glyphicon glyphicon-plus"></i> Color Family </a>
                  </h4>
                </div>
                <!-- Content -->
                <form method="get" action="#">
                  <div id="ad-color" class="panel-collapse collapse " role="tabpanel" aria-labelledby="ad-color-family">
                    <div class="panel-body">
                      <div class="skin-minimal">
                        <ul class="list">
                          <li>
                            <input type="checkbox" id="ad_color-69" value="Black" name="color_family[]">
                            <label for="ad_color-69">Black</label>
                            <span> (2) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="ad_color-70" value="Blue" name="color_family[]">
                            <label for="ad_color-70">Blue</label>
                            <span> (3) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="ad_color-75" value="Bronze" name="color_family[]">
                            <label for="ad_color-75">Bronze</label>
                            <span> (0) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="ad_color-113" value="Gold" name="color_family[]">
                            <label for="ad_color-113">Gold</label>
                            <span> (0) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="ad_color-114" value="Green" name="color_family[]">
                            <label for="ad_color-114">Green</label>
                            <span> (0) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="ad_color-115" value="Grey" name="color_family[]">
                            <label for="ad_color-115">Grey</label>
                            <span> (0) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="ad_color-192" value="Red" name="color_family[]">
                            <label for="ad_color-192">Red</label>
                            <span> (0) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="ad_color-206" value="Silver" name="color_family[]">
                            <label for="ad_color-206">Silver</label>
                            <span> (0) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="ad_color-245" value="White" name="color_family[]">
                            <label for="ad_color-245">White</label>
                            <span> (3) </span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="carspot_layout_type" value="2" />
                </form>
              </div>
              <div class="panel panel-default" id="red-ad-type">
                <!-- Heading -->
                <div class="panel-heading" role="tab" id="headingSeven">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseSeven" aria-expanded="true" aria-controls="collapseSeven">
                      <i class="more-less glyphicon glyphicon-plus"></i> Car Type </a>
                  </h4>
                </div>
                <!-- Content -->
                <form method="get" action="#">
                  <div id="collapseSeven" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingSeven">
                    <div class="panel-body">
                      <div class="skin-minimal">
                        <ul class="list">
                          <li>
                            <input tabindex="7" type="checkbox" id="minimal-radio-76" name="ad_type[]" value="Buy">
                            <label for="minimal-radio-76">Buy</label>
                            <span> (1) </span>
                          </li>
                          <li>
                            <input tabindex="7" type="checkbox" id="minimal-radio-204" name="ad_type[]" value="Sell">
                            <label for="minimal-radio-204">Sell</label>
                            <span> (7) </span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="carspot_layout_type" value="2" />
                </form>
              </div>
              <div class="panel panel-default" id="red-isurance">
                <!-- Heading -->
                <div class="panel-heading" role="tab" id="ad-insurance">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#insurance" aria-expanded="true" aria-controls="ad-insurance">
                      <i class="more-less glyphicon glyphicon-plus"></i> Car Insurance </a>
                  </h4>
                </div>
                <!-- Content -->
                <form method="get" action="#">
                  <div id="insurance" class="panel-collapse collapse " role="tabpanel" aria-labelledby="ad-insurance">
                    <div class="panel-body">
                      <div class="skin-minimal">
                        <ul class="list">
                          <li>
                            <input type="checkbox" id="ad_color-170" value="No" name="insurance[]">
                            <label for="ad_color-170">No</label>
                            <span> (5) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="ad_color-247" value="Yes" name="insurance[]">
                            <label for="ad_color-247">Yes</label>
                            <span> (3) </span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="carspot_layout_type" value="2" />
                </form>
              </div>
              <div class="panel panel-default" id="red-assembly">
                <!-- Heading -->
                <div class="panel-heading" role="tab" id="ad-assembly-type">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#ad-assembly" aria-expanded="true" aria-controls="ad-assembly-type">
                      <i class="more-less glyphicon glyphicon-plus"></i> Car Assembly </a>
                  </h4>
                </div>
                <!-- Content -->
                <form method="get" action="#">
                  <div id="ad-assembly" class="panel-collapse collapse " role="tabpanel" aria-labelledby="ad-assembly-type">
                    <div class="panel-body">
                      <div class="skin-minimal">
                        <ul class="list">
                          <li>
                            <input type="checkbox" id="ad-assembly-131" value="Imported" name="assembly[]">
                            <label for="ad-assembly-131">Imported</label>
                            <span> (2) </span>
                          </li>
                          <li>
                            <input type="checkbox" id="ad-assembly-139" value="Local" name="assembly[]">
                            <label for="ad-assembly-139">Local</label>
                            <span> (6) </span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                  <input type="hidden" name="carspot_layout_type" value="2" />
                </form>
              </div>
              <div class="panel panel-default" id="red-condition">
                <!-- Heading -->
                <div class="panel-heading" role="tab" id="headingThree">
                  <h4 class="panel-title">
                    <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                      <i class="more-less glyphicon glyphicon-plus"></i> Condition </a>
                  </h4>
                </div>
                <!-- Content -->
                <form method="get" action="#">
                  <input type="hidden" name="carspot_layout_type" value="2" />
                  <div id="collapseThree" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingThree">
                    <div class="panel-body">
                      <div class="skin-minimal">
                        <ul class="list">
                          <li>
                            <input tabindex="7" type="checkbox" id="minimal-radio-166" name="condition[]" value="New">
                            <label for="minimal-radio-166">New</label>
                            <span> (0) </span>
                          </li>
                          <li>
                            <input tabindex="7" type="checkbox" id="minimal-radio-231" name="condition[]" value="Used">
                            <label for="minimal-radio-231">Used</label>
                            <span> (8) </span>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </form>
              </div>
              <div class="panel panel-default" id="red-radius">
                <!-- Heading -->
                <div class="panel-heading" role="tab" id="radius_search_heading">
                  <!-- Title -->
                  <h4 class="panel-title">
                    <a role="button" data-toggle="collapse" data-parent="#accordion" href="#radius_search" aria-expanded="true" aria-controls="radius_search">
                      <i class="more-less glyphicon glyphicon-plus"></i> Location with radius </a>
                  </h4>
                  <!-- Title End -->
                </div>
                <script>
                  (function($) {
                    'use strict';
                    /*RADIUS SEARC PLACES ON SEARCH PAGE*/
                    $(document).ready(function() {
                      function initMap() {
                        var options = {
                          types: ['(regions)'],
                          //componentRestrictions: {country: ['NL','BE']} 
                        };
                        var input = document.getElementById('searchMapInput');
                        var autocomplete = new google.maps.places.Autocomplete(input, options);
                        autocomplete.addListener('place_changed', function() {
                          var place = autocomplete.getPlace();
                          $('#location-snap').val(place.formatted_address);
                          $('#loc_lat').val(place.geometry.location.lat());
                          $('#loc_long').val(place.geometry.location.lng());
                        });
                      }
                      initMap();
                    });
                  })(jQuery);
                </script>
                <!-- Content -->
                <form method="get" id="radius_search_countries" action="#">
                  <div id="radius_search" class="panel-collapse collapse " role="tabpanel" aria-labelledby="radius_search_heading">
                    <div class="panel-body">
                      <div class="form-group">
                        <input id="searchMapInput" class="form-control" type="text" name="radius_address" placeholder="Search location" value="">
                      </div>
                      <select class="search-select form-control" id="radius_number" name="radius" data-placeholder="Select Radius">
                        <option value=""> Radius in KM </option>
                        <option value="5">5 KM </option>
                        <option value="10">10 KM </option>
                        <option value="15">15 KM </option>
                        <option value="20">20 KM </option>
                        <option value="25">25 KM </option>
                        <option value="35">35 KM </option>
                        <option value="50">50 KM </option>
                        <option value="100">100 KM </option>
                        <option value="150">150 KM </option>
                        <option value="200">200 KM </option>
                        <option value="300">300 KM </option>
                        <option value="500">500 KM </option>
                        <option value="1000">1000 KM </option>
                      </select>
                      <!--<input type="submit" class="btn btn-theme btn-sm margin-top-10 margin-bottom-10" id="search_make" value="Search" />-->
                      <input type="hidden" name="loc_long" id="loc_long" value="" />
                      <input type="hidden" name="loc_lat" id="loc_lat" value="" />
                      <input type="hidden" id="location-snap" value="">
                    </div>
                  </div>
                  <input type="hidden" name="carspot_layout_type" value="2" />
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-9 col-lg-9 col-xs-12">
          <!-- Row -->
          <div class="row">
            <div class="col-md-12 col-xs-12 col-sm-12 col-lg-12">
              <div class="clearfix"></div>
              <div class="listingTopFilterBar">
                <div class="col-md-7 col-xs-12 col-sm-6 no-padding">
                  <ul class="filterAdType">
                    <li class="active">
                      <a href="javascript:void(0)">Found Ads <small>(8)</small>
                      </a>
                    </li>
                    <li class="">
                      <a href="index.php">Reset Search</a>
                    </li>
                  </ul>
                </div>
                <div class="col-md-5 col-xs-12 col-sm-6 no-padding">
                  <div class="header-listing">
                    <h6>Sort by:</h6>
                    <div class="custom-select-box">
                      <form method="get">
                        <select name="sort" id="order_by" class="custom-select">
                          <option value="id-desc"> Newest To Oldest </option>
                          <option value="id-asc"> Oldest To New </option>
                          <option value="title-asc"> Alphabetically [a-z] </option>
                          <option value="title-desc"> Alphabetically [z-a] </option>
                          <option value="price-desc"> Highest price </option>
                          <option value="price-asc"> Lowest price </option>
                        </select>
                        <input type="hidden" name="carspot_layout_type" value="2" />
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="grid-style-2">
              <div class="">
                <div class="heading-panel">
                  <div class="col-xs-12 col-md-12 col-sm-12">
                    <h3 class="main-title text-left"> Featured Ads </h3>
                  </div>
                </div>
                <div class="featured-slider-1 owl-carousel owl-theme">
                  <div class="item">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="holder-44">
                      <div class="category-grid-box-1 featured_ads">
                        <div class="featured-ribbon">
                          <span>Featured</span>
                        </div>
                        <div class="image">
                          <a href="#" class="play-video">
                            <img src="wp-content/img1/90fb7abb94138c9d45a60fd7280a1e90.webp" alt="Icon">
                          </a>
                          <a href="#">
                            <img src="wp-content/img1/90fb7abb94138c9d45a60fd7280a1e90.webp" alt="A4 2.0 Quattro Premium Plus" class="img-responsive">
                          </a>
                          <div class="price-tag">
                            <div class="price">
                              <span>₹ 10.03 Lakh

<span class=""> (03 Lakh)</span>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="short-description-1 clearfix">
                          <div class="category-title">
                            <span class="padding_cats">
                              <a href="#">Audi</a>
                            </span>
                            <span class="padding_cats">
                              <a href="#">A4</a>
                            </span>
                          </div>
                          <h3>
                            <a href="#">A4 2.0 Quattro Premi...</a>
                          </h3>
                          <p class="location">
                            <i class="fa fa-map-marker"></i>
                            <a href="#">Carmel</a>, <a href="#">California</a>, <a href="#">United States</a>
                          </p>
                          <ul class="list-unstyled">
                            <li>
                              <i class="flaticon-gas-station-1"></i>Petrol
                            </li>
                            <li>
                              <i class="flaticon-dashboard"></i>125,000 km
                            </li>
                            <li>
                              <i class="flaticon-engine-2"></i>3500 cc
                            </li>
                            <li>
                              <i class="flaticon-car-2"></i>Coupe
                            </li>
                            <li>
                              <i class="flaticon-cogwheel-outline"></i>Blue
                            </li>
                          </ul>
                        </div>
                        <!-- Ad Meta Stats -->
                        <div class="ad-info-1">
                          <p>
                            <!-- <i class="flaticon-calendar"></i> &nbsp; <span>June 20, 2017</span> -->
                          </p>
                          <ul class="pull-right ">
                            <li>
                              <a data-toggle="tooltip" data-placement="top" data-original-title="Saved Ad" href="javascript:void(0);" class="save-ad" data-adid="44">
                                <i class="flaticon-like-1"></i>
                              </a>
                              <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                            </li>
                            <li>
                              <a href="#">
                                <i class="flaticon-message"></i>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="item">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="holder-1552">
                      <div class="category-grid-box-1 featured_ads">
                        <div class="featured-ribbon">
                          <span>Featured</span>
                        </div>
                        <div class="image">
                          <a href="#">
                            <img src="wp-content/img1/2d984be41197924a16dc1c350863135a.webp" alt="2010 Chevrolet Camaro" class="img-responsive">
                          </a>
                          <div class="price-tag">
                            <div class="price">
                              <span>₹ 12.

<span class=""> (83 Lakh)</span>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="short-description-1 clearfix">
                          <div class="category-title">
                            <span class="padding_cats">
                              <a href="#">Corvette</a>
                            </span>
                          </div>
                          <h3>
                            <a href="#">2010 Chevrolet Camaro</a>
                          </h3>
                          <p class="location">
                            <i class="fa fa-map-marker"></i>
                            <a href="#">Fort Lauderdale</a>, <a href="#">Florida</a>, <a href="#">United States</a>
                          </p>
                          <ul class="list-unstyled">
                            <li>
                              <i class="flaticon-gas-station-1"></i>Petrol
                            </li>
                            <li>
                              <i class="flaticon-dashboard"></i>23 km
                            </li>
                            <li>
                              <i class="flaticon-engine-2"></i>1500 cc
                            </li>
                            <li>
                              <i class="flaticon-car-2"></i>Coupe
                            </li>
                            <li>
                              <i class="flaticon-cogwheel-outline"></i>White
                            </li>
                          </ul>
                        </div>
                        <!-- Ad Meta Stats -->
                        <div class="ad-info-1">
                          <p>
                            <!-- <i class="flaticon-calendar"></i> &nbsp; <span>January 24, 2019</span> -->
                          </p>
                          <ul class="pull-right ">
                            <li>
                              <a data-toggle="tooltip" data-placement="top" data-original-title="Saved Ad" href="javascript:void(0);" class="save-ad" data-adid="1552">
                                <i class="flaticon-like-1"></i>
                              </a>
                              <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                            </li>
                            <li>
                              <a href="#">
                                <i class="flaticon-message"></i>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="item">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="holder-1362">
                      <div class="category-grid-box-1 featured_ads">
                        <div class="featured-ribbon">
                          <span>Featured</span>
                        </div>
                        <div class="image">
                          <a href="#">
                            <img src="wp-content/img1/28431ff5b056b3cd6bd31f3843613397.webp" alt="2017 Maserati Ghibli SQ4 Blue 1,694 Miles" class="img-responsive">
                          </a>
                          <div class="price-tag">
                            <div class="price">
                              <span>₹ 16. <span class=""> (40 Lakh)</span>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="short-description-1 clearfix">
                          <div class="category-title">
                            <span class="padding_cats">
                              <a href="#">Austin</a>
                            </span>
                            <span class="padding_cats">
                              <a href="#">Mini</a>
                            </span>
                          </div>
                          <h3>
                            <a href="#">2017 Maserati Ghibli...</a>
                          </h3>
                          <p class="location">
                            <i class="fa fa-map-marker"></i>
                            <a href="#">California</a>, <a href="#">United States</a>
                          </p>
                          <ul class="list-unstyled">
                            <li>
                              <i class="flaticon-gas-station-1"></i>Petrol
                            </li>
                            <li>
                              <i class="flaticon-dashboard"></i>1,694 km
                            </li>
                            <li>
                              <i class="flaticon-engine-2"></i>3500 cc
                            </li>
                            <li>
                              <i class="flaticon-car-2"></i>Sedan
                            </li>
                            <li>
                              <i class="flaticon-cogwheel-outline"></i>Blue
                            </li>
                          </ul>
                        </div>
                        <!-- Ad Meta Stats -->
                        <div class="ad-info-1">
                          <p>
                            <i class="flaticon-calendar"></i> &nbsp; <span>January 24, 2019</span>
                          </p>
                          <ul class="pull-right ">
                            <li>
                              <a data-toggle="tooltip" data-placement="top" data-original-title="Saved Ad" href="javascript:void(0);" class="save-ad" data-adid="1362">
                                <i class="flaticon-like-1"></i>
                              </a>
                              <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                            </li>
                            <li>
                              <a href="#">
                                <i class="flaticon-message"></i>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="item">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="holder-54">
                      <div class="category-grid-box-1 featured_ads">
                        <div class="featured-ribbon">
                          <span>Featured</span>
                        </div>
                        <div class="image">
                          <a href="#" class="play-video">
                            <img src="wp-content/img1/0c7ec6664537f7506bf9378a318f3ecc.webp" alt="Icon">
                          </a>
                          <a href="#">
                            <img src="wp-content/img1/0c7ec6664537f7506bf9378a318f3ecc.webp" alt="Honda BR-V i-VTEC S 2017" class="img-responsive">
                          </a>
                          <div class="price-tag">
                            <div class="price">
                              <span>₹ 6. <span class=""> (05 Lakh)</span>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="short-description-1 clearfix">
                          <div class="category-title">
                            <span class="padding_cats">
                              <a href="#">Honda</a>
                            </span>
                            <span class="padding_cats">
                              <a href="#">BR-V</a>
                            </span>
                          </div>
                          <h3>
                            <a href="#">Honda BR-V i-VTEC S 2017</a>
                          </h3>
                          <p class="location">
                            <i class="fa fa-map-marker"></i>
                            <a href="#">Bellevue</a>, <a href="#">Washington</a>, <a href="#">United States</a>
                          </p>
                          <ul class="list-unstyled">
                            <li>
                              <i class="flaticon-gas-station-1"></i>Hybrid
                            </li>
                            <li>
                              <i class="flaticon-dashboard"></i>25,000 km
                            </li>
                            <li>
                              <i class="flaticon-engine-2"></i>3500 cc
                            </li>
                            <li>
                              <i class="flaticon-car-2"></i>SUV
                            </li>
                            <li>
                              <i class="flaticon-cogwheel-outline"></i>White
                            </li>
                          </ul>
                        </div>
                        <!-- Ad Meta Stats -->
                        <div class="ad-info-1">
                          <p>
                            <i class="flaticon-calendar"></i> &nbsp; <span>June 20, 2017</span>
                          </p>
                          <ul class="pull-right ">
                            <li>
                              <a data-toggle="tooltip" data-placement="top" data-original-title="Saved Ad" href="javascript:void(0);" class="save-ad" data-adid="54">
                                <i class="flaticon-like-1"></i>
                              </a>
                              <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                            </li>
                            <li>
                              <a href="#">
                                <i class="flaticon-message"></i>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="item">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="holder-1554">
                      <div class="category-grid-box-1 featured_ads">
                        <div class="featured-ribbon">
                          <span>Featured</span>
                        </div>
                        <div class="image">
                          <a href="#">
                            <img src="wp-content/img1/aeccde91933f6c2a6c9a7d2178dea977.webp" alt="2017 Ford Mustang" class="img-responsive">
                          </a>
                          <div class="price-tag">
                            <div class="price">
                              <span>₹ 8.

<span class=""> (39 Lakh)</span>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="short-description-1 clearfix">
                          <div class="category-title">
                            <span class="padding_cats">
                              <a href="#">Ford</a>
                            </span>
                            <span class="padding_cats">
                              <a href="#">Mustang</a>
                            </span>
                          </div>
                          <h3>
                            <a href="#">2017 Ford Mustang</a>
                          </h3>
                          <p class="location">
                            <i class="fa fa-map-marker"></i>
                            <a href="#">Fort Lauderdale</a>, <a href="#">Florida</a>, <a href="#">United States</a>
                          </p>
                          <ul class="list-unstyled">
                            <li>
                              <i class="flaticon-gas-station-1"></i>Petrol
                            </li>
                            <li>
                              <i class="flaticon-dashboard"></i>12,454 km
                            </li>
                            <li>
                              <i class="flaticon-engine-2"></i>2000 cc
                            </li>
                            <li>
                              <i class="flaticon-car-2"></i>Coupe
                            </li>
                            <li>
                              <i class="flaticon-cogwheel-outline"></i>White
                            </li>
                          </ul>
                        </div>
                        <!-- Ad Meta Stats -->
                        <div class="ad-info-1">
                          <p>
                            <!-- <i class="flaticon-calendar"></i> &nbsp; <span>January 24, 2019</span> -->
                          </p>
                          <ul class="pull-right ">
                            <li>
                              <a data-toggle="tooltip" data-placement="top" data-original-title="Saved Ad" href="javascript:void(0);" class="save-ad" data-adid="1554">
                                <i class="flaticon-like-1"></i>
                              </a>
                              <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                            </li>
                            <li>
                              <a href="#">
                                <i class="flaticon-message"></i>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="item">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="holder-1375">
                      <div class="category-grid-box-1 featured_ads">
                        <div class="featured-ribbon">
                          <span>Featured</span>
                        </div>
                        <div class="image">
                          <a href="#">
                            <img src="wp-content/img1/aeccde91933f6c2a6c9a7d2178dea977.webp" alt="2017 Maserati Ghibli SQ4 Blue 1,695 Miles" class="img-responsive">
                          </a>
                          <div class="price-tag">
                            <div class="price">
                              <span>₹ 4.
<span class=""> (20 Lakh)</span>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="short-description-1 clearfix">
                          <div class="category-title">
                            <span class="padding_cats">
                              <a href="#">Audi</a>
                            </span>
                            <span class="padding_cats">
                              <a href="#">A3</a>
                            </span>
                          </div>
                          <h3>
                            <a href="#">2017 Maserati Ghibli...</a>
                          </h3>
                          <p class="location">
                            <i class="fa fa-map-marker"></i>
                            <a href="#">California</a>, <a href="#">United States</a>
                          </p>
                          <ul class="list-unstyled">
                            <li>
                              <i class="flaticon-gas-station-1"></i>Petrol
                            </li>
                            <li>
                              <i class="flaticon-dashboard"></i>1,694 km
                            </li>
                            <li>
                              <i class="flaticon-engine-2"></i>3000 cc
                            </li>
                            <li>
                              <i class="flaticon-car-2"></i>Sedan
                            </li>
                            <li>
                              <i class="flaticon-cogwheel-outline"></i>Blue
                            </li>
                          </ul>
                        </div>
                        <!-- Ad Meta Stats -->
                        <div class="ad-info-1">
                          <p>
                            <!-- <i class="flaticon-calendar"></i> &nbsp; <span>January 24, 2019</span> -->
                          </p>
                          <ul class="pull-right ">
                            <li>
                              <a data-toggle="tooltip" data-placement="top" data-original-title="Saved Ad" href="javascript:void(0);" class="save-ad" data-adid="1375">
                                <i class="flaticon-like-1"></i>
                              </a>
                              <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                            </li>
                            <li>
                              <a href="#">
                                <i class="flaticon-message"></i>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="item">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="holder-81">
                      <div class="category-grid-box-1 featured_ads">
                        <div class="featured-ribbon">
                          <span>Featured</span>
                        </div>
                        <div class="image">
                          <a href="#" class="play-video">
                            <img src="wp-content/img1/0d7806dac64714029f8c2d758b6cf0b9.webp" alt="Icon">
                          </a>
                          <a href="#">
                            <img src="wp-content/img1/0d7806dac64714029f8c2d758b6cf0b9.webp" alt="Mustang Shelby GT350 Coupe" class="img-responsive">
                          </a>
                          <div class="price-tag">
                            <div class="price">
                              <span>₹ 5.15 Lakh

</span>
                            </div>
                          </div>
                        </div>
                        <div class="short-description-1 clearfix">
                          <div class="category-title">
                            <span class="padding_cats">
                              <a href="#">Ford</a>
                            </span>
                            <span class="padding_cats">
                              <a href="#">Mustang</a>
                            </span>
                          </div>
                          <h3>
                            <a href="#">Mustang Shelby GT350...</a>
                          </h3>
                          <p class="location">
                            <i class="fa fa-map-marker"></i>
                            <a href="#">District of Columbia</a>, <a href="#">United States</a>
                          </p>
                          <ul class="list-unstyled">
                            <li>
                              <i class="flaticon-gas-station-1"></i>Diesel
                            </li>
                            <li>
                              <i class="flaticon-dashboard"></i>5,400 km
                            </li>
                            <li>
                              <i class="flaticon-engine-2"></i>3500 cc
                            </li>
                            <li>
                              <i class="flaticon-car-2"></i>Coupe
                            </li>
                            <li>
                              <i class="flaticon-cogwheel-outline"></i>Black
                            </li>
                          </ul>
                        </div>
                        <!-- Ad Meta Stats -->
                        <div class="ad-info-1">
                          <p>
                            <!-- <i class="flaticon-calendar"></i> &nbsp; <span>June 20, 2017</span> -->
                          </p>
                          <ul class="pull-right ">
                            <li>
                              <a data-toggle="tooltip" data-placement="top" data-original-title="Saved Ad" href="javascript:void(0);" class="save-ad" data-adid="81">
                                <i class="flaticon-like-1"></i>
                              </a>
                              <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                            </li>
                            <li>
                              <a href="#">
                                <i class="flaticon-message"></i>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="item">
                    <div class="col-md-12 col-lg-12 col-sm-12 col-xs-12" id="holder-1366">
                      <div class="category-grid-box-1 featured_ads">
                        <div class="featured-ribbon">
                          <span>Featured</span>
                        </div>
                        <div class="image">
                          <a href="#">
                            <img src="wp-content/img1/b4epota_1501805.webp" alt="Maserati Ghibli SQ4 Blue 1,695 Miles 2017" class="img-responsive">
                          </a>
                          <div class="price-tag">
                            <div class="price">
                              <span>$ 20,000 <span class=""> (Fixed)</span>
                              </span>
                            </div>
                          </div>
                        </div>
                        <div class="short-description-1 clearfix">
                          <div class="category-title">
                            <span class="padding_cats">
                              <a href="#">Audi</a>
                            </span>
                            <span class="padding_cats">
                              <a href="#">A4</a>
                            </span>
                          </div>
                          <h3>
                            <a href="#">Maserati Ghibli SQ4 ...</a>
                          </h3>
                          <p class="location">
                            <i class="fa fa-map-marker"></i>
                            <a href="#">California</a>, <a href="#">United States</a>
                          </p>
                          <ul class="list-unstyled">
                            <li>
                              <i class="flaticon-gas-station-1"></i>Petrol
                            </li>
                            <li>
                              <i class="flaticon-dashboard"></i>1,695 km
                            </li>
                            <li>
                              <i class="flaticon-engine-2"></i>2000 cc
                            </li>
                            <li>
                              <i class="flaticon-car-2"></i>Sedan
                            </li>
                            <li>
                              <i class="flaticon-cogwheel-outline"></i>Black
                            </li>
                          </ul>
                        </div>
                        <!-- Ad Meta Stats -->
                        <div class="ad-info-1">
                          <p>
                            <!-- <i class="flaticon-calendar"></i> &nbsp; <span>January 24, 2019</span> -->
                          </p>
                          <ul class="pull-right ">
                            <li>
                              <a data-toggle="tooltip" data-placement="top" data-original-title="Saved Ad" href="javascript:void(0);" class="save-ad" data-adid="1366">
                                <i class="flaticon-like-1"></i>
                              </a>
                              <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                            </li>
                            <li>
                              <a href="#">
                                <i class="flaticon-message"></i>
                              </a>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="clearfix"></div>
            <div class="posts-masonry">
              <div class="col-md-12 col-xs-12 col-sm-12">
                <div class="ads-list-archive featured_ads">
                  <!-- Image Block -->
                  <div class="col-lg-4 col-md-4 col-sm-4 no-padding">
                    <!-- Img Block -->
                    <div class="ad-archive-img">
                      <a href="index.php">
                        <img src="wp-content/img1/b4epota_1501805.webp" alt="2017 Maserati Ghibli SQ4 Blue 1,695 Miles" class="img-responsive">
                      </a>
                      <div class="featured-ribbon">
                        <span>Featured</span>
                      </div>
                    </div>
                  </div>
                  <!-- Ads Listing -->
                  <div class="clearfix visible-xs-block"></div>
                  <!-- Content Block -->
                  <div class="col-lg-8 col-md-8 col-sm-8 no-padding">
                    <!-- Ad Desc -->
                    <div class="ad-archive-desc">
                      <!-- Title -->
                      <h3>
                        <a href="#">2017 Maserati Ghibli SQ4 Blue 1,695 Miles</a>
                      </h3>
                      <!-- Category -->
                      <div class="category-title">
                        <span class="padding_cats">
                          <a href="#">Audi</a>
                        </span>
                        <span class="padding_cats">
                          <a href="#">A3</a>
                        </span>
                      </div>
                      <!-- Short Description -->
                      <div class="clearfix visible-xs-block"></div>
                      <p class="hidden-sm">If you have any questions, please feel free to contact us at 813-246-3673! And be sure to check out this vehic...</p>
                      <!-- Ad Features -->
                      <ul class="short-meta list-inline">
                        <li>
                          <a href="javascript:void(0)">Petrol</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">1694 km</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">3,000 cc</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">Blue</a>
                        </li>
                      </ul>
                      <!-- Ad History -->
                      <!-- Price -->
                      <div class="ad-price-simple"> 
      ₹ 15.<span class=""> (88 Lakh )</span>
                      </div>
                      <div class="clearfix archive-history">
                        <div class="last-updated">Posted : January 24, 2019</div>
                        <div class="ad-meta">
                          <a href="#" class="btn btn-success">
                            <i class="fa fa-phone"></i> View Details </a>
                        </div>
                      </div>
                    </div>
                    <!-- Ad Desc End -->
                  </div>
                  <!-- Content Block End -->
                </div>
                <div class="ads-list-archive featured_ads">
                  <!-- Image Block -->
                  <div class="col-lg-4 col-md-4 col-sm-4 no-padding">
                    <!-- Img Block -->
                    <div class="ad-archive-img">
                      <a href="#">
                        <img src="wp-content/img1/a3f6a4631dffe2d767fbef47cc8d4c57.webp" alt="Maserati Ghibli SQ4 Blue 1,695 Miles 2017" class="img-responsive">
                      </a>
                      <div class="featured-ribbon">
                        <span>Featured</span>
                      </div>
                    </div>
                  </div>
                  <!-- Ads Listing -->
                  <div class="clearfix visible-xs-block"></div>
                  <!-- Content Block -->
                  <div class="col-lg-8 col-md-8 col-sm-8 no-padding">
                    <!-- Ad Desc -->
                    <div class="ad-archive-desc">
                      <!-- Title -->
                      <h3>
                        <a href="#">Maserati Ghibli SQ4 Blue 1,695 Miles 2017</a>
                      </h3>
                      <!-- Category -->
                      <div class="category-title">
                        <span class="padding_cats">
                          <a href="#">Audi</a>
                        </span>
                        <span class="padding_cats">
                          <a href="#">A4</a>
                        </span>
                      </div>
                      <!-- Short Description -->
                      <div class="clearfix visible-xs-block"></div>
                      <p class="hidden-sm">If you have any questions, please feel free to contact us at 813-246-3673! And be sure to check out this vehic...</p>
                      <!-- Ad Features -->
                      <ul class="short-meta list-inline">
                        <li>
                          <a href="javascript:void(0)">Petrol</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">1695 km</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">2,000 cc</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">Black</a>
                        </li>
                      </ul>
                      <!-- Ad History -->
                      <!-- Price -->
                      <div class="ad-price-simple"> $ 20,000 <span class=""> (Fixed)</span>
                      </div>
                      <div class="clearfix archive-history">
                        <div class="last-updated">Posted : January 24, 2019</div>
                        <div class="ad-meta">
                          <a href="#" class="btn btn-success">
                            <i class="fa fa-phone"></i> View Details </a>
                        </div>
                      </div>
                    </div>
                    <!-- Ad Desc End -->
                  </div>
                  <!-- Content Block End -->
                </div>
                <div class="ads-list-archive featured_ads">
                  <!-- Image Block -->
                  <div class="col-lg-4 col-md-4 col-sm-4 no-padding">
                    <!-- Img Block -->
                    <div class="ad-archive-img">
                      <a href="#">
                        <img src="wp-content/img1/37755228310ac82acd5a140ffff9621d.webp" alt="2017 Maserati Ghibli SQ4 Blue 1,694 Miles" class="img-responsive">
                      </a>
                      <div class="featured-ribbon">
                        <span>Featured</span>
                      </div>
                    </div>
                  </div>
                  <!-- Ads Listing -->
                  <div class="clearfix visible-xs-block"></div>
                  <!-- Content Block -->
                  <div class="col-lg-8 col-md-8 col-sm-8 no-padding">
                    <!-- Ad Desc -->
                    <div class="ad-archive-desc">
                      <!-- Title -->
                      <h3>
                        <a href="#">2017 Maserati Ghibli SQ4 Blue 1,694 Miles</a>
                      </h3>
                      <!-- Category -->
                      <div class="category-title">
                        <span class="padding_cats">
                          <a href="#">Austin</a>
                        </span>
                        <span class="padding_cats">
                          <a href="#">Mini</a>
                        </span>
                      </div>
                      <!-- Short Description -->
                      <div class="clearfix visible-xs-block"></div>
                      <p class="hidden-sm">1 Owner, Factory Warranty, Fully Loaded</p>
                      <!-- Ad Features -->
                      <ul class="short-meta list-inline">
                        <li>
                          <a href="javascript:void(0)">Petrol</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">1694 km</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">3,500 cc</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">Blue</a>
                        </li>
                      </ul>
                      <!-- Ad History -->
                      <!-- Price -->
                      <div class="ad-price-simple">₹ 43  <span class=""> (Lakh)</span>
                      </div>
                      <div class="clearfix archive-history">
                        <div class="last-updated">Posted : January 24, 2019</div>
                        <div class="ad-meta">
                          <a href="#" class="btn btn-success">
                            <i class="fa fa-phone"></i> View Details </a>
                        </div>
                      </div>
                    </div>
                    <!-- Ad Desc End -->
                  </div>
                  <!-- Content Block End -->
                </div>
                <div class="ads-list-archive featured_ads">
                  <!-- Image Block -->
                  <div class="col-lg-4 col-md-4 col-sm-4 no-padding">
                    <!-- Img Block -->
                    <div class="ad-archive-img">
                      <a href="#">
                        <img src="wp-content/IMG1/usedcar_1_1_9411667906868_1667906926.webp" alt="2017 Ford Mustang" class="img-responsive">
                      </a>
                      <div class="featured-ribbon">
                        <span>Featured</span>
                      </div>
                    </div>
                  </div>
                  <!-- Ads Listing -->
                  <div class="clearfix visible-xs-block"></div>
                  <!-- Content Block -->
                  <div class="col-lg-8 col-md-8 col-sm-8 no-padding">
                    <!-- Ad Desc -->
                    <div class="ad-archive-desc">
                      <!-- Title -->
                      <h3>
                        <a href="#">2017 Ford Mustang</a>
                      </h3>
                      <!-- Category -->
                      <div class="category-title">
                        <span class="padding_cats">
                          <a href="#">Ford</a>
                        </span>
                        <span class="padding_cats">
                          <a href="#">Mustang</a>
                        </span>
                      </div>
                      <!-- Short Description -->
                      <div class="clearfix visible-xs-block"></div>
                      <p class="hidden-sm">**** Only 5,200 Miles *** &#8211; Ford Executive Vehicle &#8211; Convertible &#8211; Navigation &#8211; Shaker...</p>
                      <!-- Ad Features -->
                      <ul class="short-meta list-inline">
                        <li>
                          <a href="javascript:void(0)">Petrol</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">12454 km</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">2,000 cc</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">White</a>
                        </li>
                      </ul>
                      <!-- Ad History -->
                      <!-- Price -->
                      <div class="ad-price-simple"> $ 37,455 <span class=""> (Fixed)</span>
                      </div>
                      <div class="clearfix archive-history">
                        <div class="last-updated">Posted : January 24, 2019</div>
                        <div class="ad-meta">
                          <a href="#" class="btn btn-success">
                            <i class="fa fa-phone"></i> View Details </a>
                        </div>
                      </div>
                    </div>
                    <!-- Ad Desc End -->
                  </div>
                  <!-- Content Block End -->
                </div>
                <div class="ads-list-archive featured_ads">
                  <!-- Image Block -->
                  <div class="col-lg-4 col-md-4 col-sm-4 no-padding">
                    <!-- Img Block -->
                    <div class="ad-archive-img">
                      <a href="#">
                        <img src="https://media.tenor.com/-HXPt-K3XRYAAAAd/mg-zs-mg.gif" alt="2010 Chevrolet Camaro" class="img-responsive">
                      </a>
                      <div class="featured-ribbon">
                        <span>Featured</span>
                      </div>
                    </div>
                  </div>
                  <!-- Ads Listing -->
                  <div class="clearfix visible-xs-block"></div>
                  <!-- Content Block -->
                  <div class="col-lg-8 col-md-8 col-sm-8 no-padding">
                    <!-- Ad Desc -->
                    <div class="ad-archive-desc">
                      <!-- Title -->
                      <h3>
                        <a href="#">2010 Chevrolet Camaro</a>
                      </h3>
                      <!-- Category -->
                      <div class="category-title">
                        <span class="padding_cats">
                          <a href="#">Corvette</a>
                        </span>
                      </div>
                      <!-- Short Description -->
                      <div class="clearfix visible-xs-block"></div>
                      <p class="hidden-sm">**** Only 5,200 Miles *** &#8211; Ford Executive Vehicle &#8211; Convertible &#8211; Navigation &#8211; Shaker...</p>
                      <!-- Ad Features -->
                      <ul class="short-meta list-inline">
                        <li>
                          <a href="javascript:void(0)">Petrol</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">23,000 km</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">1,500 cc</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">White</a>
                        </li>
                      </ul>
                      <!-- Ad History -->
                      <!-- Price -->
                      <div class="ad-price-simple"> $ 37,345 <span class=""> (Negotiable)</span>
                      </div>
                      <div class="clearfix archive-history">
                        <div class="last-updated">Posted : January 24, 2019</div>
                        <div class="ad-meta">
                          <a href="#" class="btn btn-success">
                            <i class="fa fa-phone"></i> View Details </a>
                        </div>
                      </div>
                    </div>
                    <!-- Ad Desc End -->
                  </div>
                  <!-- Content Block End -->
                </div>
                <div class="ads-list-archive featured_ads">
                  <!-- Image Block -->
                  <div class="col-lg-4 col-md-4 col-sm-4 no-padding">
                    <!-- Img Block -->
                    <div class="ad-archive-img">
                      <a href="#" class="play-video">
                        <img src="wp-content/img1/b4159718308bc8fa545704c777a1219f.webp" alt="Icon">
                      </a>
                      <a href="#">
                        <img src="wp-content/img1/b4159718308bc8fa545704c777a1219f.webp" alt="Mustang Shelby GT350 Coupe" class="img-responsive">
                      </a>
                      <div class="featured-ribbon">
                        <span>Featured</span>
                      </div>
                    </div>
                  </div>
                  <!-- Ads Listing -->
                  <div class="clearfix visible-xs-block"></div>
                  <!-- Content Block -->
                  <div class="col-lg-8 col-md-8 col-sm-8 no-padding">
                    <!-- Ad Desc -->
                    <div class="ad-archive-desc">
                      <!-- Title -->
                      <h3>
                        <a href="#">Mustang Shelby GT350 Coupe</a>
                      </h3>
                      <!-- Category -->
                      <div class="category-title">
                        <span class="padding_cats">
                          <a href="#">Ford</a>
                        </span>
                        <span class="padding_cats">
                          <a href="#">Mustang</a>
                        </span>
                      </div>
                      <!-- Short Description -->
                      <div class="clearfix visible-xs-block"></div>
                      <p class="hidden-sm">It is the customer&#8217;s sole responsibility to verify the existence and condition of any equipment listed. ...</p>
                      <!-- Ad Features -->
                      <ul class="short-meta list-inline">
                        <li>
                          <a href="javascript:void(0)">Diesel</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">5400 km</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">3,500 cc</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">Black</a>
                        </li>
                      </ul>
                      <!-- Ad History -->
                      <!-- Price -->
                      <div class="ad-price-simple"> $ 17,000 </div>
                      <div class="clearfix archive-history">
                        <div class="last-updated">Posted : June 20, 2017</div>
                        <div class="ad-meta">
                          <a href="#" class="btn btn-success">
                            <i class="fa fa-phone"></i> View Details </a>
                        </div>
                      </div>
                    </div>
                    <!-- Ad Desc End -->
                  </div>
                  <!-- Content Block End -->
                </div>
                <div class="ads-list-archive featured_ads">
                  <!-- Image Block -->
                  <div class="col-lg-4 col-md-4 col-sm-4 no-padding">
                    <!-- Img Block -->
                    <div class="ad-archive-img">
                      <a href="#" class="play-video">
                        <img src="https://img.etimg.com/thumb/height-450,width-600,imgsize-1562086,msid-81435046/.jpg" alt="Icon">
                      </a>
                      <a href="#">
                        <img src="https://img.etimg.com/thumb/height-450,width-600,imgsize-1562086,msid-81435046/.jpg" alt="Honda BR-V i-VTEC S 2017" class="img-responsive">
                      </a>
                      <div class="featured-ribbon">
                        <span>Featured</span>
                      </div>
                    </div>
                  </div>
                  <!-- Ads Listing -->
                  <div class="clearfix visible-xs-block"></div>
                  <!-- Content Block -->
                  <div class="col-lg-8 col-md-8 col-sm-8 no-padding">
                    <!-- Ad Desc -->
                    <div class="ad-archive-desc">
                      <!-- Title -->
                      <h3>
                        <a href="#">Honda BR-V i-VTEC S 2017</a>
                      </h3>
                      <!-- Category -->
                      <div class="category-title">
                        <span class="padding_cats">
                          <a href="#">Honda</a>
                        </span>
                        <span class="padding_cats">
                          <a href="#">BR-V</a>
                        </span>
                      </div>
                      <!-- Short Description -->
                      <div class="clearfix visible-xs-block"></div>
                      <p class="hidden-sm">All original are complete.All taxes paid.Price is slightly negotiable.documentsOriginal Book is available.In G...</p>
                      <!-- Ad Features -->
                      <ul class="short-meta list-inline">
                        <li>
                          <a href="javascript:void(0)">Hybrid</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">25000 km</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">3,500 cc</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">White</a>
                        </li>
                      </ul>
                      <!-- Ad History -->
                      <!-- Price -->
                      <div class="ad-price-simple"> $ 40,000 <span class=""> (Negotiable)</span>
                      </div>
                      <div class="clearfix archive-history">
                        <div class="last-updated">Posted : June 20, 2017</div>
                        <div class="ad-meta">
                          <a href="#" class="btn btn-success">
                            <i class="fa fa-phone"></i> View Details </a>
                        </div>
                      </div>
                    </div>
                    <!-- Ad Desc End -->
                  </div>
                  <!-- Content Block End -->
                </div>
                <div class="ads-list-archive featured_ads">
                  <!-- Image Block -->
                  <div class="col-lg-4 col-md-4 col-sm-4 no-padding">
                    <!-- Img Block -->
                    <div class="ad-archive-img">
                      <a href="#" class="play-video">
                        <img src="wp-content/img1/aa8a757d7d62d13a72d451ec58d877f6.webp" alt="Icon">
                      </a>
                      <a href="#">
                        <img src="wp-content/img1/aa8a757d7d62d13a72d451ec58d877f6.webp" alt="A4 2.0 Quattro Premium Plus" class="img-responsive">
                      </a>
                      <div class="featured-ribbon">
                        <span>Featured</span>
                      </div>
                    </div>
                  </div>
                  <!-- Ads Listing -->
                  <div class="clearfix visible-xs-block"></div>
                  <!-- Content Block -->
                  <div class="col-lg-8 col-md-8 col-sm-8 no-padding">
                    <!-- Ad Desc -->
                    <div class="ad-archive-desc">
                      <!-- Title -->
                      <h3>
                        <a href="#">A4 2.0 Quattro Premium Plus</a>
                      </h3>
                      <!-- Category -->
                      <div class="category-title">
                        <span class="padding_cats">
                          <a href="#">Audi</a>
                        </span>
                        <span class="padding_cats">
                          <a href="#">A4</a>
                        </span>
                      </div>
                      <!-- Short Description -->
                      <div class="clearfix visible-xs-block"></div>
                      <p class="hidden-sm">It is a flying machine with ultimate luxuries to spoil yourself All 4 new tyres just done 125000 kilometres fu...</p>
                      <!-- Ad Features -->
                      <ul class="short-meta list-inline">
                        <li>
                          <a href="javascript:void(0)">Petrol</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">125000 km</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">3,500 cc</a>
                        </li>
                        <li>
                          <a href="javascript:void(0)">Blue</a>
                        </li>
                      </ul>
                      <!-- Ad History -->
                      <!-- Price -->
                      <div class="ad-price-simple"> ₹ 5.<span class=""> (50 Lakh)</span>
                      </div>
                      <div class="clearfix archive-history">
                        <div class="last-updated">Posted : June 20, 2017</div>
                        <div class="ad-meta">
                          <a href="#" class="btn btn-success">
                            <i class="fa fa-phone"></i> View Details </a>
                        </div>
                      </div>
                    </div>


                    
                    <div class="clear">
          
                             </div>
                            </li>
                          </ul>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
              </div>
            </div>
          </div>
        </div>
      </section>
    <?php include 'include/footer.php';?>