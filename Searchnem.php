

<?php include "include/header.php" ?>

    <section class="transparent-breadcrumb-listing" style="background: url(../wp-content/uploads/2018/04/services-bg1.png); background-repeat: no-repeat ; background-size: cover ;  background-position: center center ;  background-attachment: scroll; ">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="list-heading">
              <h2>Contact Us</h2>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="wpb-content-wrapper">
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="section-padding gray  ">
                <div class="container">
                  <div class="row">
                    <div class="col-md-12 col-sm-12 col-xs-12 no-padding commentForm">
                      <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <div role="form" class="wpcf7" id="wpcf7-f345-p347-o1" lang="en-US" dir="ltr">
                          <div class="screen-reader-response">
                            <p role="status" aria-live="polite" aria-atomic="true"></p>
                            <ul></ul>
                          </div>
                          <form action="https://carspot.scriptsbundle.com/contact-us/#wpcf7-f345-p347-o1" method="post" class="wpcf7-form init" novalidate="novalidate" data-status="init">
                            <div style="display: none;">
                              <input type="hidden" name="_wpcf7" value="345" />
                              <input type="hidden" name="_wpcf7_version" value="5.6.4" />
                              <input type="hidden" name="_wpcf7_locale" value="en_US" />
                              <input type="hidden" name="_wpcf7_unit_tag" value="wpcf7-f345-p347-o1" />
                              <input type="hidden" name="_wpcf7_container_post" value="347" />
                              <input type="hidden" name="_wpcf7_posted_data_hash" value="" />
                            </div>
                            <div class="row">
                              <div class="col-sm-12 col-md-6 col-xs-12 clearfix">
                                <div class="form-group">
                                  <span class="wpcf7-form-control-wrap" data-name="your-name">
                                    <input type="text" name="your-name" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false" placeholder="Name" />
                                  </span>
                                </div>
                                <div class="form-group">
                                  <span class="wpcf7-form-control-wrap" data-name="your-email">
                                    <input type="email" name="your-email" value="" size="40" class="wpcf7-form-control wpcf7-text wpcf7-email wpcf7-validates-as-required wpcf7-validates-as-email form-control" aria-required="true" aria-invalid="false" placeholder="Email" />
                                  </span>
                                </div>
                                <div class="form-group">
                                  <span class="wpcf7-form-control-wrap" data-name="your-subject">
                                    <input type="text" name="your-subject" value="" size="40" class="wpcf7-form-control wpcf7-text form-control" aria-invalid="false" placeholder="Subject" />
                                  </span>
                                </div>
                              </div>
                              <div class="col-lg-6 col-md-6 col-xs-12">
                                <div class="form-group">
                                  <span class="wpcf7-form-control-wrap" data-name="your-message">
                                    <textarea name="your-message" cols="30" rows="7" class="wpcf7-form-control wpcf7-textarea wpcf7-validates-as-required form-control" aria-required="true" aria-invalid="false" placeholder="Message"></textarea>
                                  </span>
                                </div>
                              </div>
                              <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                                <input type="submit" value="Contact Us" class="wpcf7-form-control has-spinner wpcf7-submit btn-theme btn" />
                              </div>
                            </div>
                            <div class="wpcf7-response-output" aria-hidden="true"></div>
                          </form>
                        </div>
                      </div>
                      <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="contactInfo">
                          <div class="singleContadds">
                            <i class="fa fa-map-marker"></i>
                            <p> Model Town Link Road Lahore, 60 Street. Pakistan 54770 </p>
                          </div>
                          <div class="singleContadds phone">
                            <i class="fa fa-phone"></i>
                            <p>0123 456 78 90 - Office <br /> 0123 456 78 90 - Mobile </p>
                          </div>
                          <div class="singleContadds">
                            <i class="fa fa-envelope"></i> contact@scriptsbundle.com <br /> contact@scriptsbundle.com
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </section>
    <?php include 'include/footer.php';?>