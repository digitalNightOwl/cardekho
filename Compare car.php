



<?php include "include/header.php" ?>
    <section class="transparent-breadcrumb-listing" style="background: url(wp-content/img1/d2.jpg); background-repeat: no-repeat ; background-size: cover ;  background-position: center center ;  background-attachment: scroll; ">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="list-heading">
              <h2>Compare cars</h2>
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="wpb-content-wrapper">
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="custom-padding  gray ">
                <div class="container">
                  <div class="row">
                    <div class="col-md-8 col-xs-12 col-sm-12">
                      <div class="row">
                        <!-- Car Comparison Archive -->
                        <ul class="compare-masonry text-center">
                          <li class="col-md-12 col-sm-6 col-xs-12">
                            <div class="comparison-box">
                              <a href="#">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                  <div class="compare-grid">
                                    <img src="wp-content/img1/MG-Astor-200920211457.jpg" alt="imag not found" class="img-responsive" />
                                    
                                    <div class="rating">
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-o"></i>
                                      <span class="star-score"> ( <strong>1</strong>) </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="vsbox">vs</div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                  <div class="compare-grid">
                                    <img src="wp-content/img1/15b1e266b974ebccf949c6f01155b8ff.png" alt="imag not found" class="img-responsive" />
                                    
                                    <div class="rating">
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-o"></i>
                                      <i class="fa fa-star-o"></i>
                                      <span class="star-score"> ( <strong>2</strong>) </span>
                                    </div>
                                  </div>
                                </div>
                              </a>
                            </div>
                          </li>
                          <li></li>
                          <li class="col-md-12 col-sm-6 col-xs-12">
                            <div class="comparison-box">
                              <a href="wp-content/img1/zs-ev-exterior-right-front-three-quarter-2.webp">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                  <div class="compare-grid">
                                    <img src="wp-content/img1/zs-ev-exterior-right-front-three-quarter-2.webp" alt="imag not found" class="img-responsive" />
                                    <h2>Smart 20.32cm (8.0”)</h2>
                                    <div class="rating">
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <span class="star-score"> ( <strong>5</strong>) </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="vsbox">vs</div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                  <div class="compare-grid">
                                    <img src="wp-content/img1/kisspng-kia-motors-car-2019-kia-sportage-lx-2019-kia-sport-sportage-andrew-simms-5b675ca30e3236.3076012915335005790582.jpg" alt="imag not found" class="img-responsive" />
                                    <h2> Head-Up Display (HUD)</h2>
                                    <div class="rating">
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-o"></i>
                                      <span class="star-score"> ( <strong>4</strong>) </span>
                                    </div>
                                  </div>
                                </div>
                              </a>
                            </div>
                          </li>
                          <li></li>
                          <li class="col-md-12 col-sm-6 col-xs-12">
                            <div class="comparison-box">
                              <a href="wp-content/img1/800X600.webp">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                  <div class="compare-grid">
                                    <img src="wp-content/img1/800X600.webp" alt="imag not found" class="img-responsive" />
                                    <h2>Trend Leading Innovator</h2>
                                    <div class="rating">
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-o"></i>
                                      <i class="fa fa-star-o"></i>
                                      <span class="star-score"> ( <strong>3</strong>) </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="vsbox">vs</div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                  <div class="compare-grid">
                                    <img src="https://www.kia.com/content/dam/kia2/in/en/images/our-vehicles/seltos/showroom/RoomForEverything-unplanned-trip-new.png" alt="imag not found" class="img-responsive" />
                                    <h2>Room For Everything</h2>
                                    <div class="rating">
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-o"></i>
                                      <span class="star-score"> ( <strong>4</strong>) </span>
                                    </div>
                                  </div>
                                </div>
                              </a>
                            </div>
                          </li>
                          <li></li>
                          <li class="col-md-12 col-sm-6 col-xs-12">
                            <div class="comparison-box">
                              <a href="wp-content/img1/HIW_banner_01.webp">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                  <div class="compare-grid">
                                    <img src="wp-content/img1/HIW_banner_01.webp" alt="imag not found" class="img-responsive" />
                                    <h2>Audi A5 Sport</h2>
                                    <div class="rating">
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <span class="star-score"> ( <strong>5</strong>) </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="vsbox">vs</div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                  <div class="compare-grid">
                                    <img src="wp-content/img1/kia-carens.webp" alt="imag not found" class="img-responsive" />
                                    <h2>Alfa Romeo 8C</h2>
                                    <div class="rating">
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-o"></i>
                                      <span class="star-score"> ( <strong>4</strong>) </span>
                                    </div>
                                  </div>
                                </div>
                              </a>
                            </div>
                          </li>
                          <li></li>
                          <li class="col-md-12 col-sm-6 col-xs-12">
                            <div class="comparison-box">
                              <a href="wp-content/img1/KiaSonet1stAnniversaryEdition.webp">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                  <div class="compare-grid">
                                    <img src="wp-content/img1/Kia-Carens-Exterior (1).webp" alt="imag not found" class="img-responsive" />
                                    <h2>Mercedes-Benz C-Class</h2>
                                    <div class="rating">
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <span class="star-score"> ( <strong>5</strong>) </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="vsbox">vs</div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                  <div class="compare-grid">
                                    <img src="wp-content/img1/05August2021.webp" alt="imag not found" class="img-responsive" />
                                    <h2>Audi A5 Sport</h2>
                                    <div class="rating">
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <span class="star-score"> ( <strong>5</strong>) </span>
                                    </div>
                                  </div>
                                </div>
                              </a>
                            </div>
                          </li>
                          <li></li>
                        </ul>
                        <div class="clearfix"></div>
                        <div class="text-center">
                          <div class="load-more-btn"></div>
                        </div>
                      </div>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-12">
                      <div class="blog-sidebar">
                        <!-- Heading -->
                        <div class="widget">
                          <div class="latest-news">
                            <div class="widget-heading">
                              <h4 class="panel-title">
                                <a> Latest Reviews </a>
                              </h4>
                            </div>
                            <div class="recent-ads">
                              <div class="recent-ads-list">
                                <div class="recent-ads-container">
                                  <div class="recent-ads-list-image">
                                    <a href="wp-content/img1/Refreshed_Kia_Carnival.webp" class="recent-ads-list-image-inner">
                                      <img src="wp-content/img1/Refreshed_Kia_Carnival.webp" alt="2017 Mercedes-Benz E-Class Coupe review" />
                                    </a>
                                    <!-- /.recent-ads-list-image-inner -->
                                  </div>
                                  <!-- /.recent-ads-list-image -->
                                  <div class="recent-ads-list-content">
                                    <h3 class="recent-ads-list-title">
                                      <a href="index.php">Kia Reveals Official Sketches of Kia Carens – Bold, Premium and Sophisticated Recreational Vehicle</a>
                                    </h3>
                                    <ul class="recent-ads-list-location">
                                      <li>
                                        <a href="javascript:void(0)"></a>
                                      </li>
                                    </ul>
                                    <!-- /.recent-ads-list-price -->
                                  </div>
                                  <!-- /.recent-ads-list-content -->
                                </div>
                                <!-- /.recent-ads-container -->
                              </div>
                              <div class="recent-ads-list">
                                <div class="recent-ads-container">
                                  <div class="recent-ads-list-image">
                                    <a href="wp-content/img1/25Jan2021.webp" class="recent-ads-list-image-inner">
                                      <img src="wp-content/img1/25Jan2021.webp" alt="2017 BMW M760 Sports &#038; Luxury Car" />
                                    </a>
                                    <!-- /.recent-ads-list-image-inner -->
                                  </div>
                                  <!-- /.recent-ads-list-image -->
                                  <div class="recent-ads-list-content">
                                    <h3 class="recent-ads-list-title">
                                      <a href="wp-content/img1/25Jan2021.webp">Kia Motors India reaches the milestone within  17 months of sales operations in the country</a>
                                    </h3>
                                    <ul class="recent-ads-list-location">
                                      <li>
                                        <a href="javascript:void(0)"></a>
                                      </li>
                                    </ul>
                                    <!-- /.recent-ads-list-price -->
                                  </div>
                                  <!-- /.recent-ads-list-content -->
                                </div>
                                <!-- /.recent-ads-container -->
                              </div>
                              <div class="recent-ads-list">
                                <div class="recent-ads-container">
                                  <div class="recent-ads-list-image">
                                    <a href="wp-content/img1/Refreshed_Kia_Carnival (1).webp" class="recent-ads-list-image-inner">
                                      <img src="wp-content/img1/Refreshed_Kia_Carnival (1).webp" alt="2018 Mercedes-Benz AMG GT Roadster First Drive" />
                                    </a>
                                    <!-- /.recent-ads-list-image-inner -->
                                  </div>
                                  <!-- /.recent-ads-list-image -->
                                  <div class="recent-ads-list-content">
                                    <h3 class="recent-ads-list-title">
                                      <a href="wp-content/img1/Refreshed_Kia_Carnival (1).webp">Kia introduces refreshed Carnival premium MPV with multiple new features, trim options</a>
                                    </h3>
                                    <ul class="recent-ads-list-location">
                                      <li>
                                        <a href="javascript:void(0)"></a>
                                      </li>
                                    </ul>
                                    <!-- /.recent-ads-list-price -->
                                  </div>
                                  <!-- /.recent-ads-list-content -->
                                </div>
                                <!-- /.recent-ads-container -->
                              </div>
                            </div>
                          </div>
                        </div>
                        <!-- Heading -->
                        <div class="widget">
                          <div class="widget-heading">
                            <h4 class="panel-title">
                              <a> Reviews By Categories </a>
                            </h4>
                          </div>
                          <div class="categories">
                            <ul>
                              <li>
                                <a href="Used car Dealers.php"> Used car Dealers <span>&nbsp;(2)</span>
                                </a>
                              </li>
                              <li>
                                <a href="">Feature Stories<span>&nbsp;(2)</span>
                                </a>
                              </li>
                              <li>
                                <a href="car News.php"> car News<span>&nbsp;(1)</span>
                                </a>
                              </li>
                              <li>
                                <a href="Used car Dealers.php">  Used car Dealers<span>&nbsp;(1)</span>
                                </a>
                              </li>
                              <li>
                                <a href="Used car valuation .php"> Used car valuation <span>&nbsp;(2)</span>
                                </a>
                              </li>
                            </ul>
                          </div>
                        </div>
                        <!-- Heading -->
                       
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div class="clear">

    </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </section>
	<?php include 'include/footer.php';?>