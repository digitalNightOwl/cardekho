
<?php include "include/header.php" ?> 
    <section class="wpb-content-wrapper">
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <div id="banner" style="background: rgba(0, 0, 0, 0) url(wp-content/img/pexels-mike-b-9265832.jpg) center center no-repeat; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
                <div class="container">
                  <div class="search-container">
                   
                    <a href="# " class="btn btn-theme"> Post Free Ad </a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <div class="advance-search">
                <div class="section-search search-style-2">
                  <div class="container">
                    <div class="row">
                      <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs">
                          <li class="nav-item active">
                            <a class="nav-link" data-toggle="tab" href="#tab1">Search Car In Details </a>
                          </li>
                          <li class="nav-item">
                            <a class="nav-link" data-toggle="tab" href="#tab2">Search Car By Type</a>
                          </li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content clearfix">
                          <div class="tab-pane fade in active" id="tab1">
                            <form method="get" action="#">
                              <div class="search-form pull-left ">
                                <div class="search-form-inner pull-left ">
                                  <div class="col-md-4 no-padding">
                                    <div class="form-group">
                                      <label>Keyword</label>
                                      <input autocomplete="off" name="ad_title" id="autocomplete-dynamic" class="form-control banner-icon-search" placeholder="What Are You Looking For..." type="text" />
                                    </div>
                                  </div>
                                  <div class="col-md-4 no-padding">
                                    <div class="form-group">
                                      <label>Select Make</label>
                                      <select class="form-control select-make" name="cat_id">
                                        <option label="Select Make" value="">Select Make</option>
                                        <option value="540">Alfa Romeo (0)</option>
                                        <option value="541">&nbsp;&nbsp; - &nbsp;Allard (0) </option>
                                        <option value="543">&nbsp;&nbsp; - &nbsp;Alpina (0) </option>
                                        <option value="544">&nbsp;&nbsp; - &nbsp;Alpine (0) </option>
                                        <option value="545">&nbsp;&nbsp; - &nbsp;Alvis (0) </option>
                                        <option value="547">&nbsp;&nbsp; - &nbsp;AMC (0) </option>
                                        <option value="548">&nbsp;&nbsp; - &nbsp;Ariel (0) </option>
                                        <option value="662">&nbsp;&nbsp; - &nbsp;Mito (0) </option>
                                        <option value="731">&nbsp;&nbsp; - &nbsp;Spider (0) </option>
                                        <option value="549">Aston Martin (0)</option>
                                        <option value="586">&nbsp;&nbsp; - &nbsp;DB 11 (0) </option>
                                        <option value="587">&nbsp;&nbsp; - &nbsp;DB 7 (0) </option>
                                        <option value="588">&nbsp;&nbsp; - &nbsp;DB 9 (0) </option>
                                        <option value="589">&nbsp;&nbsp; - &nbsp;DBS (0) </option>
                                        <option value="704">&nbsp;&nbsp; - &nbsp;Rapide (0) </option>
                                        <option value="757">&nbsp;&nbsp; - &nbsp;V 12 (0) </option>
                                        <option value="758">&nbsp;&nbsp; - &nbsp;V8 (0) </option>
                                        <option value="761">&nbsp;&nbsp; - &nbsp;VANTAGE (0) </option>
                                        <option value="551">Audi (4)</option>
                                        <option value="808">&nbsp;&nbsp; - &nbsp;A1 (0) </option>
                                        <option value="809">&nbsp;&nbsp; - &nbsp;A3 (1) </option>
                                        <option value="810">&nbsp;&nbsp; - &nbsp;A4 (2) </option>
                                        <option value="811">&nbsp;&nbsp; - &nbsp;A5 (1) </option>
                                        <option value="812">&nbsp;&nbsp; - &nbsp;A6 (0) </option>
                                        <option value="813">&nbsp;&nbsp; - &nbsp;A7 (0) </option>
                                        <option value="701">&nbsp;&nbsp; - &nbsp;Q3 (0) </option>
                                        <option value="702">&nbsp;&nbsp; - &nbsp;R8 (0) </option>
                                        <option value="749">&nbsp;&nbsp; - &nbsp;TT (0) </option>
                                        <option value="554">Austin (1)</option>
                                        <option value="789">&nbsp;&nbsp; - &nbsp;10 (0) </option>
                                        <option value="614">&nbsp;&nbsp; - &nbsp;Fx4 (0) </option>
                                        <option value="657">&nbsp;&nbsp; - &nbsp;Maxi (0) </option>
                                        <option value="661">&nbsp;&nbsp; - &nbsp;Mini (1) </option>
                                        <option value="555">Austin Healey (0)</option>
                                        <option value="557">Aveo (0)</option>
                                        <option value="558">Bentley (0)</option>
                                        <option value="562">BMW (0)</option>
                                        <option value="782">&nbsp;&nbsp; - &nbsp;1 Series (0) </option>
                                        <option value="790">&nbsp;&nbsp; - &nbsp;2 Series (0) </option>
                                        <option value="792">&nbsp;&nbsp; - &nbsp;3 Series (0) </option>
                                        <option value="795">&nbsp;&nbsp; - &nbsp;4 Series (0) </option>
                                        <option value="798">&nbsp;&nbsp; - &nbsp;5 Series (0) </option>
                                        <option value="799">&nbsp;&nbsp; - &nbsp;6 Series (0) </option>
                                        <option value="800">&nbsp;&nbsp; - &nbsp;7 Series (0) </option>
                                        <option value="806">&nbsp;&nbsp; - &nbsp;8 Series (0) </option>
                                        <option value="619">&nbsp;&nbsp; - &nbsp;Gt (0) </option>
                                        <option value="631">&nbsp;&nbsp; - &nbsp;i8 (0) </option>
                                        <option value="642">&nbsp;&nbsp; - &nbsp;M series (0) </option>
                                        <option value="779">&nbsp;&nbsp; - &nbsp;Z3 (0) </option>
                                        <option value="780">&nbsp;&nbsp; - &nbsp;Z4 (0) </option>
                                        <option value="781">&nbsp;&nbsp; - &nbsp;Z8 (0) </option>
                                        <option value="566">Bolt (0)</option>
                                        <option value="568">Brothers (0)</option>
                                        <option value="570">Camaro (0)</option>
                                        <option value="575">Charger (0)</option>
                                        <option value="580">Corvette (1)</option>
                                        <option value="585">Dart (0)</option>
                                        <option value="601">Ferrari (0)</option>
                                        <option value="796">&nbsp;&nbsp; - &nbsp;458 Italia (0) </option>
                                        <option value="797">&nbsp;&nbsp; - &nbsp;488 (0) </option>
                                        <option value="841">&nbsp;&nbsp; - &nbsp;California (0) </option>
                                        <option value="843">&nbsp;&nbsp; - &nbsp;California T (0) </option>
                                        <option value="892">&nbsp;&nbsp; - &nbsp;F430 Spider (0) </option>
                                        <option value="605">Ford (2)</option>
                                        <option value="823">&nbsp;&nbsp; - &nbsp;Anglia (0) </option>
                                        <option value="837">&nbsp;&nbsp; - &nbsp;Bronco (0) </option>
                                        <option value="862">&nbsp;&nbsp; - &nbsp;Cortina (0) </option>
                                        <option value="867">&nbsp;&nbsp; - &nbsp;Crown Victoria (0) </option>
                                        <option value="884">&nbsp;&nbsp; - &nbsp;Escort (0) </option>
                                        <option value="891">&nbsp;&nbsp; - &nbsp;F 150 (0) </option>
                                        <option value="893">&nbsp;&nbsp; - &nbsp;Fairlane (0) </option>
                                        <option value="666">&nbsp;&nbsp; - &nbsp;Mustang (2) </option>
                                        <option value="703">&nbsp;&nbsp; - &nbsp;Ranger (0) </option>
                                        <option value="623">Honda (1)</option>
                                        <option value="817">&nbsp;&nbsp; - &nbsp;Accord (0) </option>
                                        <option value="818">&nbsp;&nbsp; - &nbsp;Accord Euro (0) </option>
                                        <option value="835">&nbsp;&nbsp; - &nbsp;BR-V (1) </option>
                                        <option value="855">&nbsp;&nbsp; - &nbsp;City (0) </option>
                                        <option value="856">&nbsp;&nbsp; - &nbsp;Civic (0) </option>
                                        <option value="866">&nbsp;&nbsp; - &nbsp;CR-V (0) </option>
                                        <option value="765">&nbsp;&nbsp; - &nbsp;Vezel (0) </option>
                                        <option value="627">Hummer (0)</option>
                                        <option value="903">&nbsp;&nbsp; - &nbsp;H1 (0) </option>
                                        <option value="904">&nbsp;&nbsp; - &nbsp;H2 (0) </option>
                                        <option value="905">&nbsp;&nbsp; - &nbsp;H3 (0) </option>
                                        <option value="634">Jaguar (0)</option>
                                        <option value="636">Joy (0)</option>
                                        <option value="783">&nbsp;&nbsp; - &nbsp;1.0 (0) </option>
                                        <option value="784">&nbsp;&nbsp; - &nbsp;1.0 CNG (0) </option>
                                        <option value="637">Lamborghini (0)</option>
                                        <option value="829">&nbsp;&nbsp; - &nbsp;Aventador (0) </option>
                                        <option value="839">&nbsp;&nbsp; - &nbsp;Cabrera (0) </option>
                                        <option value="864">&nbsp;&nbsp; - &nbsp;Countach (0) </option>
                                        <option value="871">&nbsp;&nbsp; - &nbsp;Diablo (0) </option>
                                        <option value="898">&nbsp;&nbsp; - &nbsp;Gallardo (0) </option>
                                        <option value="900">&nbsp;&nbsp; - &nbsp;Gt (0) </option>
                                        <option value="665">&nbsp;&nbsp; - &nbsp;Murcielago (0) </option>
                                        <option value="638">Land Rover (1)</option>
                                        <option value="658">McLaren (0)</option>
                                        <option value="659">Mercedes (0)</option>
                                        <option value="838">&nbsp;&nbsp; - &nbsp;C Class (0) </option>
                                        <option value="876">&nbsp;&nbsp; - &nbsp;E Class (0) </option>
                                        <option value="715">&nbsp;&nbsp; - &nbsp;S Class (0) </option>
                                        <option value="716">&nbsp;&nbsp; - &nbsp;S Series (0) </option>
                                        <option value="728">&nbsp;&nbsp; - &nbsp;SLK Class (0) </option>
                                        <option value="663">Mitsubishi (0)</option>
                                        <option value="877">&nbsp;&nbsp; - &nbsp;Ek Wagon (0) </option>
                                        <option value="920">&nbsp;&nbsp; - &nbsp;Lancer (0) </option>
                                        <option value="927">&nbsp;&nbsp; - &nbsp;Mirage (0) </option>
                                        <option value="681">&nbsp;&nbsp; - &nbsp;Pajero (0) </option>
                                        <option value="682">&nbsp;&nbsp; - &nbsp;Pajero Mini (0) </option>
                                        <option value="674">Nissan (0)</option>
                                        <option value="793">&nbsp;&nbsp; - &nbsp;350Z (0) </option>
                                        <option value="794">&nbsp;&nbsp; - &nbsp;370Z (0) </option>
                                        <option value="901">&nbsp;&nbsp; - &nbsp;GT-R (0) </option>
                                        <option value="726">&nbsp;&nbsp; - &nbsp;SILVIA (0) </option>
                                        <option value="727">&nbsp;&nbsp; - &nbsp;SKYLINE (0) </option>
                                        <option value="676">Nitro (0)</option>
                                        <option value="680">Optra (0)</option>
                                        <option value="785">&nbsp;&nbsp; - &nbsp;1.4 (0) </option>
                                        <option value="787">&nbsp;&nbsp; - &nbsp;1.6 Automatic (0) </option>
                                        <option value="788">&nbsp;&nbsp; - &nbsp;1.8 Automatic (0) </option>
                                        <option value="686">Porsche (0)</option>
                                        <option value="807">&nbsp;&nbsp; - &nbsp;911 (0) </option>
                                        <option value="834">&nbsp;&nbsp; - &nbsp;Boxster (0) </option>
                                        <option value="853">&nbsp;&nbsp; - &nbsp;Cayenne (0) </option>
                                        <option value="933">&nbsp;&nbsp; - &nbsp;Panamera (0) </option>
                                        <option value="708">Renault (1)</option>
                                        <option value="712">Rolls-Royce (1)</option>
                                        <option value="729">Sonic (0)</option>
                                        <option value="730">Spark (0)</option>
                                        <option value="736">Stealth (0)</option>
                                        <option value="741">Suzuki (0)</option>
                                        <option value="766">Viper (0)</option>
                                        <option value="769">Volt (0)</option>
                                      </select>
                                    </div>
                                  </div>
                                  <div class="col-md-4 no-padding">
                                    <div class="form-group">
                                      <label>Select Year</label>
                                      <select class=" orm-control" name="year_from">
                                        <option label="Select Year" value="">Select Year</option>
                                        <option value="2010">2010</option>
                                        <option value="2011">2011</option>
                                        <option value="2012">2012</option>
                                        <option value="2013">2013</option>
                                        <option value="2014">2014</option>
                                        <option value="2015">2015</option>
                                        <option value="2016">2016</option>
                                        <option value="2017">2017</option>
                                      </select>
                                    </div>
                                  </div>
                                </div>
                                <div class="form-group pull-right ">
                                  <button type="submit" id="submit_loader" value="submit" class="btn btn-lg btn-theme">Search Now</button>
                                </div>
                              </div>
                            </form>
                          </div>
                          <div class="tab-pane fade" id="tab2">
                            <form>
                              <div class="search-form">
                                <div class="search-form-inner-type">
                                  <div class="col-md-2 col-sm-4 col-xs-6">
                                    <div class="box">
                                      <a href="#">
                                        <img alt="Convertible" src="wp-content/img/front-left-side-47 (5).jpg" />
                                        <h4>Convertible</h4>
                                      </a>
                                    </div>
                                  </div>
                                  <div class="col-md-2 col-sm-4 col-xs-6">
                                    <div class="box">
                                      <a href="#">
                                        <img alt="Coupe" src="wp-content/img1/front-left-side-47 (4).webp" />
                                        <h4>Coupe</h4>
                                      </a>
                                    </div>
                                  </div>
                                  <div class="col-md-2 col-sm-4 col-xs-6">
                                    <div class="box">
                                      <a href="#">
                                        <img alt="Sedan" src="wp-content/img1/front-left-side-47 (5).webp" />
                                        <h4>Sedan</h4>
                                      </a>
                                    </div>
                                  </div>
                                  <div class="col-md-2 col-sm-4 col-xs-6">
                                    <div class="box">
                                      <a href="#">
                                        <img alt="Van/Minivan" src="wp-content/img1/front-left-side-47 (6).webp" />
                                        <h4>Van/Minivan</h4>
                                      </a>
                                    </div>
                                  </div>
                                  <div class="col-md-2 col-sm-4 col-xs-6">
                                    <div class="box">
                                      <a href="#">
                                        <img alt="Truck" src="wp-content/img1/front-left-side-47 (7).webp" />
                                        <h4>Truck</h4>
                                      </a>
                                    </div>
                                  </div>
                                  <div class="col-md-2 col-sm-4 col-xs-6">
                                    <div class="box">
                                      <a href="#">
                                        <img alt="Hybrid" src="wp-content/img1/front-left-side-47 (8).webp" />
                                        <h4>Hybrid</h4>
                                      </a>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="custom-padding gray">
                <!-- Main Container -->
                <div class="container">
                  <!-- Row -->
                  <div class="row">
                    <div class="heading-panel">
                      <div class="col-xs-12 col-md-12 col-sm-12 left-side">
                        <!-- Main Title -->
                        <h2>Latest <span class="heading-color"> Featured </span> Ads </h2>
                        <!-- Short Description -->
                      </div>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                      <div class="row">
                        <div class="grid-style-2">
                          <div class="posts-masonry ads-for-home">
                            <div class="col-md-4  col-lg-4 col-sm-6 col-xs-12" id=".holder-1375">
                              <div class="category-grid-box-1">
                                <div class="featured-ribbon">
                                  <span>Featured</span>
                                </div>
                                <div class="image">
                                  <a href="#">
                                    <img src="wp-content/img1/front-left-side-47 (9).webp" alt="2017 Maserati Ghibli SQ4 Blue 1,695 Miles" class="img-responsive" />
                                  </a>
                                  <div class="price-tag">
                                    <div class="price">
                                      <span>Rs18. <span class=""> (24 Lakh)</span>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="short-description-1 clearfix">
                                  <div class="category-title">
                                    <span class="padding_cats">
                                      <a href="#">Audi</a>
                                    </span>
                                    <span class="padding_cats">
                                      <a href="#">A3</a>
                                    </span>
                                  </div>
                                  <h3>
                                    <a href="#">2017 Maserati Ghibli...</a>
                                  </h3>
                                  <p class="location">
                                    <i class="fa fa-map-marker"></i>
                                    <a href="#">California</a>, <a href="#">United States</a>
                                  </p>
                                  <ul class="list-unstyled">
                                    <li>
                                      <i class="flaticon-gas-station-1"></i>Petrol
                                    </li>
                                    <li>
                                      <i class="flaticon-dashboard"></i>1,694 km
                                    </li>
                                    <li>
                                      <i class="flaticon-engine-2"></i>3000 cc
                                    </li>
                                    <li>
                                      <i class="flaticon-car-2"></i>Sedan
                                    </li>
                                    <li>
                                      <i class="flaticon-cogwheel-outline"></i>Blue
                                    </li>
                                  </ul>
                                </div>
                                <div class="ad-info-1">
                                  <p>
                                    <i class="flaticon-calendar"></i> &nbsp; <span>January 24, 2019</span>
                                  </p>
                                  <ul class="pull-right ">
                                    <li>
                                      <a data-toggle="tooltip" data-placement="top" data-original-title="Save Ad" href="javascript:void(0);" class="save-ad" data-adid="1375">
                                        <i class="flaticon-like-1"></i>
                                      </a>
                                      <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                                    </li>
                                    <li>
                                      <a href="#">
                                        <i class="flaticon-message"></i>
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <!-- Listing Ad Grid -->
                            </div>
                            <div class="col-md-4  col-lg-4 col-sm-6 col-xs-12" id=".holder-1366">
                              <div class="category-grid-box-1">
                                <div class="featured-ribbon">
                                  <span>Featured</span>
                                </div>
                                <div class="image">
                                  <a href="#">
                                    <img src="wp-content/img1/front-left-side-47 (10).webp" alt="Maserati Ghibli SQ4 Blue 1,695 Miles 2017" class="img-responsive" />
                                  </a>
                                  <div class="price-tag">
                                    <div class="price">
                                      <span>Rs18.<span class=""> (65 Lakh* )</span>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="short-description-1 clearfix">
                                  <div class="category-title">
                                    <span class="padding_cats">
                                      <a href="#">Audi</a>
                                    </span>
                                    <span class="padding_cats">
                                      <a href="#">A4</a>
                                    </span>
                                  </div>
                                  <h3>
                                    <a href="#">X-Line AT D</a>
                                  </h3>
                                  <p class="location">
                                    <i class="fa fa-map-marker"></i>
                                    <a href="#">California</a>, <a href="#">United States</a>
                                  </p>
                                  <ul class="list-unstyled">
                                    <li>
                                      <i class="flaticon-gas-station-1"></i>Petrol
                                    </li>
                                    <li>
                                      <i class="flaticon-dashboard"></i>1,695 km
                                    </li>
                                    <li>
                                      <i class="flaticon-engine-2"></i>2000 cc
                                    </li>
                                    <li>
                                      <i class="flaticon-car-2"></i>Sedan
                                    </li>
                                    <li>
                                      <i class="flaticon-cogwheel-outline"></i>Black
                                    </li>
                                  </ul>
                                </div>
                                <div class="ad-info-1">
                                  <p>
                                    <i class="flaticon-calendar"></i> &nbsp; <span>January 24, 2019</span>
                                  </p>
                                  <ul class="pull-right ">
                                    <li>
                                      <a data-toggle="tooltip" data-placement="top" data-original-title="Save Ad" href="javascript:void(0);" class="save-ad" data-adid="1366">
                                        <i class="flaticon-like-1"></i>
                                      </a>
                                      <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                                    </li>
                                    <li>
                                      <a href="#">
                                        <i class="flaticon-message"></i>
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <!-- Listing Ad Grid -->
                            </div>
                            <div class="col-md-4  col-lg-4 col-sm-6 col-xs-12" id=".holder-1362">
                              <div class="category-grid-box-1">
                                <div class="featured-ribbon">
                                  <span>Featured</span>
                                </div>
                                <div class="image">
                                  <a href="#">
                                    <img src="wp-content/img1/630x420-(1).webp" alt="2017 Maserati Ghibli SQ4 Blue 1,694 Miles" class="img-responsive" />
                                  </a>
                                  <div class="price-tag">
                                    <div class="price">
                                      <span>Rs15.<span class=""> (59 Lakh*)</span>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="short-description-1 clearfix">
                                  <div class="category-title">
                                    <span class="padding_cats">
                                      <a href="#">Austin</a>
                                    </span>
                                    <span class="padding_cats">
                                      <a href="#">Mini</a>
                                    </span>
                                  </div>
                                  <h3>
                                    <a href="#">2017 Maserati Ghibli...</a>
                                  </h3>
                                  <p class="location">
                                    <i class="fa fa-map-marker"></i>
                                    <a href="#">California</a>, <a href="#">United States</a>
                                  </p>
                                  <ul class="list-unstyled">
                                    <li>
                                      <i class="flaticon-gas-station-1"></i>Petrol
                                    </li>
                                    <li>
                                      <i class="flaticon-dashboard"></i>1,694 km
                                    </li>
                                    <li>
                                      <i class="flaticon-engine-2"></i>3500 cc
                                    </li>
                                    <li>
                                      <i class="flaticon-car-2"></i>Sedan
                                    </li>
                                    <li>
                                      <i class="flaticon-cogwheel-outline"></i>Blue
                                    </li>
                                  </ul>
                                </div>
                                <div class="ad-info-1">
                                  <p>
                                    <i class="flaticon-calendar"></i> &nbsp; <span>January 24, 2019</span>
                                  </p>
                                  <ul class="pull-right ">
                                    <li>
                                      <a data-toggle="tooltip" data-placement="top" data-original-title="Save Ad" href="javascript:void(0);" class="save-ad" data-adid="1362">
                                        <i class="flaticon-like-1"></i>
                                      </a>
                                      <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                                    </li>
                                    <li>
                                      <a href="#">
                                        <i class="flaticon-message"></i>
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <!-- Listing Ad Grid -->
                            </div>
                            <div class="col-md-4  col-lg-4 col-sm-6 col-xs-12" id=".holder-1554">
                              <div class="category-grid-box-1">
                                <div class="featured-ribbon">
                                  <span>Featured</span>
                                </div>
                                <div class="image">
                                  <a href="#">
                                    <img src="wp-content/img1/front-left-side-47 (11).webp" alt="2017 Ford Mustang" class="img-responsive" />
                                  </a>
                                  <div class="price-tag">
                                    <div class="price">
                                      <span>Rs.11.56 <span class=""> (- 18.96 Lakh* )</span>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="short-description-1 clearfix">
                                  <div class="category-title">
                                    <span class="padding_cats">
                                      <a href="#">Ford</a>
                                    </span>
                                    <span class="padding_cats">
                                      <a href="#">Mustang</a>
                                    </span>
                                  </div>
                                  <h3>
                                    <a href="#">2017 Ford Mustang</a>
                                  </h3>
                                  <p class="location">
                                    <i class="fa fa-map-marker"></i>
                                    <a href="#">Fort Lauderdale</a>, <a href="#">Florida</a>, <a href="#">United States</a>
                                  </p>
                                  <ul class="list-unstyled">
                                    <li>
                                      <i class="flaticon-gas-station-1"></i>Petrol
                                    </li>
                                    <li>
                                      <i class="flaticon-dashboard"></i>12,454 km
                                    </li>
                                    <li>
                                      <i class="flaticon-engine-2"></i>2000 cc
                                    </li>
                                    <li>
                                      <i class="flaticon-car-2"></i>Coupe
                                    </li>
                                    <li>
                                      <i class="flaticon-cogwheel-outline"></i>White
                                    </li>
                                  </ul>
                                </div>
                                <div class="ad-info-1">
                                  <p>
                                    <i class="flaticon-calendar"></i> &nbsp; <span>January 24, 2019</span>
                                  </p>
                                  <ul class="pull-right ">
                                    <li>
                                      <a data-toggle="tooltip" data-placement="top" data-original-title="Save Ad" href="javascript:void(0);" class="save-ad" data-adid="1554">
                                        <i class="flaticon-like-1"></i>
                                      </a>
                                      <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                                    </li>
                                    <li>
                                      <a href="#">
                                        <i class="flaticon-message"></i>
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <!-- Listing Ad Grid -->
                            </div>
                            <div class="col-md-4  col-lg-4 col-sm-6 col-xs-12" id=".holder-1552">
                              <div class="category-grid-box-1">
                                <div class="featured-ribbon">
                                  <span>Featured</span>
                                </div>
                                <div class="image">
                                  <a href="#">
                                    <img src="wp-content/img1/front-left-side-47 (12).webp" alt="2010 Chevrolet Camaro" class="img-responsive" />
                                  </a>
                                  <div class="price-tag">
                                    <div class="price">
                                      <span>Rs.7.70  <span class=""> (- 14.18 Lakh*)</span>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="short-description-1 clearfix">
                                  <div class="category-title">
                                    <span class="padding_cats">
                                      <a href="#">Corvette</a>
                                    </span>
                                  </div>
                                  <h3>
                                    <a href="#">2010 Chevrolet Camaro</a>
                                  </h3>
                                  <p class="location">
                                    <i class="fa fa-map-marker"></i>
                                    <a href="#">Fort Lauderdale</a>, <a href="#">Florida</a>, <a href="#">United States</a>
                                  </p>
                                  <ul class="list-unstyled">
                                    <li>
                                      <i class="flaticon-gas-station-1"></i>Petrol
                                    </li>
                                    <li>
                                      <i class="flaticon-dashboard"></i>23 km
                                    </li>
                                    <li>
                                      <i class="flaticon-engine-2"></i>1500 cc
                                    </li>
                                    <li>
                                      <i class="flaticon-car-2"></i>Coupe
                                    </li>
                                    <li>
                                      <i class="flaticon-cogwheel-outline"></i>White
                                    </li>
                                  </ul>
                                </div>
                                <div class="ad-info-1">
                                  <p>
                                    <i class="flaticon-calendar"></i> &nbsp; <span>January 24, 2019</span>
                                  </p>
                                  <ul class="pull-right ">
                                    <li>
                                      <a data-toggle="tooltip" data-placement="top" data-original-title="Save Ad" href="javascript:void(0);" class="save-ad" data-adid="1552">
                                        <i class="flaticon-like-1"></i>
                                      </a>
                                      <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                                    </li>
                                    <li>
                                      <a href="#">
                                        <i class="flaticon-message"></i>
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <!-- Listing Ad Grid -->
                            </div>
                            <div class="col-md-4  col-lg-4 col-sm-6 col-xs-12" id=".holder-89">
                              <div class="category-grid-box-1">
                                <div class="featured-ribbon">
                                  <span>Featured</span>
                                </div>
                                <div class="image">
                                  <a href="#" class="play-video">
                                    <img src="wp-content/img1/6392c4151b703.webp" alt="Icon" />
                                  </a>
                                  <a href="#">
                                    <img src="wp-content/img1/6392c4151b703.webp" alt="Land Rover Freelander 2 Se" class="img-responsive" />
                                  </a>
                                  <div class="price-tag">
                                    <div class="price">
                                      <span>Price On Call</span>
                                    </div>
                                  </div>
                                </div>
                                <div class="short-description-1 clearfix">
                                  <div class="category-title">
                                    <span class="padding_cats">
                                      <a href="#">Land Rover</a>
                                    </span>
                                  </div>
                                  <h3>
                                    <a href="#">Land Rover Freelande...</a>
                                  </h3>
                                  <p class="location">
                                    <i class="fa fa-map-marker"></i>
                                    <a href="#">District of Columbia</a>, <a href="#">United States</a>
                                  </p>
                                  <ul class="list-unstyled">
                                    <li>
                                      <i class="flaticon-gas-station-1"></i>Diesel
                                    </li>
                                    <li>
                                      <i class="flaticon-dashboard"></i>1,300 km
                                    </li>
                                    <li>
                                      <i class="flaticon-engine-2"></i>1500 cc
                                    </li>
                                    <li>
                                      <i class="flaticon-car-2"></i>Luxury
                                    </li>
                                    <li>
                                      <i class="flaticon-cogwheel-outline"></i>Blue
                                    </li>
                                  </ul>
                                </div>
                                <div class="ad-info-1">
                                  <p>
                                    <i class="flaticon-calendar"></i> &nbsp; <span>June 20, 2017</span>
                                  </p>
                                  <ul class="pull-right ">
                                    <li>
                                      <a data-toggle="tooltip" data-placement="top" data-original-title="Save Ad" href="javascript:void(0);" class="save-ad" data-adid="89">
                                        <i class="flaticon-like-1"></i>
                                      </a>
                                      <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                                    </li>
                                    <li>
                                      <a href="#">
                                        <i class="flaticon-message"></i>
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <!-- Listing Ad Grid -->
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="text-center">
                      <div class="load-more-btn"></div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      

      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="custom-padding  gray no-top">
                <div class="container">
                  <div class="heading-panel">
                    <div class="col-xs-12 col-md-12 col-sm-12 text-center">
                      <!-- Main Title -->
                      <h2>Top <span class="heading-color"> Car's </span> Comparison </h2>
                      <!-- Short Description -->
                      <p class="heading-text">When it comes to buying a car, everyone has different priorities. carspot comes with excellent car comparison features that suits your needs.</p>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 col-xs-12 col-sm-12">
                      <div class="row">
                        <!-- Car Comparison Archive -->
                        <ul class="compare-masonry text-center">
                          <li class="col-md-6 col-sm-6 col-xs-12">
                            <div class="comparison-box">
                              <a href="../car-comparison/index1deb.html?id1=647&amp;id2=649">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                  <div class="compare-grid">
                                    <img src="https://static.wixstatic.com/media/5f6da5_207ab0c20dc24223b375f022199e3b41~mv2.gif/v1/fit/w_2500,h_1330,al_c/5f6da5_207ab0c20dc24223b375f022199e3b41~mv2.gif" alt="imag not found" class="img-responsive" />
                                    <h2>2016 Ford Escape Cape</h2>
                                    <div class="rating">
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-o"></i>
                                      <span class="star-score"> ( <strong>4</strong>) </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="vsbox">vs</div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                  <div class="compare-grid">
                                    <img src="https://static.wixstatic.com/media/5f6da5_207ab0c20dc24223b375f022199e3b41~mv2.gif/v1/fit/w_2500,h_1330,al_c/5f6da5_207ab0c20dc24223b375f022199e3b41~mv2.gif" alt="imag not found" class="img-responsive" />
                                    <h2>2017 Chevrolet Camaro</h2>
                                    <div class="rating">
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-o"></i>
                                      <i class="fa fa-star-o"></i>
                                      <span class="star-score"> ( <strong>3</strong>) </span>
                                    </div>
                                  </div>
                                </div>
                              </a>
                            </div>
                          </li>
                          <li></li>
                          <li class="col-md-6 col-sm-6 col-xs-12">
                            <div class="comparison-box">
                              <a href="#">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                  <div class="compare-grid">
                                    <img src="wp-content/img/20190718124653_MG-Hector-black-front-removebg-preview.png" alt="imag not found" class="img-responsive" />
                                    <h2>Mercedes-Benz C-Class</h2>
                                    <div class="rating">
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <span class="star-score"> ( <strong>5</strong>) </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="vsbox">vs</div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                  <div class="compare-grid">
                                    <img src="wp-content/img/20190718124653_MG-Hector-black-front-removebg-preview.png" alt="imag not found" class="img-responsive" />
                                    <h2>2017 Honda CR-V</h2>
                                    <div class="rating">
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-o"></i>
                                      <span class="star-score"> ( <strong>4</strong>) </span>
                                    </div>
                                  </div>
                                </div>
                              </a>
                            </div>
                          </li>
                          <li></li>
                          <li class="col-md-6 col-sm-6 col-xs-12">
                            <div class="comparison-box">
                              <a href="../car-comparison/index7d76.html?id1=663&amp;id2=666">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                  <div class="compare-grid">
                                    <img src="wp-content/img/15b1e266b974ebccf949c6f01155b8ff.png" alt="imag not found" class="img-responsive" />
                                    <h2>renault suv 2016</h2>
                                    <div class="rating">
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-o"></i>
                                      <i class="fa fa-star-o"></i>
                                      <span class="star-score"> ( <strong>3</strong>) </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="vsbox">vs</div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                  <div class="compare-grid">
                                    <img src="wp-content/img/15b1e266b974ebccf949c6f01155b8ff.png" alt="imag not found" class="img-responsive" />
                                    <h2>2017 Toyota RAV4</h2>
                                    <div class="rating">
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-o"></i>
                                      <span class="star-score"> ( <strong>4</strong>) </span>
                                    </div>
                                  </div>
                                </div>
                              </a>
                            </div>
                          </li>
                          <li></li>
                          <li class="col-md-6 col-sm-6 col-xs-12">
                            <div class="comparison-box">
                              <a href="#">
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                  <div class="compare-grid">
                                    <img src="https://static.wixstatic.com/media/8f2779_717f1c11d63342ef817f4639c3ff291e~mv2.gif" alt="imag not found" class="img-responsive" />
                                    <h2>Mercedes-Benz C-Class</h2>
                                    <div class="rating">
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <span class="star-score"> ( <strong>5</strong>) </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="vsbox">vs</div>
                                <div class="col-md-6 col-sm-12 col-xs-12">
                                  <div class="compare-grid">
                                    <img src="https://static.wixstatic.com/media/8f2779_717f1c11d63342ef817f4639c3ff291e~mv2.gif" alt="imag not found" class="img-responsive" />
                                    <h2>Audi A5 Sport</h2>
                                    <div class="rating">
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <i class="fa fa-star-ofa fa-star"></i>
                                      <span class="star-score"> ( <strong>5</strong>) </span>
                                    </div>
                                  </div>
                                </div>
                              </a>
                            </div>
                          </li>
                          <li></li>
                        </ul>
                        <div class="clearfix"></div>
                        <div class="text-center">
                          <div class="load-more-btn">
                            <a href="#" target="%20_blank" class="btn btn-lg  btn-theme"> Compare More Cars </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
     
      
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="car-inspection section-padding    ">
                <div class="container">
                  <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 nopadding hidden-sm">
                      <img src="wp-content/img/Weights-of-your-car.webp" class="wow slideInLeft img-responsive" data-wow-delay="0ms" data-wow-duration="3000ms" alt="Image Not Found" />
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12 nopadding">
                      <div class="call-to-action-detail-section">
                        <div class="heading-2">
                          <h3> Want To Sale Your Car ?</h3>
                          <h2>Car Inspection</h2>
                        </div>
                        <p>Our CarSure experts inspect the car on over 200 checkpoints so you get complete satisfaction and peace of mind before buying. </p>
                        <div class="row">
                          <ul>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Transmission
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Steering
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Engine
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Tires
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Lighting
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Interior
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Suspension
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Exterior
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Brakes
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Air Conditioning
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Engine Diagnostics
                            </li>
                            <li class="col-sm-4">
                              <i class="fa fa-check"></i> Wheel Alignment
                            </li>
                          </ul>
                        </div>
                        <a href="# " target="%20_blank" class="btn-theme btn-lg btn"> Schedule Inspection <i class="fa fa-angle-right"></i>
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
     
                          
      
    <div class="clear"></div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </section>
	<?php include 'include/footer.php';?>