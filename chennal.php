<?php include "include/header.php" ?>
<section class="transparent-breadcrumb-listing" style="background: url(wp-content/img/639372f2aed5f.webp); background-repeat: no-repeat ; background-size: cover ;  background-position: center center ;  background-attachment: scroll; ">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="list-heading">

        </div>
      </div>
    </div>
  </div>
</section><div class="main-content-area clearfix">
  <section class="section-padding   gray ">
    <!-- Main Container -->
    <div class="container">
      <!-- Row -->
      <div class="row">
        <!-- Left Sidebar -->
        <!-- Middle Content Area -->
        <div class="col-md-8 col-xs-12 col-sm-12">
          <div class="single-blog blog-detial">
            <!-- Blog Archive -->
            <div class="blog-post">
              <div class="post-img">
                <a href="https://web.rtpcompany.com/markets/automotive/animated-EV-autonomous-car.gif" data-fancybox>
                  <img class="img-responsive" src="wp-content/img2/2022-2023-mitsubishi-eclipse-cross-sel-red-diamond-side-profile.webp" alt="Land Rover Freelander 2 Se First Impression">
                </a>
              </div>
              <div class="post-info ">
                <a href="javascript:void(0);"></a>
                <a href="javascript:void(0);"></a>
              </div>
              <h3 class="post-title"> Buy the new MG HS today</h3>
              <div class="post-excerpt post-desc">
                <p>The MG HS has the bold lines and proud features of a thoroughbred SUV designed for family living. Sporty looks, purposeful, and with a spacious interior filled with comfort and intelligent features, it’s easy to just get in and get away for the perfect escape. With three trim levels to choose from; Explore, Excite and Exclusive, there’s an MG HS to suit every family lifestyle and every family budget - Add 7 years warranty for complete peace of mind, class-leading space in the front, reclining rear seats and a multitude of innovative technology including; keyless entry, colour touchscreen, Apple CarPlay™, Android Auto™ plus much more and you have a real winner.</p>
                <blockquote>
                  <p>
                    <strong>MG HS offers buyers an exciting and high-quality alternative to the conventional choices with a sporty exterior and a family friendly interior with soft-touch materials used throughout. </strong>
                  </p>
                </blockquote>
                <p>With purposeful, sporty looks and a spacious interior filled with comfort and intelligent features, it’s easy to just get in and get away for the perfect escape. Relax in the generous seats as our smart tech smooths out the ride. And, with two levels of trim to choose from; Excite and Exclusive, there’s an MG HS to suit every lifestyle..</p>
                <p>
               
               
                  <img decoding="async" class="alignleft wp-image-304 size-full" src="wp-content/img2/global-nav-shopping-tools.webp" alt="" width="250" height="250" srcset="wp-content/img2/global-nav-find-a-dealer.webp"
                p>MG HS has the bold lines and statement features of a thoroughbred SUV. At the front our stellar field grille perfectly frames our largest MG badge yet. Travel down the sleek lines, past the gleaming alloys, and to the rear you’ll find the twin tailpipes and aluminium bumper exuding dynamic confidence.
                <p>
                  <img decoding="async" loading="lazy" class="size-full wp-image-305 alignright" src="wp-content/img2/global-nav-shopping-tools.webp" sizes="(max-width: 250px) 100vw, 250px" />
                </p>
                <p>Time together. It’s so rare these days, but it’s never been more important. That’s why MG HS is designed to bring your family together and leave the world behind. There’s space and comfort, so you can all feel more relaxed from the moment you set off. Not to mention everything from Adaptive Cruise Control to Traffic Jam Assist from our MG Pilot suite of driver assistance systems, to make every journey stress-free.
            </p>
               
                <!-- You can start editing here. -->
                <!-- If comments are closed. -->
                <p class="nocomments">Comments are closed.</p>
              </div>
            </div>
            <!-- Blog Grid -->
          </div>
        </div>
        <!-- Right Sidebar -->
        <div class="col-md-4 col-xs-12 col-sm-12">
          <div class="blog-sidebar">
            <div class="widget widget-content">
              <div id="categories-3">
                <div class="widget-heading">
                  <h4 class="panel-title">
                    <a href="javascript:void(0)">Categories</a>
                  </h4>
                </div>
                <ul>
                  <li class="cat-item cat-item-15">
                    <a href="#">Analysis &amp; Features</a>
                  </li>
                  <li class="cat-item cat-item-16">
                    <a href="#">Modified Cars</a>
                  </li>
                  <li class="cat-item cat-item-17">
                    <a href="#">Sport Cars</a>
                  </li>
                  <li class="cat-item cat-item-18">
                    <a href="#">Test Drive</a>
                  </li>
                  <li class="cat-item cat-item-19">
                    <a href="#">Upcomming Cars</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="widget widget-content">
              <div id="archives-3">
                <div class="widget-heading">
                  <h4 class="panel-title">
                    <a href="javascript:void(0)">Archive Posts</a>
                  </h4>
                </div>
                <ul>
                  <li>
                    <a href='#'>June 2017</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="widget widget-content">
              <div id="tag_cloud-2">
                <div class="widget-heading">
                  <h4 class="panel-title">
                    <a href="javascript:void(0)">Tags</a>
                  </h4>
                </div>
                <div class="tagcloud">
                  <a href="#" class="tag-cloud-link tag-link-20 tag-link-position-1" style="font-size: 22pt;" aria-label="audi (5 items)">audi</a>
                  <a href="#" class="tag-cloud-link tag-link-21 tag-link-position-2" style="font-size: 8pt;" aria-label="bmw (1 item)">bmw</a>
                  <a href="#" class="tag-cloud-link tag-link-22 tag-link-position-3" style="font-size: 22pt;" aria-label="Bugatti (5 items)">Bugatti</a>
                  <a href="#" class="tag-cloud-link tag-link-23 tag-link-position-4" style="font-size: 8pt;" aria-label="Convertible (1 item)">Convertible</a>
                  <a href="#" class="tag-cloud-link tag-link-24 tag-link-position-5" style="font-size: 8pt;" aria-label="Ferrari (1 item)">Ferrari</a>
                  <a href="#" class="tag-cloud-link tag-link-25 tag-link-position-6" style="font-size: 8pt;" aria-label="ford (1 item)">ford</a>
                  <a href="#" class="tag-cloud-link tag-link-26 tag-link-position-7" style="font-size: 8pt;" aria-label="Italia (1 item)">Italia</a>
                  <a href="#" class="tag-cloud-link tag-link-27 tag-link-position-8" style="font-size: 8pt;" aria-label="Land Rover (1 item)">Land Rover</a>
                </div>
              </div>
            </div>
            <div class="widget widget-content">
              <div id="recent-posts-3">
                <div class="widget-heading">
                  <h4 class="panel-title">
                    <a href="javascript:void(0)">Recent Posts</a>
                  </h4>
                </div>
                <ul>
                  <li>
                    <a href="#">Ford Focus Production for North America Will Cease for a Year</a>
                  </li>
                  <li>
                    <a href="#">We Hear: Audi RS Models Could Be Offered in Rear-Wheel Drive</a>
                  </li>
                  <li>
                    <a href="#">BMW Black Fire Edition X5, X6 M Coming in August</a>
                  </li>
                  <li>
                    <a href="#">2015 Ferrari 458 Italia Convertible a true sports car</a>
                  </li>
                  <li>
                    <a href="#" aria-current="page">Land Rover Freelander 2 Se First Impression</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="widget widget-content">
              <div id="search-2">
                <form role="search" method="get" class="search-form" action="#">
                  <div class="search-blog">
                    <div class="input-group stylish-input-group">
                      <input type="search" class="form-control" id="serch" placeholder="Search &hellip;" value="" name="s" />
                      <span class="input-group-addon">
                        <button type="submit">
                          <span class="fa fa-search"></span>
                        </button>
                      </span>
                    </div>
                  </div>
                  <input type="submit" class="search-submit" value="Search" />
                </form>
              </div>
            </div>
            
    </section>

    <div class="clear"></div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </section>

	<?php include 'include/footer.php';?> 