



<?php include "include/header.php" ?> 
    <section class="transparent-breadcrumb-listing" style="background: url(wp-content/img2/2023-outlander-phev-sel-white-diamond.webp); background-repeat: no-repeat ; background-size: cover ;  background-position: center center ;  background-attachment: scroll; ">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="list-heading"></div>
          </div>
        </div>
      </div>
    </section>
    <div class="main-content-area clearfix">
      <!-- =-=-=-=-=-=-= Latest Ads =-=-=-=-=-=-= -->
      <section class="section-padding pattern_dots">
        <!-- Main Container -->
        <div class="container">
          <!-- Row -->
          <div class="row">
            <!-- Middle Content Area -->
            <div class="col-md-12 col-lg-12 col-sx-12">
              <!-- Row -->
              <div class="row">
                <div class="clearfix"></div>
                <!-- Ads Archive -->
                <div class="dealers-single-page">
                  <div class="col-md-8 col-xs-12 col-sm-12 col-lg-8">
                    <div class="profile-title-box">
                      <div class="profile-heading">
                        <h2> ScriptsBundle Wheels <span class="label label-success"> Verified</span>
                        </h2>
                      </div>
                      <div class="profile-meta">
                        <ul>
                          <li>
                            <i class="la la-certificate icon"></i>
                            <span class="profile-meta-title"></span>
                            <ul class="review-stars">
                              <li class="star colored-star">
                                <i class="fa fa-star"></i>
                              </li>
                              <li class="star colored-star">
                                <i class="fa fa-star"></i>
                              </li>
                              <li class="star colored-star">
                                <i class="fa fa-star"></i>
                              </li>
                              <li class="star">
                                <i class="fa fa-star"></i>
                              </li>
                              <li class="star">
                                <i class="fa fa-star"></i>
                              </li>
                            </ul> (3)
                          </li>
                          <li>
                            <i class="la la-calendar-o icon"></i>
                            <span class="profile-meta-title">
                          </li>
                          <li>
                            <i class="la la-clock-o icon"></i>
                            <span class="profile-meta-title">
                          </li>
                        </ul>
                      </div>
                    </div>
                    <div class="store-front">
                      <img src="wp-content/img2/2023-outlander-phev-sel-white-diamond.webp" class="img-responsive" alt="Dealer store picture">
                    </div>
                    <h3 class="profile-heading"> About: </h3>
                    <div class="profile-desc">
                      <p>This is the demo user account added from scriptsbundle team. Here will be dealer can add about his shop.</p>
                    </div>
                    <h3 class="profile-heading"> Inventory: </h3>
                    <ul class="list-unstyled" id="inventory">
                      <li>
                        <div class="ad-listing on-profile clearfix ">
                          <div class="col-md-3 col-sm-5 col-xs-12 grid-style no-padding">
                            <div class="img-box">
                              <a href="wp-content/img2/2023-outlander-phev-sel-white-diamond.webp">
                                <img src="wp-content/img2/2023-outlander-phev-sel-white-diamond.webp" alt="2017 Maserati Ghibli SQ4 Blue 1,695 Miles" class="img-responsive">
                              </a>
                              <div class="total-images">
                                <strong>1</strong> photos
                              </div>
                            </div>
                            <span class="ad-status"> Featured </span>
                          </div>
                          <div class="col-md-9 col-sm-7 col-xs-12">
                            <!-- Ad Content-->
                            <div class="row">
                              <div class="content-area">
                                <div class="col-md-12 col-sm-12 col-xs-12 col-lg-12">
                                  <div class="badge">Used</div>
                                  <h3>
                                    <a href="#">2017 Maserati Ghibli SQ4 Blue 1,695 Miles</a>
                                  </h3>
                                  <ul class="ad-meta-info">
                                    <li>
                                      <i class="fa fa-map-marker"></i>
                                      <a href="#">California</a>, <a href="#">United States</a>
                                    </li>
                                    <li>
                                      <i class="fa fa-clock-o"></i> January 24, 2019
                                    </li>
                                  </ul>
                                  <div class="ad-details">
                                    <ul class="list-unstyled">
                                      <li>Petrol</li>
                                      <li>1,694 km</li>
                                      <li>3,000 cc</li>
                                      <li>Sedan</li>
                                      <li>Blue</li>
                                    </ul>
                                  </div>
                                  <div class="price">
                                    <span> $ 64,500 <span class=""> (Negotiable)</span>
                                    </span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </li>
                    </ul>
                    <div class="extra-button">
                      <a href="login.php" class="btn btn-theme pull-right"> More ads (8) </a>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12 no-padding">
                      <div class="dealers-review-section">
                        <h3 class="profile-heading"> Reviews: </h3>
                        <div class="profile-review-section">
                          <div class="review-box">
                            <div class="main-review">
                              <div class="review-avg-rating">
                                <span> 3.3</span>
                                <ul class="">
                                  <li class="star colored-star">
                                    <i class="fa fa-star"></i>
                                  </li>
                                  <li class="star colored-star">
                                    <i class="fa fa-star"></i>
                                  </li>
                                  <li class="star colored-star">
                                    <i class="fa fa-star"></i>
                                  </li>
                                  <li class="star">
                                    <i class="fa fa-star"></i>
                                  </li>
                                  <li class="star">
                                    <i class="fa fa-star"></i>
                                  </li>
                                </ul>
                              </div>
                              <div class="review-content-box">
                                <div class="review-content-meta">
                                  <div class="review-author-name">
                                    <h4>demo</h4>
                                  </div>
                                  <div class="review-text-box">
                                    <p>demo</p>
                                  </div>
                                  <div class="review-date">
                                    <span class="user-profile">
                                      <a href="index.php"></a> has <span class="recommend"> Recommended </span> this vendor on </span>
                                    <span>January 15, 2021</span>
                                  </div>
                                </div>
                                <div class="rating-stars-box">
                                  <div class="rating-stars">
                                    <label> Level of Services </label>
                                    <ul>
                                      <li class="star colored-star">
                                        <i class="fa fa-star"></i>
                                      </li>
                                      <li class="star colored-star">
                                        <i class="fa fa-star"></i>
                                      </li>
                                      <li class="star colored-star">
                                        <i class="fa fa-star"></i>
                                      </li>
                                      <li class="star">
                                        <i class="fa fa-star"></i>
                                      </li>
                                      <li class="star">
                                        <i class="fa fa-star"></i>
                                      </li>
                                    </ul>
                                  </div>
                                  <div class="rating-stars">
                                    <label> Buying Process </label>
                                    <ul>
                                      <li class="star colored-star">
                                        <i class="fa fa-star"></i>
                                      </li>
                                      <li class="star colored-star">
                                        <i class="fa fa-star"></i>
                                      </li>
                                      <li class="star">
                                        <i class="fa fa-star"></i>
                                      </li>
                                      <li class="star">
                                        <i class="fa fa-star"></i>
                                      </li>
                                      <li class="star">
                                        <i class="fa fa-star"></i>
                                      </li>
                                    </ul>
                                  </div>
                                  <div class="rating-stars">
                                    <label> Vehicle Selection </label>
                                    <ul>
                                      <li class="star colored-star">
                                        <i class="fa fa-star"></i>
                                      </li>
                                      <li class="star colored-star">
                                        <i class="fa fa-star"></i>
                                      </li>
                                      <li class="star colored-star">
                                        <i class="fa fa-star"></i>
                                      </li>
                                      <li class="star colored-star">
                                        <i class="fa fa-star"></i>
                                      </li>
                                      <li class="star colored-star">
                                        <i class="fa fa-star"></i>
                                      </li>
                                    </ul>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="extra-button">
                          <a href="Register.php" class="btn btn-theme btn-block"> Read all reviews </a>
                        </div>
                      </div>
                      <div class="alert custom-alert custom-alert-warning" role="alert">
                        <div class="custom-alert_top-side">
                          <span class="alert-icon custom-alert_icon la la-info-circle"></span>
                          <div class="custom-alert_body">
                            <h6 class="custom-alert_heading"> Login To Write A Review. </h6>
                            <div class="custom-alert_content"> Sorry, only login users are eligibale to post a review.. </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div class="col-md-4 col-lg-4 col-sm-12 col-xs-12">
                    <aside class="sidebar transparen-listing-sidebar">
                      <div class="contact-box">
                        <div class="contact-img">
                          <img src="wp-content/img2/9583230006295b3c782008508376620_0_0.jpg" class="img-responsive" alt="Profile Picture">
                        </div>
                      </div>
                      <div class="profile-widget">
                        <div class="panel with-nav-tabs panel-default">
                          <div class="panel-heading">
                            <ul class="nav nav-tabs">
                              <li class="active">
                                <a href="#tab1default" data-toggle="tab">Detail</a>
                              </li>
                              <li>
                                <a href="#tab2default" data-toggle="tab">Contact</a>
                              </li>
                            </ul>
                          </div>
                          <div class="panel-body">
                            <div class="tab-content">
                              <div class="tab-pane fade in active" id="tab1default">
                                <ul class="widget-listing-details">
                                  <li>
                                    <span>
                                      <i class="la la-map-marker"></i>
                                    </span>
                                    <span>
                                      <h4>Address</h4>   H 146/147 Sector 63 , Noida-201301
                                    </span>
                                  </li>
                                  <li>
                                    <span>
                                      <i class="la la-mobile"></i>
                                    </span>
                                    <span>
                                      <h4>Contact Number</h4>
                                      <a class="phonenumber" href="javascript:void(0)">+91 8923050580
+91 9458124722
 </a>
                                    </span>
                                  </li>
                                  <li>
                                    <span>
                                      <i class="la la-globe"></i>
                                    </span>
                                    <span>
                                      <h4>Website</h4>
                                      <a target="_blank" href="index.php">http://greenusys.com/</a>
                                    </span>
                                  </li>
                                </ul>
                                <ul class="social-media">
                                  <li>
                                    <a target="_blank" href="http://facebook.com/">
                                      <i class="la la-facebook"></i>
                                    </a>
                                  </li>
                                  <li>
                                    <a target="_blank" href="#">
                                      <i class="la la-twitter"></i>
                                    </a>
                                  </li>
                                  <li>
                                    <a target="_blank" href="#">
                                      <i class="la la-linkedin"></i>
                                    </a>
                                  </li>
                                  <li>
                                    <a target="_blank" href="http://youtube.com/">
                                      <i class="la  la-youtube-play"></i>
                                    </a>
                                  </li>
                                </ul>
                              </div>
                              <div class="tab-pane fade" id="tab2default">
                                <form data-parsley-validate="" method="post" id="dealer-contact-form">
                                  <div class="form-group">
                                    <input placeholder="Full name" name="name" required="" class="form-control" type="text" data-parsley-error-message="Please write your name">
                                  </div>
                                  <div class="form-group">
                                    <input placeholder="Email address" name="email" required="" class="form-control" type="email" data-parsley-error-message="Correct email please">
                                  </div>
                                  <div class="form-group">
                                    <input placeholder="Phone number" name="phone" data-parsley-type="number" required="" class="form-control" type="text" data-parsley-error-message="Provide your contact number">
                                  </div>
                                  <div class="form-group">
                                    <textarea cols="6" placeholder="Your message" required name="message" rows="6" class="form-control" data-parsley-error-message="Please write your message"></textarea>
                                  </div>
                                  <div class="form-group col-md-12  col-sm-12">
                                    <div class="g-recaptcha" name="dealer_review_captcha" data-sitekey="6Ldt5pIUAAAAABDsF2h0Wv3jLS3m94xtBFOOqhZj" required></div>
                                  </div>
                                  <input type="hidden" name="ad_dealer_id" value="1" />
                                  <input type="hidden" id="user_contact_nonce" value="a43cd7d65e" />
                                  <input class="btn btn-theme" value="Send Message" type="submit">
                                </form>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div>
                        <div class="singlemap-location">
                          <div id="itemMap"></div>
                          <input type="hidden" id="lat" value="40.7127837" />
                          <input type="hidden" id="lon" value="-74.00594130000002" />
                        </div>
                      </div>
                    </aside>
                    <div class="margin-top-30 margin-bottom-30">
                      <img class="center-block img-responsive" alt="" src="wp-content/img/hector-exterior-right-front-three-quarter-166303-removebg-preview.png">
                    </div>
                  </div>
                </div>
                <div class="clearfix"></div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </div>
    <div class="clearfix"></div>
                    <div class="text-center">
                      <div class="load-more-btn"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div> <?php include 'include/footer.php';?>