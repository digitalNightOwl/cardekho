<?php include "include/header.php" ?> 
<section class="transparent-breadcrumb-listing" style="background: url(wp-content/img/hero-cars-2-1.png); background-repeat: no-repeat ; background-size: cover ;  background-position: center center ;  background-attachment: scroll; ">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="list-heading">
              <h2>Car News</h2>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div class="main-content-area clearfix">
      <section class="section-padding  gray review-details ">
        <!-- Main Container -->
        <div class="container">
          <!-- Row -->
          <div class="row">
            <!-- Middle Content Area -->
            <div class="col-md-8 col-xs-12 col-sm-12">
              <div class="blog-detial">
                <!-- Blog Archive -->
                <div class="blog-post">
                  <div class="post-img">
                    <span class="badge text-uppercase badge-overlay badge-tech"></span>
                    <a href="wp-content/img1/mg-rx5-3311c863.jpg" data-fancybox="group">
                      <img class="img-responsive large-img" alt="" src="wp-content/img1/6355625e15e12.webp">
                    </a>
                  </div>
                  <div class="review-excerpt">
                    <h3>Overview Of Car</h3>
                    <p>The best way to live out your super-spy fantasy is behind the wheel of an Aston Martin, and the Vantage is the least expensive way to do it. Offered as a coupe or convertible, performance from the V8 Vantage’s engine is nothing short of brutish, at 420 or 430 hp; six-speed manual and seven-speed automatic transmissions are offered. </p>
                    <p>For speed demons, the V12 Vantage has a turbine-smooth 565-hp 5.9-liter V-12 with either a seven-speed manual or a seven-speed automatic handling shifting duties. </p>
                    <table class="table">
                      <tbody></tbody>
                    </table>
                    <div class="row pro-cons">
                      <div class="col-md-6 col-sm-12 col-xs-12 ">
                        <div class="pro-section">
                          <img src="wp-content/img1/111.jpg" alt="Like" />
                          <h3>What We Like</h3>
                          <ul class="standard-list">
                            <li>Acceleration</li>
                            <li>Excellent ride quality</li>
                            <li>Rich interior materials</li>
                            <li>Audi refinement</li>
                            <li>Leather upholstery standard</li>
                          </ul>
                        </div>
                      </div>
                      <div class="col-md-6 col-sm-12 col-xs-12 ">
                        <div class="cons-section">
                          <img src="wp-content/img1/112.jpg" alt="Dislike" />
                          <h3>What We Don't</h3>
                          <ul>
                            <li>Acceleration</li>
                            <li>Excellent ride quality</li>
                            <li>Rich interior materials</li>
                            <li>Audi refinement</li>
                            <li>Leather upholstery standard</li>
                          </ul>
                        </div>
                      </div>
                    </div>
                    <div class="entry-content post-excerpt post-desc">
                      <p>You only need to drive the V8 Vantage Vantage S Coupe 2dr to realise Aston Martin has thrown everything it knows at its latest four-seat convertible. While the intense levels of body shake that blighted the previous V8 Vantage Vantage S Coupe 2dr have been reduced exponentially</p>
                      <div>
                        <h3>Dynamic Mode: Quiet, Sport, Sport+</h3>
                      </div>
                      <p>You only need to drive the V8 Vantage Vantage S Coupe 2dr to realise Aston Martin has thrown everything it knows at its latest four-seat convertible. While the intense levels of body shake that blighted the previous V8 Vantage Vantage S Coupe 2dr have been reduced exponentially.</p>
                      <p>The best way to live out your super-spy fantasy is behind the wheel of an Aston Martin, and the Vantage is the least expensive way to do it. Offered as a coupe or convertible</p>
                    </div>
                    <h3>Related Gallery</h3>
                    <ul class="gallery list-inline clearfix">
                      <li>
                        <a data-fancybox="gallery" href="">
                          <img src="wp-content/img1/112.jpg" alt="Related Gallery" />
                        </a>
                      </li>
                      <li>
                        <a data-fancybox="gallery" href="">
                          <img src="wp-content/img1/113.jpg" alt="Related Gallery" />
                        </a>
                      </li>
                      <li>
                        <a data-fancybox="gallery" href="">
                          <img src="wp-content/img1/113.jpg" alt="Related Gallery" />
                        </a>
                      </li>
                    </ul>
                    <div class="clearfix"></div>
                    <br>
                    <div class="post-review">
                      <h3>The CarSpot expert verdict:</h3>
                      <div class="progress-bar-review">
                        <div class="row">
                          <div class="col-sm-12 col-md-3">
                           
                          </div>
                        </div>
                      </div>
                      <div class="progress-bar-review">
                        <div class="row">
                          <div class="col-sm-12 col-md-3">
                            
                          </div>
                        </div>
                      </div>
                      <div class="progress-bar-review">
                        <div class="row">
                          <div class="col-sm-12 col-md-3">
                           
                          </div>
                        </div>
                      </div>
                      <div class="progress-bar-review">
                        <div class="row">
                          <div class="col-sm-12 col-md-3">
                          
                          </div>
                        </div>
                      </div>
                      <div class="progress-bar-review">
                        <div class="row">
                          <div class="col-sm-12 col-md-3">
                          
                          </div>
                        </div>
                      </div>
                      <div class="progress-bar-review">
                        <div class="row">
                          <div class="col-sm-12 col-md-3">
                            <span class="progress-title">Hyundai Ioniq 5 vs Rivals: </span>
                          </div>
                          <div class="col-sm-12 col-md-8">
                            <div class="progress">
                              <div class="progress-bar">
                                <span data-percent="80"></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-12 col-md-1">
                            <span class="progress-title">80%</span>
                          </div>
                        </div>
                      </div>
                      <div class="progress-bar-review">
                        <div class="row">
                          <div class="col-sm-12 col-md-3">
                            <span class="progress-title">Saftey</span>
                          </div>
                          <div class="col-sm-12 col-md-8">
                            <div class="progress">
                              <div class="progress-bar">
                                <span data-percent="95"></span>
                              </div>
                            </div>
                          </div>
                          <div class="col-sm-12 col-md-1">
                            <span class="progress-title">95%</span>
                          </div>
                        </div>
                      </div>
                      <div class="summary-review">
                        <div class='text-summary'>
                          <h5>Summary</h5>
                          <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla auctor, mi sed rhoncus vehicula, ex nulla pellentesque dolor, ut tristique nisl sem a metus. Vestibulum sit amet nunc et lectus ullamcorper volutpat non in eros. Aliquam erat volutpat. Pellentesque hendrerit aliquet pharetra.</p>
                        </div>
                      </div>
                    </div>
                    <div class="clearfix"></div>
                  </div>
                </div>
                <!-- Blog Grid -->
              </div>
            </div>
            <!-- Right Sidebar -->
            <!-- Right Sidebar -->
            <div class="col-md-4 col-xs-12 col-sm-12">
              <div class="blog-sidebar">
                <!-- Heading -->
                <div class="widget">
                  <div class="latest-news">
                    <div class="widget-heading">
                      <h4 class="panel-title">
                        <a> Latest Reviews </a>
                      </h4>
                    </div>
                    <div class="recent-ads">
                      <div class="recent-ads-list">
                        <div class="recent-ads-container">
                          <div class="recent-ads-list-image">
                            <a href="#" class="recent-ads-list-image-inner">
                              <img src="wp-content/img1/114.webp" alt="2017 Mercedes-Benz E-Class Coupe review">
                            </a>
                            <!-- /.recent-ads-list-image-inner -->
                          </div>
                          <!-- /.recent-ads-list-image -->
                          <div class="recent-ads-list-content">
                            <h3 class="recent-ads-list-title">
                              <a href="#">2017 Mercedes-Benz E-Class Coupe review</a>
                            </h3>
                            <ul class="recent-ads-list-location">
                              <li>
                                <a href="javascript:void(0)">June 23, 2017</a>
                              </li>
                            </ul>
                            <!-- /.recent-ads-list-price -->
                          </div>
                          <!-- /.recent-ads-list-content -->
                        </div>
                        <!-- /.recent-ads-container -->
                      </div>
                      <div class="recent-ads-list">
                        <div class="recent-ads-container">
                          <div class="recent-ads-list-image">
                            <a href="#" class="recent-ads-list-image-inner">
                              <img src="wp-content/img1/115.webp" alt="2017 BMW M760 Sports &#038; Luxury Car">
                            </a>
                            <!-- /.recent-ads-list-image-inner -->
                          </div>
                          <!-- /.recent-ads-list-image -->
                          <div class="recent-ads-list-content">
                            <h3 class="recent-ads-list-title">
                              <a href="#">2017 BMW M760 Sports &#038; Luxury Car</a>
                            </h3>
                            <ul class="recent-ads-list-location">
                              <li>
                                <a href="javascript:void(0)">June 23, 2017</a>
                              </li>
                            </ul>
                            <!-- /.recent-ads-list-price -->
                          </div>
                          <!-- /.recent-ads-list-content -->
                        </div>
                        <!-- /.recent-ads-container -->
                      </div>
                      <div class="recent-ads-list">
                        <div class="recent-ads-container">
                          <div class="recent-ads-list-image">
                            <a href="index.php" class="recent-ads-list-image-inner">
                              <img src="wp-content/img1/116.jpg" alt="2018 Mercedes-Benz AMG GT Roadster First Drive">
                            </a>
                            <!-- /.recent-ads-list-image-inner -->
                          </div>
                          <!-- /.recent-ads-list-image -->
                          <div class="recent-ads-list-content">
                            <h3 class="recent-ads-list-title">
                              <a href="index.php">2018 Mercedes-Benz AMG GT Roadster First Drive</a>
                            </h3>
                            <ul class="recent-ads-list-location">
                              <li>
                                <a href="javascript:void(0)">June 23, 2017</a>
                              </li>
                            </ul>
                            <!-- /.recent-ads-list-price -->
                          </div>
                          <!-- /.recent-ads-list-content -->
                        </div>
                        <!-- /.recent-ads-container -->
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Heading -->
                <div class="widget">
                  <div class="widget-heading">
                    <h4 class="panel-title">
                      <a> Reviews By Categories </a>
                    </h4>
                  </div>
                  <div class="categories">
                    <ul>
                      <li>
                        <a href="#"> Sport Cars <span>&nbsp;(2)</span>
                        </a>
                      </li>
                      <li>
                        <a href="#"> Road Test <span>&nbsp;(2)</span>
                        </a>
                      </li>
                      <li>
                        <a href="#"> Performance Test <span>&nbsp;(1)</span>
                        </a>
                      </li>
                      <li>
                        <a href="#"> First Impressions <span>&nbsp;(1)</span>
                        </a>
                      </li>
                      <li>
                        <a href="#"> First Drive <span>&nbsp;(2)</span>
                        </a>
                      </li>
                    </ul>
                  </div>
                </div>
                <!-- Heading -->
                <div class="widget">
                  <div class="reviews_bybrands">
                    <div class="widget-heading">
                      <h4 class="panel-title">
                        <a> Reviews By Top 20 Brands </a>
                      </h4>
                    </div>
                    <table class='table'>
                      <tbody>
                        <tr>
                          <td>
                            <a href="upcoming.php">Ahmedebad  <span>&nbsp;(1)</span>
                            </a>
                          </td>
                          <td>
                            <a href="ahmedabad.php">Bangalore <span>&nbsp;(1)</span>
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <a href="chennal.php">Chennai <span>&nbsp;(1)</span>
                            </a>
                          </td>
                          <td>
                            <a href="Sell Used car.php">carDekho Used car stores <span>&nbsp;(0)</span>
                            </a>
                          </td>
                        </tr>
                        <tr>
                          <td>
                            <a href="Used car Dealers.php">Sell Used car <span>&nbsp;(2)</span>
                            </a>
                          </td>
                          
                        </tr>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <!-- Middle Content Area  End -->
          </div>
          <!-- Row End -->
        </div>
        <!-- Main Container End -->
      </section>
    </div>
    <div class="clearfix"></div>
                    <div class="text-center">
                      <div class="load-more-btn"></div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </section>
        </div>
      </div>
    </div>
  </div> <?php include 'include/footer.php';?>