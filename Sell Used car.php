<?php include "include/header.php" ?> <section class="transparent-breadcrumb-listing" style="background: url(wp-content/img1/Maruti-Suzuki-Brezza-Price.jpg); background-repeat: no-repeat ; background-size: cover ;  background-position: center center ;  background-attachment: scroll; ">
  <div class="container">
    <div class="row">
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="list-heading">
          <h2>Sell Used car</h2>
        </div>
      </div>
    </div>
  </div>
</section>
<div class="main-content-area clearfix">
  <section class="section-padding   gray ">
    <div class="container">
      <div class="row">
        <div class="col-md-8 col-xs-12 col-sm-12">
          <div class="row">
            <div class="posts-masonry">
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="blog-post">
                  <div class="post-334 post type-post status-publish format-standard has-post-thumbnail hentry category-analysis-features category-modified-cars category-sport-cars category-upcomming-cars tag-audi tag-bugatti tag-ford">
                    <div class="post-img">
                      <a href="#">
                        <img class="img-responsive" src="wp-content/img1/CheckValue.svg" alt="Ford Focus Production for North America Will Cease for a Year">
                      </a>
                    </div>
                    <div class="blog-content">
                      <div class="user-preview">
                        <img alt='' src='wp-content/img1/CheckValue.svg' srcset='wp-content/img1/CheckValue.svg' class='avatar avatar-40 photo' height='40' width='40' loading='lazy' decoding='async' />
                      </div>
                      <div class="post-info ">
                        <a href="javascript:void(0);"></a>
                        <a href="javascript:void(0);"></a>
                      </div>
                      <h3 class="post-title">
                        <a href="index.php"> 1.Do I need to book an appointment before visiting the store?</a>
                      </h3>
                      <p class="post-excerpt">We would suggest you to book an appointment beforehand in order to avoid any waiting time. Just enter your car details and book your slot instantly. Request a home inspection from the comfort of your home and get the best price assurance on your used car... <a href="#">
                          <strong>Read More</strong>
                        </a>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="blog-post">
                  <div class="post-333 post type-post status-publish format-standard has-post-thumbnail hentry category-analysis-features tag-audi tag-bugatti">
                    <div class="post-img">
                      <a href="#">
                        <img class="img-responsive" src="wp-content/img1/Sellcar-Img (1).svg" alt="We Hear: Audi RS Models Could Be Offered in Rear-Wheel Drive">
                      </a>
                    </div>
                    <div class="blog-content">
                      <div class="user-preview">
                        <img alt="img-responsive" src='https://thumbs.gfycat.com/MadeupImpracticalHippopotamus-max-1mb.gif' srcset='wp-content/img1/Sellcar-Img (1).svg' class='avatar avatar-40 photo' height='40' width='40' loading='lazy' decoding='async' />
                      </div>
                      <div class="post-info ">
                        <a href="javascript:void(0);"></a>
                        <a href="javascript:void(0);"></a>
                      </div>
                      <h3 class="post-title">
                        <a href="#">2. Do you provide home inspection? </a>
                      </h3>
                      <p class="post-excerpt"> Yes, we do provide home inspection for a hassle-free car selling experience.Selling your used car can be cumbersome and tiring, but we make sure to. To check whether this service is available at your location, please select your city from our page while booking the appointment or call us on 1800-12345-2323. <a href="../we-hear-audi-rs-models-could-be-offered-in-rear-wheel-drive/index.html">
                          <strong>Read More</strong>
                        </a>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="blog-post">
                  <div class="post-332 post type-post status-publish format-standard has-post-thumbnail hentry category-upcomming-cars tag-audi tag-bmw tag-bugatti">
                    <div class="post-img">
                      <a href="#">
                        <img class="img-responsive" src="https://www.car-revs-daily.com/wp-content/uploads/2014-Kia-Soul-Exclaim-GIF-colors-SOUL-TRAIN-slider.gif" alt="BMW Black Fire Edition X5, X6 M Coming in August">
                      </a>
                    </div>
                    <div class="blog-content">
                      <div class="user-preview">
                        <img alt='' src='wp-content/img1/2015-kia-soul-car-kia-motors-2014-kia-soul-exclaim-png-favpng-hfiK0eTmY1ncHhwt7E7jyibMC.jpg' srcset='wp-content/img1/2015-kia-soul-car-kia-motors-2014-kia-soul-exclaim-png-favpng-hfiK0eTmY1ncHhwt7E7jyibMC.jpg' class='avatar avatar-40 photo' height='40' width='40' loading='lazy' decoding='async' />
                      </div>
                      <div class="post-info ">
                        <a href="javascript:void(0);"></a>
                        <a href="javascript:void(0);"></a>
                      </div>
                      <h3 class="post-title">
                        <a href="#">KIA SELTOS </a>
                      </h3>
                      <p class="post-excerpt"> KIA SELTOS
The newest member of our SUV family, Kia Seltos features a bold and versatile look that helps owners capture the spirit of adventure.Seltos is a great addition to our With their upscale interiors and innovative tech, Seltos is a great addition to our SUV lineup.. <a href="#">
                          <strong>Read More</strong>
                        </a>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="blog-post">
                  <div class="post-331 post type-post status-publish format-standard has-post-thumbnail hentry category-modified-cars category-sport-cars tag-convertible tag-ferrari tag-italia">
                    <div class="post-img">
                      <a href="#">
                        <img class="img-responsive" src="wp-content/img1/suv-img-top__1_-removebg-preview.png" alt="2015 Ferrari 458 Italia Convertible a true sports car">
                      </a>
                    </div>
                    <div class="blog-content">
                      <div class="user-preview">
                        <img alt='' src='wp-content/jd-power-hero-1-removebg-preview (3).png' srcset='wp-content/img1/suv-img-top__1_-removebg-preview.png' class='avatar avatar-40 photo' height='40' width='40' loading='lazy' decoding='async' />
                      </div>
                      <div class="post-info ">
                        <a href="javascript:void(0);"></a>
                        <a href="javascript:void(0);"></a>
                      </div>
                      <h3 class="post-title">
                        <a href="#"> 3.KIA SUVS FOR SALE IN FLORIDA </a>
                      </h3>
                      <p class="post-excerpt"> At Bill Bryan Kia, we know that purchasing a new SUV can sometimes be stressful or overwhelming. Which is why we work hard to make it as easy as possible for our Leesburg customers to choose which new Kia SUV can meet all of their specific wants and needs and needs,. <a href="#">
                          <strong>Read More</strong>
                        </a>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="blog-post">
                  <div class="post-330 post type-post status-publish format-standard has-post-thumbnail hentry category-analysis-features tag-audi tag-bugatti tag-land-rover">
                    <div class="post-img">
                      <a href="index.php">
                        <img class="img-responsive" src="wp-content/img1/jd-power-hero-1-removebg-preview (3).png" alt="Land Rover Freelander 2 Se First Impression">
                      </a>
                    </div>
                    <div class="blog-content">
                      <div class="user-preview">
                        <img alt='' src='wp-content/img1/jd-power-hero-1-removebg-preview (3).png' srcset='wp-content/img1/jd-power-hero-1-removebg-preview (3).png' class='avatar avatar-40 photo' height='40' width='40' loading='lazy' decoding='async' />
                      </div>
                      <div class="post-info ">
                        <a href="javascript:void(0);"></a>
                        <a href="javascript:void(0);"></a>
                      </div>
                      <h3 class="post-title">
                        <a href="../land-rover-freelander-2-se/index.html"> Land Rover Freelander 2 Se First Impression </a>
                      </h3>
                      <p class="post-excerpt"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quis erat sed lorem
                         dictum ullamcorper. Sed vel elit sed nunc ornare auctor. Suspendisse i... <a href="#">
                          <strong>Read More</strong>
                        </a>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-md-6 col-sm-6 col-xs-12">
                <div class="blog-post">
                  <div class="post-328 post type-post status-publish format-standard has-post-thumbnail hentry category-sport-cars category-test-drive tag-audi tag-bugatti">
                    <div class="post-img">
                      <a href="#">
                        <img class="img-responsive" src="wp-content/img1/6392c4151b703.webp" alt="2017 Bugatti Chiron : Again with the Overkill">
                      </a>
                    </div>
                    <div class="blog-content">
                      <div class="user-preview">
                        <img alt='' src='wp-content/img1/6392c4151b703.webp' srcset='wp-content/img1/6392c4151b703.webp' class='avatar avatar-40 photo' height='40' width='40' loading='lazy' decoding='async' />
                      </div>
                      <div class="post-info ">
                        <a href="javascript:void(0);"></a>
                        <a href="javascript:void(0);"></a>
                      </div>
                      <h3 class="post-title">
                        <a href="#"> 2017 Bugatti Chiron : Again with the Overkill </a>
                      </h3>
                      <p class="post-excerpt"> Lorem ipsum dolor sit amet, consectetur adipiscing elit. Integer quis erat sed lorem dictum ullamcorper. .. <a href="#">
                          <strong>Read More</strong>
                        </a>
                      </p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-12 col-xs-12 col-sm-12"></div>
          </div>
        </div>
        <div class="col-md-4 col-xs-12 col-sm-12">
          <div class="blog-sidebar">
            <div class="widget widget-content">
              <div id="categories-3">
                <div class="widget-heading">
                  <h4 class="panel-title">
                    <a href="javascript:void(0)">Categories</a>
                  </h4>
                </div>
                <ul>
                  <li class="cat-item cat-item-15">
                    <a href="cars.php">Search New cars &amp; cars</a>
                  </li>
                  <li class="cat-item cat-item-16">
                    <a href="popular brand.php">Popular Brands</a>
                  </li>
                  <li class="cat-item cat-item-17">
                    <a href="latest care.php">Latest cars</a>
                  </li>
                  <li class="popular cars.php">Popular cars</a>
                  </li>
                  <li class="cat-item cat-item-19">
                    <a href="upcoming cars.php">Upcoming cars</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="widget widget-content">
              <div id="archives-3">
                <div class="widget-heading">
                  <h4 class="panel-title">
                    <a href="javascript:void(0)">Archive Posts</a>
                  </h4>
                </div>
                <ul>
                  <li>
                    <a href='../2017/06/index.html'>June 2017</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="widget widget-content">
              <div id="tag_cloud-2">
                <div class="widget-heading">
                  <h4 class="panel-title">
                    <a href="javascript:void(0)">Tags</a>
                  </h4>
                </div>
                <div class="tagcloud">
                  <a href="#" class="tag-cloud-link tag-link-20 tag-link-position-1" style="font-size: 22pt;" aria-label="audi (5 items)">audi</a>
                  <a href="#" class="tag-cloud-link tag-link-21 tag-link-position-2" style="font-size: 8pt;" aria-label="bmw (1 item)">bmw</a>
                  <a href="#" class="tag-cloud-link tag-link-22 tag-link-position-3" style="font-size: 22pt;" aria-label="Bugatti (5 items)">Bugatti</a>
                  <a href="#" class="tag-cloud-link tag-link-23 tag-link-position-4" style="font-size: 8pt;" aria-label="Convertible (1 item)">Convertible</a>
                  <a href="#" class="tag-cloud-link tag-link-24 tag-link-position-5" style="font-size: 8pt;" aria-label="Ferrari (1 item)">Ferrari</a>
                  <a href="#" class="tag-cloud-link tag-link-25 tag-link-position-6" style="font-size: 8pt;" aria-label="ford (1 item)">ford</a>
                  <a href="#" class="tag-cloud-link tag-link-26 tag-link-position-7" style="font-size: 8pt;" aria-label="Italia (1 item)">Italia</a>
                  <a href="#" class="tag-cloud-link tag-link-27 tag-link-position-8" style="font-size: 8pt;" aria-label="Land Rover (1 item)">Land Rover</a>
                </div>
              </div>
            </div>
            

            
            <div class="widget widget-content">
              <div id="search-2">
                <form role="search" method="get" class="search-form" action="#">
                  <div class="search-blog">
                    <div class="input-group stylish-input-group">
                      <input type="search" class="form-control" id="serch" placeholder="Search &hellip;" value="" name="s" />
                      <span class="input-group-addon">
                        <button type="submit">
                          <span class="fa fa-search"></span>
                        </button>
                      </span>
                    </div>
                  </div>
                  <input type="submit" class="search-submit" value="Search" />
                </form>
              </div>
            </div>
            <div class="widget widget-content">
              <div id="categories-2">
                <div class="widget-heading">
                  <h4 class="panel-title">
                    <a href="javascript:void(0)">Categories</a>
                  </h4>
                </div>
                <ul>
                  <li class="cat-item cat-item-15">
                    <a href="#">Analysis &amp; Features</a>
                  </li>
                  <li class="cat-item cat-item-16">
                    <a href="#">Modified Cars</a>
                  </li>
                  <li class="cat-item cat-item-17">
                    <a href="#">Sport Cars</a>
                  </li>
                  <li class="cat-item cat-item-18">
                    <a href="#">Test Drive</a>
                  </li>
                  <li class="cat-item cat-item-19">
                    <a href="#">Upcomming Cars</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="widget widget-content">
              <div id="recent-posts-2">
                <div class="widget-heading">
                  <h4 class="panel-title">
                    <a href="javascript:void(0)">Recent Posts</a>
                  </h4>
                </div>
                <ul>
                  <li>
                    <a href="#">Ford Focus Production for North America Will Cease for a Year</a>
                  </li>
                  <li>
                    <a href="#">We Hear: Audi RS Models Could Be Offered in Rear-Wheel Drive</a>
                  </li>
                  <li>
                    <a href="#">BMW Black Fire Edition X5, X6 M Coming in August</a>
                  </li>
                  <li>
                    <a href="#">2015 Ferrari 458 Italia Convertible a true sports car</a>
                  </li>
                  <li>
                    <a href="#">Land Rover Freelander 2 Se First Impression</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="widget widget-content">
              <div id="archives-2">
                <div class="widget-heading">
                  <h4 class="panel-title">
                    <a href="javascript:void(0)">Archives</a>
                  </h4>
                </div>
                <ul>
                  <li>
                 
                  </li>
                </ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<div class="clear"></div>
</li>
</ul>
</div>
</div>
</div>

<?php include 'include/footer.php';?>