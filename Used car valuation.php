
  <?php include "include/header.php" ?> 
    <section class="transparent-breadcrumb-listing" style="background: url(wp-content/img/hero-cars-2-1.png); background-repeat: no-repeat ; background-size: cover ;  background-position: center center ;  background-attachment: scroll; ">
      <div class="container">
        <div class="row">
          <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="list-heading">
             
            </div>
          </div>
        </div>
      </div>
    </section>
    <section class="wpb-content-wrapper">
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="section-padding  gray padding-top-20">
                <div class="container">
                  <div class="row">
                    <div class="col-md-8 col-xs-12 col-sm-12">
                      <ul class="accordion">
                        <li>
                          <h3 class="accordion-title">
                            <a href="#">Valuation of Used Cars Online?</a>
                            <img alt="Coupe" src="wp-content/img1/front-left-side-47 (4).webp" />
                            
                          <div class="accordion-content">
                            <p>The Car Valuation Tool is a free tool designed to help you get the estimated resale value of your car within seconds. Our car valuation algorithm is updated on a real-time basis which means that it’s in sync with the latest changes and market developments. However, the figures shown during online valuation are just an estimation and are subject to change post your car’s inspection at the store.

To get the estimated value of your car, you don’t even need to register to get the valuation done, what you have to do is add basic details of your car like, the make, model, kilometers run, your city, your contact number and that’s it. If you like the price range shown, you can schedule an appointment at the nearest CarDekho Gaadi store. We currently offer our services in Delhi-NCR, Bengaluru, Lucknow, Pune, Jaipur, Hyderabad, and Ahmedabad. </p>
                          </div>
                        </li>
                        <li>
                          <h3 class="accordion-title">
                            <a href="#">Can you list real estate with this theme?</a>
                          </h3>
                          <div class="accordion-content">
                            <p>Yes You can use it.</p>
                          </div>
                        </li>
                        <li>
                          <h3 class="accordion-title">
                            <a href="#">Can the admin add and remove custom fields ?</a>
                          </h3>
                          <div class="accordion-content">
                            <p>Yes Admin can add custom fields as many as they want </p>
                          </div>
                        </li>
                        <li>
                          <h3 class="accordion-title">
                            <a href="#"> Will visual composer work with this theme</a>
                          </h3>
                          <div class="accordion-content">
                            <p>Carspot is bulid up with latest trends and coading standards. Yes it&#039;s fully customizable with visual composer</p>
                          </div>
                        </li>
                        <li>
                          <h3 class="accordion-title">
                            <a href="#">Can i create some static pages and put with a shortcode ?</a>
                          </h3>
                          <div class="accordion-content">
                            <p>Carspot is fully customizable ypu can create whatever you want.</p>
                          </div>
                        </li>
                        <li>
                          <h3 class="accordion-title">
                            <a href="#">Does each member have their own page they can set up when they register. Can payments be integrated into the theme ?</a>
                          </h3>
                          <div class="accordion-content">
                            <p>Yes Each user will have there own page and dashboard . And its compatible with Wocommerce. </p>
                          </div>
                        </li>
                        <li>
                          <h3 class="accordion-title">
                            <a href="#">Is this Google AdSense‎ supported?</a>
                          </h3>
                          <div class="accordion-content">
                            <p>Yes This theme is Google AdSense supported.Even you can use Image ads if you don&#039;t want to use Google AdSense.</p>
                          </div>
                        </li>
                        <li>
                          <h3 class="accordion-title">
                            <a href="#">Is there any way to check messages, which send to ads authors??</a>
                          </h3>
                          <div class="accordion-content">
                            <p>Yes CarSpot come&#039;s with complete Messages solution. You just need to go to your dashboard to see all conversations </p>
                          </div>
                        </li>
                        <li>
                          <h3 class="accordion-title">
                            <a href="#">Do ads require review and approval before being visible on the site or are they be added immediately once the fee is paid?</a>
                          </h3>
                          <div class="accordion-content">
                            <p>Yes From theme options you can select add will be approved automatically or on approval </p>
                          </div>
                        </li>
                        <li>
                          <h3 class="accordion-title">
                            <a href="#">is subscription plan concerns both normal and featured ads? </a>
                          </h3>
                          <div class="accordion-content">
                            <p>All Registered user can post free ads, And IF user want to post featured ads then user must need to purchase a plan. </p>
                          </div>
                        </li>
                        <li>
                          <h3 class="accordion-title">
                            <a href="#">How to change the currency ?</a>
                          </h3>
                          <div class="accordion-content">
                            <p>Yes from back end you can change currency options</p>
                          </div>
                        </li>
                      </ul>
                    </div>
                    <div class="col-md-4 col-xs-12 col-sm-12">
                      <div class="blog-sidebar">
                        <div class="widget">
                          <div class="widget-heading">
                            <h4 class="panel-title">
                              <a>Saftey Tips </a>
                            </h4>
                          </div>
                          <div class="widget-content">
                            <p class="lead">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur vehicula bibendum sapien</p>
                            <ol>
                              <li>Make sure you post in the correct category.</li>
                              <li>Do not upload pictures with watermarks.</li>
                              <li>Do not post ads containing multiple items unless it's a package deal.</li>
                              <li>Do not put your email or phone numbers in the title or description.</li>
                              <li>Make sure you post in the correct category.</li>
                              <li>Do not post the same ad more than once or repost an ad within 48 hours.</li>
                            </ol>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
              <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="client-section  gray ">
                <div class="container">
                  <div class="row">
                    <div class="col-md-4 col-sm-12 col-xs-12">
                      <div class="margin-top-10">
                        <h3>Why Choose Us</h3>
                        <h2>Our premium Clients</h2>
                      </div>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-12">
                      <div class="brand-logo-area clients-bg">
                        <div class="clients-list owl-carousel owl-theme">
                          <div class="client-logo">
                            <a href="#" target="_blank">
                              <img src="wp-content/img/client_4.png" class="img-responsive" alt="clients" />
                            </a>
                          </div>
                          <div class="client-logo">
                            <a href="#" target="_blank">
                              <img src="wp-content/img/client_1-1.png" class="img-responsive" alt="clients" />
                            </a>
                          </div>
                          <div class="client-logo">
                            <a href="#" target="_blank">
                              <img src="wp-content/img/client_2.png" class="img-responsive" alt="clients" />
                            </a>
                          </div>
                          <div class="client-logo">
                            <a href="#" target="_blank">
                              <img src="wp-content/img/client_4 (1).png" class="img-responsive" alt="clients" />
                            </a>
                          </div>
                          <div class="client-logo">
                            <a href="#" target="_blank">
                              <img src="wp-content/img/client_4 (1).png" class="img-responsive" alt="clients" />
                            </a>
                          </div>
                          <div class="client-logo">
                            <a href="#" target="_blank">
                              <img src="wp-content/img/client_4 (1).png" class="img-responsive" alt="clients" />
                            </a>
                          </div>
                          <div class="client-logo">
                            <a href="#" target="_blank">
                              <img src="wp-content/img/client_4 (1).png" class="img-responsive" alt="clients" />
                            </a>
                          </div>
                        </div>
                      </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <div class="clear">
      
    </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </section>

	<?php include 'include/footer.php';?> 




  