
<?php include "include/header.php" ?>
      <!-- menu end -->
    </div>
    <!-- Navigation Menu End -->
    <!-- =-=-=-=-=-=-= Light Header End  =-=-=-=-=-=-= -->
    <section class="wpb-content-wrapper">
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="buysell-section">
                <div class="background-3" style="background-image: url(wp-content/img1/1.jpg);background-position: center center;background-repeat: no-repeat;background-size: cover;"></div>
                <div class="background-4" style="background-image: url(wp-content/img1/2.jpg);background-position: center center;background-repeat: no-repeat;background-size: cover;"></div>
                <div class="container">
                  <div class="row">
                    <div class="col-md-6 col-sm-6 col-xs-12 no-padding">
                      <div class="section-container-left">
                        <h1>Find Your Dream Car?</h1>
                        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.At vero eos et accusamus et iusto.</p> -->
                        <a href="#" target="%20_blank" class="btn-theme btn-lg btn"> Search </a>
                      </div>
                    </div>
                    <div class="col-md-6 col-sm-6 col-xs-12 no-padding">
                      <div class="section-container-right">
                        <h1>Want To Sale Your Car ?</h1>
                        <!-- <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor.At vero eos et accusamus et iusto.</p> -->
                        <a href="#" target="%20_blank" class="btn-primary btn-lg btn"> Hire Services </a>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <div class="search-bar">
                <div class="section-search search-style-2">
                  <div class="container">
                    <div class="row">
                      <div class="col-lg-12 col-md-12 col-xs-12 col-sm-12">
                        <div class="clearfix">
                          <form method="get" action="#">
                            <div class="search-form pull-left ">
                              <div class="search-form-inner pull-left ">
                                <div class="col-md-3 col-sm-6 col-xs-12 no-padding">
                                  <div class="form-group">
                                    <label>Location</label>
                                    <select name="country_id" id="country_id" autocomplete="off">
                                      <option value="">Location..</option>
                                      <option value="230">United States</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 no-padding">
                                  <div class="form-group">
                                    <label>Select Make</label>
                                    <select class="form-control" name="cat_id">
                                      <option label="Select Make" value="">Select Make</option>
                                      <option value="50">Alfa Romeo (0)</option>
                                      <option value="51">&nbsp;&nbsp; - &nbsp;Allard (0) </option>
                                      <option value="53">&nbsp;&nbsp; - &nbsp;Alpina (0) </option>
                                      <option value="54">&nbsp;&nbsp; - &nbsp;Alpine (0) </option>
                                      <option value="55">&nbsp;&nbsp; - &nbsp;Alvis (0) </option>
                                      <option value="57">&nbsp;&nbsp; - &nbsp;AMC (0) </option>
                                      <option value="58">&nbsp;&nbsp; - &nbsp;Ariel (0) </option>
                                      <option value="160">&nbsp;&nbsp; - &nbsp;Mito (0) </option>
                                      <option value="212">&nbsp;&nbsp; - &nbsp;Spider (0) </option>
                                      <option value="60">Aston Martin (0)</option>
                                      <option value="90">&nbsp;&nbsp; - &nbsp;DB 11 (0) </option>
                                      <option value="91">&nbsp;&nbsp; - &nbsp;DB 7 (0) </option>
                                      <option value="92">&nbsp;&nbsp; - &nbsp;DB 9 (0) </option>
                                      <option value="93">&nbsp;&nbsp; - &nbsp;DBS (0) </option>
                                      <option value="191">&nbsp;&nbsp; - &nbsp;Rapide (0) </option>
                                      <option value="232">&nbsp;&nbsp; - &nbsp;V 12 (0) </option>
                                      <option value="233">&nbsp;&nbsp; - &nbsp;V8 (0) </option>
                                      <option value="235">&nbsp;&nbsp; - &nbsp;VANTAGE (0) </option>
                                      <option value="62">Audi (4)</option>
                                      <option value="278">&nbsp;&nbsp; - &nbsp;A1 (0) </option>
                                      <option value="279">&nbsp;&nbsp; - &nbsp;A3 (1) </option>
                                      <option value="280">&nbsp;&nbsp; - &nbsp;A4 (2) </option>
                                      <option value="281">&nbsp;&nbsp; - &nbsp;A5 (1) </option>
                                      <option value="282">&nbsp;&nbsp; - &nbsp;A6 (0) </option>
                                      <option value="283">&nbsp;&nbsp; - &nbsp;A7 (0) </option>
                                      <option value="187">&nbsp;&nbsp; - &nbsp;Q3 (0) </option>
                                      <option value="188">&nbsp;&nbsp; - &nbsp;R8 (0) </option>
                                      <option value="227">&nbsp;&nbsp; - &nbsp;TT (0) </option>
                                      <option value="65">Austin (1)</option>
                                      <option value="259">&nbsp;&nbsp; - &nbsp;10 (0) </option>
                                      <option value="112">&nbsp;&nbsp; - &nbsp;Fx4 (0) </option>
                                      <option value="155">&nbsp;&nbsp; - &nbsp;Maxi (0) </option>
                                      <option value="159">&nbsp;&nbsp; - &nbsp;Mini (1) </option>
                                      <option value="66">Austin Healey (0)</option>
                                      <option value="297">Aveo (0)</option>
                                      <option value="68">Bentley (0)</option>
                                      <option value="71">BMW (0)</option>
                                      <option value="252">&nbsp;&nbsp; - &nbsp;1 Series (0) </option>
                                      <option value="260">&nbsp;&nbsp; - &nbsp;2 Series (0) </option>
                                      <option value="262">&nbsp;&nbsp; - &nbsp;3 Series (0) </option>
                                      <option value="265">&nbsp;&nbsp; - &nbsp;4 Series (0) </option>
                                      <option value="268">&nbsp;&nbsp; - &nbsp;5 Series (0) </option>
                                      <option value="269">&nbsp;&nbsp; - &nbsp;6 Series (0) </option>
                                      <option value="270">&nbsp;&nbsp; - &nbsp;7 Series (0) </option>
                                      <option value="276">&nbsp;&nbsp; - &nbsp;8 Series (0) </option>
                                      <option value="116">&nbsp;&nbsp; - &nbsp;Gt (0) </option>
                                      <option value="129">&nbsp;&nbsp; - &nbsp;i8 (0) </option>
                                      <option value="141">&nbsp;&nbsp; - &nbsp;M series (0) </option>
                                      <option value="249">&nbsp;&nbsp; - &nbsp;Z3 (0) </option>
                                      <option value="250">&nbsp;&nbsp; - &nbsp;Z4 (0) </option>
                                      <option value="251">&nbsp;&nbsp; - &nbsp;Z8 (0) </option>
                                      <option value="302">Bolt (0)</option>
                                      <option value="307">Brothers (0)</option>
                                      <option value="314">Camaro (0)</option>
                                      <option value="325">Charger (0)</option>
                                      <option value="84">Corvette (0)</option>
                                      <option value="339">Dart (0)</option>
                                      <option value="103">Ferrari (0)</option>
                                      <option value="266">&nbsp;&nbsp; - &nbsp;458 Italia (0) </option>
                                      <option value="267">&nbsp;&nbsp; - &nbsp;488 (0) </option>
                                      <option value="311">&nbsp;&nbsp; - &nbsp;California (0) </option>
                                      <option value="313">&nbsp;&nbsp; - &nbsp;California T (0) </option>
                                      <option value="357">&nbsp;&nbsp; - &nbsp;F430 Spider (0) </option>
                                      <option value="106">Ford (2)</option>
                                      <option value="290">&nbsp;&nbsp; - &nbsp;Anglia (0) </option>
                                      <option value="306">&nbsp;&nbsp; - &nbsp;Bronco (0) </option>
                                      <option value="333">&nbsp;&nbsp; - &nbsp;Cortina (0) </option>
                                      <option value="338">&nbsp;&nbsp; - &nbsp;Crown Victoria (0) </option>
                                      <option value="349">&nbsp;&nbsp; - &nbsp;Escort (0) </option>
                                      <option value="356">&nbsp;&nbsp; - &nbsp;F 150 (0) </option>
                                      <option value="358">&nbsp;&nbsp; - &nbsp;Fairlane (0) </option>
                                      <option value="164">&nbsp;&nbsp; - &nbsp;Mustang (2) </option>
                                      <option value="189">&nbsp;&nbsp; - &nbsp;Ranger (0) </option>
                                      <option value="119">Honda (1)</option>
                                      <option value="285">&nbsp;&nbsp; - &nbsp;Accord (0) </option>
                                      <option value="286">&nbsp;&nbsp; - &nbsp;Accord Euro (0) </option>
                                      <option value="304">&nbsp;&nbsp; - &nbsp;BR-V (1) </option>
                                      <option value="327">&nbsp;&nbsp; - &nbsp;City (0) </option>
                                      <option value="328">&nbsp;&nbsp; - &nbsp;Civic (0) </option>
                                      <option value="337">&nbsp;&nbsp; - &nbsp;CR-V (0) </option>
                                      <option value="238">&nbsp;&nbsp; - &nbsp;Vezel (0) </option>
                                      <option value="123">Hummer (0)</option>
                                      <option value="366">&nbsp;&nbsp; - &nbsp;H1 (0) </option>
                                      <option value="367">&nbsp;&nbsp; - &nbsp;H2 (0) </option>
                                      <option value="368">&nbsp;&nbsp; - &nbsp;H3 (0) </option>
                                      <option value="132">Jaguar (0)</option>
                                      <option value="134">Joy (0)</option>
                                      <option value="253">&nbsp;&nbsp; - &nbsp;1.0 (0) </option>
                                      <option value="254">&nbsp;&nbsp; - &nbsp;1.0 CNG (0) </option>
                                      <option value="135">Lamborghini (0)</option>
                                      <option value="296">&nbsp;&nbsp; - &nbsp;Aventador (0) </option>
                                      <option value="309">&nbsp;&nbsp; - &nbsp;Cabrera (0) </option>
                                      <option value="335">&nbsp;&nbsp; - &nbsp;Countach (0) </option>
                                      <option value="342">&nbsp;&nbsp; - &nbsp;Diablo (0) </option>
                                      <option value="361">&nbsp;&nbsp; - &nbsp;Gallardo (0) </option>
                                      <option value="363">&nbsp;&nbsp; - &nbsp;Gt (0) </option>
                                      <option value="163">&nbsp;&nbsp; - &nbsp;Murcielago (0) </option>
                                      <option value="137">Land Rover (1)</option>
                                      <option value="156">McLaren (0)</option>
                                      <option value="157">Mercedes (0)</option>
                                      <option value="308">&nbsp;&nbsp; - &nbsp;C Class (0) </option>
                                      <option value="345">&nbsp;&nbsp; - &nbsp;E Class (0) </option>
                                      <option value="200">&nbsp;&nbsp; - &nbsp;S Class (0) </option>
                                      <option value="201">&nbsp;&nbsp; - &nbsp;S Series (0) </option>
                                      <option value="209">&nbsp;&nbsp; - &nbsp;SLK Class (0) </option>
                                      <option value="161">Mitsubishi (1)</option>
                                      <option value="346">&nbsp;&nbsp; - &nbsp;Ek Wagon (0) </option>
                                      <option value="382">&nbsp;&nbsp; - &nbsp;Lancer (1) </option>
                                      <option value="385">&nbsp;&nbsp; - &nbsp;Mirage (0) </option>
                                      <option value="173">&nbsp;&nbsp; - &nbsp;Pajero (0) </option>
                                      <option value="174">&nbsp;&nbsp; - &nbsp;Pajero Mini (0) </option>
                                      <option value="167">Nissan (0)</option>
                                      <option value="263">&nbsp;&nbsp; - &nbsp;350Z (0) </option>
                                      <option value="264">&nbsp;&nbsp; - &nbsp;370Z (0) </option>
                                      <option value="364">&nbsp;&nbsp; - &nbsp;GT-R (0) </option>
                                      <option value="207">&nbsp;&nbsp; - &nbsp;SILVIA (0) </option>
                                      <option value="208">&nbsp;&nbsp; - &nbsp;SKYLINE (0) </option>
                                      <option value="169">Nitro (0)</option>
                                      <option value="172">Optra (0)</option>
                                      <option value="255">&nbsp;&nbsp; - &nbsp;1.4 (0) </option>
                                      <option value="257">&nbsp;&nbsp; - &nbsp;1.6 Automatic (0) </option>
                                      <option value="258">&nbsp;&nbsp; - &nbsp;1.8 Automatic (0) </option>
                                      <option value="177">Porsche (0)</option>
                                      <option value="277">&nbsp;&nbsp; - &nbsp;911 (0) </option>
                                      <option value="303">&nbsp;&nbsp; - &nbsp;Boxster (0) </option>
                                      <option value="324">&nbsp;&nbsp; - &nbsp;Cayenne (0) </option>
                                      <option value="391">&nbsp;&nbsp; - &nbsp;Panamera (0) </option>
                                      <option value="194">Renault (1)</option>
                                      <option value="198">Rolls-Royce (1)</option>
                                      <option value="210">Sonic (0)</option>
                                      <option value="211">Spark (0)</option>
                                      <option value="218">Stealth (0)</option>
                                      <option value="222">Suzuki (0)</option>
                                      <option value="239">Viper (0)</option>
                                      <option value="242">Volt (0)</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 no-padding">
                                  <div class="form-group">
                                    <label>Select Year</label>
                                    <select class=" form-control" name="year_from">
                                      <option label="Select Year" value="">Select Year</option>
                                      <option value="2010">2010</option>
                                      <option value="2011">2011</option>
                                      <option value="2012">2012</option>
                                      <option value="2013">2013</option>
                                      <option value="2014">2014</option>
                                      <option value="2015">2015</option>
                                      <option value="2016">2016</option>
                                      <option value="2017">2017</option>
                                    </select>
                                  </div>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 no-padding">
                                  <div class="form-group">
                                    <label>Body Type</label>
                                    <select class=" form-control" name="body_type">
                                      <option label="Select Body Type" value="">Select Body Type</option>
                                      <option value="Convertible">Convertible</option>
                                      <option value="Coupe">Coupe</option>
                                      <option value="Crossover">Crossover</option>
                                      <option value="Electric">Electric</option>
                                      <option value="Hatchback">Hatchback</option>
                                      <option value="Hybrid">Hybrid</option>
                                      <option value="Luxury">Luxury</option>
                                      <option value="Sedan">Sedan</option>
                                      <option value="SUV">SUV</option>
                                      <option value="Truck">Truck</option>
                                      <option value="Van/Minivan">Van/Minivan</option>
                                      <option value="Wagon">Wagon</option>
                                    </select>
                                  </div>
                                </div>
                              </div>
                              <div class="form-group pull-right ">
                                <button type="submit" id="submit_loader" value="submit" class="btn btn-lg btn-theme">Search Now</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="custom-padding gray">
                <!-- Main Container -->
                <div class="container">
                  <!-- Row -->
                  <div class="row">
                    <div class="heading-panel">
                      <div class="col-xs-12 col-md-12 col-sm-12 left-side">
                        <!-- Main Title -->
                        <h2>Latest <span class="heading-color"> Featured </span> Cars </h2>
                        <!-- Short Description -->
                      </div>
                    </div>
                    <div class="col-md-12 col-xs-12 col-sm-12">
                      <div class="row">
                        <div class="grid-style-2">
                          <div class="posts-masonry ads-for-home">
                            <div class="col-md-4  col-lg-4 col-sm-6 col-xs-12" id=".holder-1366">
                              <div class="category-grid-box-1">
                                <div class="featured-ribbon">
                                  <span>Featured</span>
                                </div>
                                <div class="image">
                                  <a href="#">
                                    <img src="wp-content/img1/MG-Astor-200920211457.jpg" alt="2017 Maserati Ghibli SQ4 Blue" class="img-responsive" />
                                  </a>
                                  <div class="price-tag">
                                    <div class="price">
                                      <span>₹ 11.06 -  <span class=""> (20.22 lakh)</span>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="short-description-1 clearfix">
                                  <div class="category-title">
                                    <span class="padding_cats">
                                      <a href="#">A4</a>
                                    </span>
                                    <span class="padding_cats">
                                      <a href="#">Audi</a>
                                    </span>
                                  </div>
                                  <h3>
                                    <!-- <a href="../ad/2017-maserati-ghibli-sq4-blue-1695-miles-2/index.html">2017 Maserati Ghibli...</a> -->
                                  </h3>
                                  <p class="location">
                                    <i class="fa fa-map-marker"></i>
                                    <a href="#">United States</a>, <a href="#">California</a>
                                  </p>
                                  <ul class="list-unstyled">
                                    <li>
                                      <i class="flaticon-gas-station-1"></i>Petrol
                                    </li>
                                    <li>
                                      <i class="flaticon-dashboard"></i>NA
                                    </li>
                                    <li>
                                      <i class="flaticon-engine-2"></i>2000 cc
                                    </li>
                                    <li>
                                      <i class="flaticon-car-2"></i>1349cc - 1498cc
                                    </li>
                                    <li>
                                      <i class="flaticon-cogwheel-outline"></i>Black
                                    </li>
                                  </ul>
                                </div>
                                <div class="ad-info-1">
                                  <p>
                                    <!-- <i class="flaticon-calendar"></i> &nbsp; <span>January 24, 2019</span> -->
                                  </p>
                                  <ul class="pull-right ">
                                    <li>
                                      <a data-toggle="tooltip" data-placement="top" data-original-title="Save Ad" href="javascript:void(0);" class="save-ad" data-adid="1366">
                                        <i class="flaticon-like-1"></i>
                                      </a>
                                      <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                                    </li>
                                    <li>
                                      <a href="#">
                                        <i class="flaticon-message"></i>
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <!-- Listing Ad Grid -->
                            </div>
                            <div class="col-md-4  col-lg-4 col-sm-6 col-xs-12" id=".holder-1362">
                              <div class="category-grid-box-1">
                                <div class="featured-ribbon">
                                  <span>Featured</span>
                                </div>
                                <div class="image">
                                  <a href="#">
                                  <img src="wp-content/img1/hyundai-venue-9a6d3a2d.jpg" alt="2017 Maserati Ghibli SQ4 Blue" class="img-responsive" style="
    width: 94%;
    height: 238px;
">
                                  <div class="price-tag">
                                    <div class="price">
                                      <span> 6.55 – <span class=""> (11.16 Lakh*)</span>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="short-description-1 clearfix">
                                  <div class="category-title">
                                    <span class="padding_cats">
                                      <a href="#">Austin</a>
                                    </span>
                                    <span class="padding_cats">
                                      <a href="#">Mini</a>
                                    </span>
                                  </div>
                                  <h3>
                                    <!-- <a href="../ad/2017-maserati-ghibli-sq4-blue-1694-miles/index.html">2017 Maserati Ghibli...</a> -->
                                  </h3>
                                  <p class="location">
                                    <i class="fa fa-map-marker"></i>
                                    <a href="#">California</a>, <a href="#">United States</a>
                                  </p>
                                  <ul class="list-unstyled">
                                    <li>
                                      <i class="flaticon-gas-station-1"></i>Petrol
                                    </li>
                                    <li>
                                      <i class="flaticon-dashboard"></i>1,694 km
                                    </li>
                                    <li>
                                      <i class="flaticon-engine-2"></i>3500 cc
                                    </li>
                                    <li>
                                      <i class="flaticon-car-2"></i>Sedan
                                    </li>
                                    <li>
                                      <i class="flaticon-cogwheel-outline"></i>Blue
                                    </li>
                                  </ul>
                                </div>
                                <div class="ad-info-1">
                                  <p>
                                    <!-- <i class="flaticon-calendar"></i> &nbsp; <span>January 24, 2019</span> -->
                                  </p>
                                  <ul class="pull-right ">
                                    <li>
                                      <a data-toggle="tooltip" data-placement="top" data-original-title="Save Ad" href="javascript:void(0);" class="save-ad" data-adid="1362">
                                        <i class="flaticon-like-1"></i>
                                      </a>
                                      <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                                    </li>
                                    <li>
                                      <a href="#">
                                        <i class="flaticon-message"></i>
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <!-- Listing Ad Grid -->
                            </div>
                            <div class="col-md-4  col-lg-4 col-sm-6 col-xs-12" id=".holder-1552">
                              <div class="category-grid-box-1">
                                <div class="featured-ribbon">
                                  <span>Featured</span>
                                </div>
                                <div class="image">
                                  <a href="#">
                                    <img src="wp-content/img/venue-exterior-front-view-3.webp" alt="2017 Ford Mustang" class="img-responsive" />
                                  </a>
                                  <div class="price-tag">
                                    <div class="price">
                                      <span>₹7.49 - <span class=""> ( 13.99 Lakh)</span>
                                      </span>
                                    </div>
                                  </div>
                                </div>
                                <div class="short-description-1 clearfix">
                                  <div class="category-title">
                                    <span class="padding_cats">
                                      <a href=".#">Mustang</a>
                                    </span>
                                    <span class="padding_cats">
                                      <a href="#">Ford</a>
                                    </span>
                                  </div>
                                  <h3>
                                    <!-- <a href="../ad/2017-ford-mustang/index.html">2017 Ford Mustang</a> -->
                                  </h3>
                                  <p class="location">
                                    <i class="fa fa-map-marker"></i>
                                    <a href="#">United States</a>, <a href="#">Fort Lauderdale</a>, <a href="#">Florida</a>
                                  </p>
                                  <ul class="list-unstyled">
                                    <li>
                                      <i class="flaticon-gas-station-1"></i>Petrol
                                    </li>
                                    <li>
                                      <i class="flaticon-dashboard"></i>23 km
                                    </li>
                                    <li>
                                      <i class="flaticon-engine-2"></i>1500 cc
                                    </li>
                                    <li>
                                      <i class="flaticon-car-2"></i>Coupe
                                    </li>
                                    <li>
                                      <i class="flaticon-cogwheel-outline"></i>White
                                    </li>
                                  </ul>
                                </div>
                                <div class="ad-info-1">
                                  <p>
                                    <!-- <i class="flaticon-calendar"></i> &nbsp; <span>January 24, 2019</span> -->
                                  </p>
                                  <ul class="pull-right ">
                                    <li>
                                      <a data-toggle="tooltip" data-placement="top" data-original-title="Save Ad" href="javascript:void(0);" class="save-ad" data-adid="1552">
                                        <i class="flaticon-like-1"></i>
                                      </a>
                                      <input type="hidden" id="fav_ad_nonce" value="cc800a7d3b" />
                                    </li>
                                    <li>
                                      <a href="#">
                                        <i class="flaticon-message"></i>
                                      </a>
                                    </li>
                                  </ul>
                                </div>
                              </div>
                              <!-- Listing Ad Grid -->
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    <div class="text-center">
                      <div class="load-more-btn"></div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="custom-padding services-2   ">
                <div class="absolute-img">
                <img alt="Image Not Found" src="wp-content/img/wcu-3.png" class="img-responsive wow slideInLeft animated" data-wow-delay="0ms" data-wow-duration="2000ms" style="visibility: visible;animation-duration: 1956ms;animation-delay: 0ms;animation-name: slideInLeft;width: 88%;height: 70%;margin-top: 55px;"></div>
                <div class="container">
                  <div class="row">
                    <div class="col-md-5"></div>
                    <div class="col-md-7 col-sm-12 col-xs-12 ">
                      <div class="choose-title">
                        <h3>Services We Offer</h3>
                        <h2>We are expert in</h2>
                        <p>Ut consequat velit a metus accumsan, vel tempor nulla blandit. Integer euismod magna vel mi congue suscipit. Praesent quis facilisis neque. Donec scelerisque nibh vitae sapien ornare efficitur.</p>
                      </div>
                      <div class="choose-services">
                        <ul class="choose-list">
                          <li class="col-md-6 col-xs-12 col-sm-6">
                            <div class="services-grid">
                              <div class="icons">
                                <i class="flaticon-vehicle-2"></i>
                              </div>
                              <h4>Engine Diagnostic</h4>
                              <p>We have the right caring, experience and dedicated professional for you.</p>
                            </div>
                          </li>
                          <li class="col-md-6 col-xs-12 col-sm-6">
                            <div class="services-grid">
                              <div class="icons">
                                <i class="flaticon-vehicle"></i>
                              </div>
                              <h4>Wheel Alignment</h4>
                              <p>We have the right caring, experience and dedicated professional for you.</p>
                            </div>
                          </li>
                          <li class="col-md-6 col-xs-12 col-sm-6">
                            <div class="services-grid">
                              <div class="icons">
                                <i class="flaticon-disc-brake"></i>
                              </div>
                              <h4>Break Checkup</h4>
                              <p>We have the right caring, experience and dedicated professional for you.</p>
                            </div>
                          </li>
                          <li class="col-md-6 col-xs-12 col-sm-6">
                            <div class="services-grid">
                              <div class="icons">
                                <i class="flaticon-car-steering-wheel"></i>
                              </div>
                              <h4>Steering & Suspension</h4>
                              <p>We have the right caring, experience and dedicated professional for you.</p>
                            </div>
                          </li>
                        </ul>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
     
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="section-padding parallex  " style="background: rgba(0, 0, 0, 0) url(wp-content/img/4.jpg) center center no-repeat; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
                <!-- Main Container -->
                <div class="container">
                  <div class="row">
                    <div class="owl-testimonial-2  owl-carousel owl-theme">
                      <!--Testimonial Column-->
                      <div class="single_testimonial">
                        <div class="textimonial-content">
                          <h4>Awesome ! Loving It</h4>
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                        </div>
                        <div class="testimonial-meta-box">
                          <img src="../wp-content/uploads/sites/28/2017/06/3-13-90x90.jpg" alt="Sonu Monu" />
                          <div class="testimonial-meta">
                            <h3>Sonu Monu</h3>
                            <p>CTO</p>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                          </div>
                        </div>
                      </div>
                      <!--Testimonial Column-->
                      <div class="single_testimonial">
                        <div class="textimonial-content">
                          <h4>Very quick and Fast Support</h4>
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                        </div>
                        <div class="testimonial-meta-box">
                          <img src="../wp-content/uploads/sites/28/2017/06/1-14-90x90.jpg" alt="Hania Sheikh " />
                          <div class="testimonial-meta">
                            <h3>Hania Sheikh </h3>
                            <p> CEO Pvt. Inc.</p>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                          </div>
                        </div>
                      </div>
                      <!--Testimonial Column-->
                      <div class="single_testimonial">
                        <div class="textimonial-content">
                          <h4>Done in 3 Months! Awesome</h4>
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                        </div>
                        <div class="testimonial-meta-box">
                          <img src="../wp-content/uploads/sites/28/2017/06/6-90x90.jpg" alt="Humayun Sarfraz " />
                          <div class="testimonial-meta">
                            <h3>Humayun Sarfraz </h3>
                            <p> CTO Glixen Tech.</p>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                          </div>
                        </div>
                      </div>
                      <!--Testimonial Column-->
                      <div class="single_testimonial">
                        <div class="textimonial-content">
                          <h4>Just fabulous theme</h4>
                          <p>Lorem ipsum dolor sit amet consectetur adipisicing elitsed eiusmod tempor enim minim veniam quis notru.</p>
                        </div>
                        <div class="testimonial-meta-box">
                          <img src="../wp-content/uploads/sites/28/2017/06/5-1-90x90.jpg" alt="Arslan Tariq" />
                          <div class="testimonial-meta">
                            <h3>Arslan Tariq</h3>
                            <p> CTO Albana Inc.</p>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-ofa fa-star"></i>
                            <i class="fa fa-star-o"></i>
                            <i class="fa fa-star-o"></i>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
      
      <div class="vc_row wpb_row vc_row-fluid">
        <div class="wpb_column vc_column_container vc_col-sm-12">
          <div class="vc_column-inner">
            <div class="wpb_wrapper">
              <section class="client-section  gray ">
                <div class="container">
                  <div class="row">
                    <div class="col-md-4 col-sm-12 col-xs-12">
                      <div class="margin-top-10">
                        <h3>Why Choose Us</h3>
                        <h2>Our premium Clients</h2>
                      </div>
                    </div>
                    <div class="col-md-8 col-sm-12 col-xs-12">
                      <div class="brand-logo-area clients-bg">
                        <div class="clients-list owl-carousel owl-theme">
                          <div class="client-logo">
                            <a href="#" target="_blank">
                              <img src="wp-content/img/client_4.png" class="img-responsive" alt="clients" />
                            </a>
                          </div>
                          <div class="client-logo">
                            <a href="#" target="_blank">
                              <img src="wp-content/img/client_1-1.png" class="img-responsive" alt="clients" />
                            </a>
                          </div>
                          <div class="client-logo">
                            <a href="#" target="_blank">
                              <img src="wp-content/img/client_2.png" class="img-responsive" alt="clients" />
                            </a>
                          </div>
                          <div class="client-logo">
                            <a href="#" target="_blank">
                              <img src="wp-content/img/client_4 (1).png" class="img-responsive" alt="clients" />
                            </a>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            </div>
          </div>
        </div>
      </div>
    </section>
   
    <?php include 'include/footer.php';?>